LabMetry
========================

LabMetry is a software able to give useful statistics and insights about the publications of a laboratory. This software relies on three main sources of data : Hyper Articles en Ligne (HAL) from the CNRS, ISI Web of Knowledge (WOK) and the Computing Research and Education Association of Australasia ranking (CORE).

1) Installing LabMetry
----------------------------------

LabMetry is a web application based on the Symfony 2 framework. You will need a webserver with the following packages installed:

* `mysql-server`
* `php` with a version >= 5.4 and <= 5.6
* `php-ldap`
* `php-mysql`
* `php-dom`
* `php-intl`
* `curl`
* `texlive`
* `texlive-latex-extra`

### Download the project

In the web directory you want to install LabMetry to, clone the repository :

    git clone https://bitbucket.org/protournoisas/labmetry.git
    
You have then to download the vendors (dependencies) of Symfony. In the Labmetry directory, execute the following command :

    php composer.phar install
    
After a while, the installer will ask you for some parameters.

### Parameters

Here are the parameters used by LabMetry and how to configure them:

* `database_*` : these are the database connection parameters. Adapt them to your own server.
* `mailer_*` : these are the mail server connection parameters. Adapt them to your own server.
* `locale` : currently, only `en` (English) is supported.
* `secret` : for security purposes. You should generate your own key.
* `authentication_type` : you have two possibilities for authenticating the users on LabMetry
    * `security_labmetryuser` : a regular authentication process with registration and profile forms.
    * `LDAP` : use your institution directory for authentication via the LDAP protocol.
    * **IMPORTANT : According to this setting, you have to modify the line 3 of the file `app/config/config.yml` to :**
        * `- { resource: security_labmetryuser.yml }`
        * `- { resource: security_LDAP.yml }`
* `ldap_*` : these are the settings for the LDAP authentication. Adapt them to your own server. If you chose the regular authentication, just keep the default parameters.
* `apache_home` : home directory of the Apache user. The default value should be okay.
* `application_path` : the absolute path where you installed LabMetry.
* `php_path` : the absolute path where the PHP engine is installed.
* `has_proxy`, `proxy_url`, `proxy_port` : if your server is behind a proxy, set `has_proxy` to `true` and set the proxy url and port accordingly. Otherwise, keep the default parameters.
* `HALID_lab`, `laboratory_code`: values that corresponds to your laboratory in HAL. A quick way to retrieve them is to query the HALV3 API : `http://api.archives-ouvertes.fr/ref/structure/?q=*[NAME OF MY LABORATORY]*&wt=json&fl=name_s,docid,code_s` by replacing [NAME OF MY LABORATORY] by (a portion of) the name of the laboratory. The `docid` corresponds to the `HALID_lab` parameter and the `code_s` corresponds to the `laboratory_code` parameter.
* `CORE_url` : the direct link to the CSV export function of Core rankings. As this service is in alpha, this URL is likely to change.
* `WOK_login`, `WOK_password` : you need to have credentials to ISI Web Of Knowledge via the [BibCnrs Portal][1]. They are bound to your laboratory and changed every year.
* `research_gate_url` : link to ResearchGate search portal. The default value should be okay.
* `google_scholar_url` : link to Google Scholar. The default value should be okay.

### Check your installation

You can check your configuration by calling this script :

    php app/check.php

Fix the different errors and warnings before using LabMetry.

### Set up the database

Run the following commands to create the database tables and load the needed default values into it

    php app/console doctrine:schema:update --force
    
    php app/console doctrine:fixtures:load

2) First data import
----------------------------------

If everything is okay, you can now retrieve the data. This may take time on first execution. Execute the following commands in order :

    php app/console bibliometry:core-import
    
    php app/console bibliometry:hal-import beginYear endYear
    
    php app/console bibliometry:wok-import
    
    php app/console bibliometry:scholar-import
    
    php app/console bibliometry:dblp-import
    
    php app/console bibliometry:cache-publications-grade
    
3) Login and promote you admin
----------------------------------

You can now register/login to the platform and start enjoying LabMetry ! By default, users don't have access to the administration panel.
To promote you administrator, execute the following command (you will be able to promote other user directly in the application after) :

    php app/console fos:user:promote [Your Login] ROLE_ADMIN
    
Enjoy LabMetry !

4) Update and Maintenance
----------------------------------
When deploying a new version on the server, after having changed the source files, the cache may need to be cleaned and the new assets generated. Otherwise, the changes may not be seen on the website. Here are the two commands to do this:

    php app/console cache:clear --env=prod --no-debug
    
    php app/console assetic:dump --env=prod --no-debug
    
Sometimes, it can be usefull to execute a SQL query on the database from the website. It can be done with this command:

    php app/console doctrine:query:sql "sql request;"
    
After making a change to the database schema (add/delete a property to an entity, remove/add an entity, ...) it can be necessary to clear the APC cache for the changes to take effect on the website. This can be done by adding in the file web/app.php the two lines

    apc_clear_cache();
    apc_clear_cache("user");
        
immediately after the creation of the kernel object. Then go to the url that begins with the root of the website and add /app.php at the end. Consult this page once and after the complete loading, you can remove the two lines added in web/app.php. By doing this, the APC cache should be cleared.

[1]:  https://bib.cnrs.fr
[2]:  https://code.google.com/apis/console
