<?php

namespace Bibliometry\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Bibliometry\MainBundle\Entity\Journal;

class Journals implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $otherJournal = new Journal();
        $otherJournal->setTitle("Other");
        $manager->persist($otherJournal);
        
        $manager->flush();
    }
}
