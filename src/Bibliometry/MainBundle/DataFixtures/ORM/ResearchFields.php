<?php

namespace Bibliometry\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Bibliometry\MainBundle\Entity\ResearchField;

class ResearchFields implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $newResearchField1 = new ResearchField();
        $newResearchField1->setName("Computer science");
        $newResearchField1->setAbbreviation("info");
        $manager->persist($newResearchField1);
        
        $newResearchField2 = new ResearchField();
        $newResearchField2->setName("Physics");
        $newResearchField2->setAbbreviation("phys");
        $manager->persist($newResearchField2);
        
        $newResearchField3 = new ResearchField();
        $newResearchField3->setName("SPI");
        $newResearchField3->setAbbreviation("spi");
        $manager->persist($newResearchField3);
        
        $newResearchField4 = new ResearchField();
        $newResearchField4->setName("SDV");
        $newResearchField4->setAbbreviation("sdv");
        $manager->persist($newResearchField4);
        
        $newResearchField5 = new ResearchField();
        $newResearchField5->setName("Chemistry");
        $newResearchField5->setAbbreviation("chim");
        $manager->persist($newResearchField5);
        
        $newResearchField6 = new ResearchField();
        $newResearchField6->setName("Statistics");
        $newResearchField6->setAbbreviation("stat");
        $manager->persist($newResearchField6);
        
        $manager->flush();
    }
}
