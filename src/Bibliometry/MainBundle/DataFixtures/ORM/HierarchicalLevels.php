<?php

namespace Bibliometry\MainBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Bibliometry\MainBundle\Entity\HierarchicalLevel;

class HierarchicalLevels implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $laboratoryLevel = new HierarchicalLevel();
        $laboratoryLevel->setLevelName("Laboratory");

        $manager->persist($laboratoryLevel);
        
        $departmentLevel = new HierarchicalLevel();
        $departmentLevel->setLevelName("Department");

        $manager->persist($departmentLevel);
        
        $thematicLevel = new HierarchicalLevel();
        $thematicLevel->setLevelName("Thematic");

        $manager->persist($thematicLevel);
        
        $projectLevel = new HierarchicalLevel();
        $projectLevel->setLevelName("Project");

        $manager->persist($projectLevel);
        
        $manager->flush();
    }
}
