<?php
namespace Bibliometry\MainBundle\EventListener;

use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class CheckRegistrationListener
{
    private $security_authorization_checker;
    private $security_token_storage;
    private $router;
    private $session;
    
    public function __construct($security_authorization_checker, $security_token_storage, $router, $session)
    {
        $this->security_authorization_checker = $security_authorization_checker;
        $this->security_token_storage = $security_token_storage;
        $this->router = $router;
        $this->session = $session;
    }
    
    public function onKernelController(GetResponseEvent $event)
    {
        // If we are not already on end registration page
        $route = $event->getRequest()->get('_route');
        if ('final_registration_route' === $route || 'select_matching_researcher_route' === $route || 'select_main_researcher_route' === $route) {
            return;
        }
        
        // We check that the user has completed its registration
        if ($this->security_authorization_checker->isGranted('IS_AUTHENTICATED_FULLY') && $this->security_token_storage->getToken()->getUser()->getRegistrationCompleted() == false)
        {
            // If not, redirect to the end registration page
            $this->session->getFlashBag()->add('error', 'bibliometry.base.error_finish_registration');
            
            $endRegistrationUrl = $this->router->generate('final_registration_route');
            $event->setResponse(new RedirectResponse($endRegistrationUrl));
        }
    }
}