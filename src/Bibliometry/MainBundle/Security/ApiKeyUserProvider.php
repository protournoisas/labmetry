<?php

namespace Bibliometry\MainBundle\Security;

use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Bibliometry\MainBundle\Entity\UserManager;

class ApiKeyUserProvider implements UserProviderInterface
{
    protected $userManager;
    
    public function __construct(UserManager $userManager)
    {
        $this->userManager = $userManager;
    }
    
    public function getUserForApiKey($apiKey)
    {
        $user = $this->userManager->findUserBy(array("apiKey" => $apiKey));
        
        if (empty($user))
        {
            throw new UsernameNotFoundException(sprintf('Not valid api key "%s".', $apiKey), 0);
        }

        return $user;
    }

    public function loadUserByUsername($username)
    {
        return new User(
            $username,
            null,
            // the roles for the user - you may choose to determine
            // these dynamically somehow based on the user
            array('IS_AUTHENTICATED_FULLY')
        );
    }

    public function refreshUser(UserInterface $user)
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    public function supportsClass($class)
    {
        return 'Symfony\Component\Security\Core\User\User' === $class;
    }
}