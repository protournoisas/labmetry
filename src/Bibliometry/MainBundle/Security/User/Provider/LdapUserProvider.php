<?php

namespace Bibliometry\MainBundle\Security\User\Provider;

use Symfony\Component\Security\Core\Exception\UsernameNotFoundException,
    Symfony\Component\Security\Core\Exception\UnsupportedUserException,
    Symfony\Component\Security\Core\User\UserProviderInterface,
    Symfony\Component\Security\Core\User\UserInterface;

use FR3D\LdapBundle\Ldap\LdapManagerInterface,
    FR3D\LdapBundle\Model\LdapUserInterface;
use Bibliometry\MainBundle\Entity\UserManager;

class LdapUserProvider implements UserProviderInterface
{
    /**
     * @var \FR3D\LdapBundle\Ldap\LdapManagerInterface
     */
    private $ldapManager;

    protected $userManager;

    /**
     * @var \Symfony\Component\Validator\Validator
     */
    protected $validator;

    /**
     * Constructor
     *
     * @param LdapManagerInterface $ldapManager
     * @param UserManager $userManager
     * @param Validator            $validator
     */
    public function __construct(LdapManagerInterface $ldapManager, UserManager $userManager, $validator)
    {
        $this->ldapManager = $ldapManager;
        $this->userManager = $userManager;
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function loadUserByUsername($username)
    {
        // Throw the exception if the username is not provided.
        if (empty($username)) {
            throw new UsernameNotFoundException('The username is not provided.');
        }

        // check if the user is already know to us
        $user = $this->userManager->findUserBy(array("username" => $username));
        $userLdap = $this->ldapManager->findUserByUsername($username);
        
        // Throw an exception if the username is not found.
        if(empty($user) && empty($userLdap)) {
            throw new UsernameNotFoundException(sprintf('User "%s" not found', $username));
        }
        
        // Throw an exception if the username cannot authenticate anymore
        if($user && empty($userLdap)) {
            throw new UsernameNotFoundException(sprintf('You don\'t have access to the platform anymore.'));
        }

        if (empty($user)) {
            $user = $this->userManager->createUser();
            $user->setRoles($userLdap->getRoles());
            $user
                ->setUsername($userLdap->getUsername())
                ->setPassword("")
                ->setDn($userLdap->getDn())
                ->setEmail($userLdap->getEmail())
                ->setEnabled(true);
        }

        return $user;
    }

    /**
     * {@inheritdoc}
     */
    public function refreshUser(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface) {
            throw new UnsupportedUserException(sprintf('Instances of "%s" are not supported.', get_class($user)));
        }

        return $this->loadUserByUsername($user->getUsername());
    }

    /**
     * {@inheritdoc}
     */
    public function supportsClass($class)
    {
        return $this->userManager->supportsClass($class);
    }
}