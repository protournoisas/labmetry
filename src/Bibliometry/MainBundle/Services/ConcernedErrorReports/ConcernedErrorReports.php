<?php

namespace Bibliometry\MainBundle\Services\ConcernedErrorReports;

class ConcernedErrorReports
{
	public $errorReportsRepository;

	public $security_authorization_checker;

	public function __construct($errorReportsRepository, $security_authorization_checker)
	{
		$this->errorReportsRepository = $errorReportsRepository;
		$this->security_authorization_checker = $security_authorization_checker;
	}

    public function getConcernedErrorReports($user)
    {
        $errors = $this->errorReportsRepository->findByHandled(false);
        
        // If Admin, all errors
        if ($this->security_authorization_checker->isGranted('ROLE_ADMIN')) {
            return $errors;
        }
        // Else get the errors that concern the teams the user lead
        $error_reports = array();
        if ($user->getResearcher())
        {
            foreach ($errors as $error)
            {
                $concernedTeams = $error->getConcernedTeams();
                foreach ($concernedTeams as $concernedTeam)
                {
                    if (in_array($concernedTeam, $user->getResearcher()->getAllLeadTeams()))
                    {
                        $error_reports[] = $error;
                        break;
                    }
                }
            }
        }  
        return $error_reports;     
    }
}
