<?php

namespace Bibliometry\MainBundle\Services\WOK;

use Bibliometry\MainBundle\Entity\Journal;
use Bibliometry\MainBundle\Entity\ImpactFactor;

class WOK
{
    private $em;
    private $WOK_login;
    private $WOK_password;
    private $ResearchGateService;
    private $dir;
    private $translator;

    public function __construct($em, $application_path, $has_proxy, $proxy_url, $proxy_port, $WOK_login, $WOK_password, $ResearchGateService, $translator)
    {
        $this->em = $em;
        $this->WOK_login = $WOK_login;
        $this->WOK_password = $WOK_password;
        $this->dir = $application_path . "web/uploads/";
        $this->ResearchGateService = $ResearchGateService;
        $this->translator = $translator;
        $this->proxyConfig = "";
        if($has_proxy)
            $this->proxyConfig = "export http_proxy='".$proxy_url.":".$proxy_port."';export https_proxy='".$proxy_url.":".$proxy_port."';";
    }

    public function loginToWOK()
    {
        // Post the form to connect and store the obtained cookie
        $response = shell_exec($this->proxyConfig." curl -sS -L --cookie-jar ".$this->dir."WOKCookie 'https://bib.cnrs.fr/api/ezticket/login?gate=insis.bib.cnrs.fr' --data 'username=" . $this->WOK_login . "&password=" . $this->WOK_password . "' --compressed --connect-timeout 10 --fail");
        // If unreachable
        if($response === null)
            throw new \Exception($this->translator->trans('bibliometry.admin.import_wok.error_login_portal_down'));
        // If cookie was not created
        if(!file_exists($this->dir . "WOKCookie"))
            throw new \Exception($this->translator->trans('bibliometry.admin.import_wok.error_cookie_not_created'));
        // We access the main portal and obtain its cookie and store it to be able to use it for all the following requests
        $response = shell_exec($this->proxyConfig." curl -sS -L --cookie ".$this->dir."WOKCookie --cookie-jar ".$this->dir."WOKCookie https://jcr2-clarivate-com.insis.bib.cnrs.fr/JCRJournalHomeAction.action --connect-timeout 10");
        // If unreachable
        if($response === null)
            throw new \Exception($this->translator->trans('bibliometry.admin.import_wok.error_wok_journal_list_page_down'));
    }

    private function getImpactFactors($journal, $abbrJournal, $edition)
    {
        if($abbrJournal == "" || $edition == "")
        {
            $journalInfo = NULL;
            $knownISSN = $journal->getISSN();
            if($knownISSN != NULL)
                $journalInfo = $this->getJournalInformations($journal, array($knownISSN));
            if($journalInfo == NULL) // No result => try to get ISSNs from ResearchGate
                $journalInfo = $this->getJournalInformations($journal, $this->ResearchGateService->getISSN($journal->getTitle(),$this->proxyConfig));
            if($journalInfo == NULL) // If there is still no result
                return; // Unable to find this journal on Web Of Knowledge
            $abbrJournal = $journalInfo->{"abbrJournal"};
            $edition = $journalInfo->{"edition"};
        }
        $abbrJournal = urlencode($abbrJournal);
        $edition = urlencode($edition);
        // Request impact factor and quartile for all available years for this journal
        $jsonResponse = shell_exec($this->proxyConfig." curl -sS --cookie ".$this->dir."WOKCookie 'https://jcr2-clarivate-com.insis.bib.cnrs.fr/JCRImpactFactorJson.action?abbrJournal=".$abbrJournal."&edition=".$edition."' --connect-timeout 10 --fail");
        $decodedJson = json_decode($jsonResponse);
        if($decodedJson->{"status"} === "FAILURE")
            return;
        $domains = $decodedJson->{"fields"}; // contain the different domain where this journal belongs
        array_splice($domains, 0, 2); // remove from the array the two first columns that correspond to year and impact factor
        foreach($decodedJson->{"data"} as $ranking)
        {
            $year = $ranking->{"year"};
            // If we already have the IF for this year
            if($journal->getIFYearNullable($year) != NULL)
                continue; // We can skip to next year
            $IF = $ranking->{"impactFactor"};
            $quartiles = array();
            foreach($domains as $domain)
            {
                $fieldContainingQuartil = $ranking->{$domain};
                preg_match("/Q(\d)/", $fieldContainingQuartil, $quartile);
                if(count($quartile) == 2 && $quartile[1] >= 1 && $quartile[1] <= 4)
                    $quartiles[] = $quartile[1];
            }
            if(count($quartiles) == 0)
                continue; // no quartile found for this year
            $bestQuartile = min($quartiles);
            // We store the informations in DB
            $impactFactor = new ImpactFactor();
            $impactFactor->setYear($year);
            $impactFactor->setValue($IF);
            $impactFactor->setQuartile($bestQuartile);
            $journal->addIF($impactFactor);
            $this->em->persist($journal);
        }
    }

    private function getJournalInformations($journal, $ISSNs)
    {
        if($ISSNs == NULL)
            return NULL;
        foreach($ISSNs as $ISSN)
        {
            $jsonResponse = shell_exec($this->proxyConfig." curl -sS --cookie ".$this->dir."WOKCookie 'https://jcr2-clarivate-com.insis.bib.cnrs.fr/SearchJournalsJson.action?query=".$ISSN."' --connect-timeout 10 --fail");
            // If unreachable
            if($jsonResponse == null)
                throw new \Exception($this->translator->trans('bibliometry.admin.import_wok.error_wok_journal_list_page_down'));
            $decodedJson = json_decode($jsonResponse);
            if($decodedJson->{"status"} === "SUCCESS")
            {
                $journalInfo = $decodedJson->{"data"}[0];
                $journal->setISSN($journalInfo->{"issn"});
                return $journalInfo;
            }
        }
        // Set an ISSN if found, even if we didn't find the journal in Web Of Knowledge
        if(count($ISSNs) > 0)
            $journal->setISSN($ISSNs[0]);
        return NULL;
    }

    public function importWOK($journals, $outputInterface)
    {
        try
        {   $numberOfJournals = count($journals);
            $progress = 0;
            $lastFlush = 0;
            foreach($journals as $journal)
            {
                $this->getImpactFactors($journal, "", "");
                $progress += 100 / $numberOfJournals;
                $outputInterface->writeln(round($progress));
                // Flush every 10 percent
                if(round($progress) / 10 > $lastFlush)
                {
                    $this->em->flush();
                    $lastFlush++;
                }
            }
            $this->em->flush();
            $outputInterface->writeln(100);
        }catch(\Exception $e)
        {
            $this->em->flush();
            $outputInterface->writeln("ERROR : " . $e->getMessage());
            throw $e;
        }
    }

    public function importOneJournal($journal, $abbrJournal, $edition)
    {
        try
        {
            $this->loginToWOK();
            $this->getImpactFactors($journal, $abbrJournal, $edition);
            $this->em->flush();
        }catch(\Exception $e)
        {
            throw $e;
        }
    }

    public function getAvailableYears()
    {
        try
        {
            $this->loginToWOK();
            // We request the Json page containing the available years
            $jsonResponse = shell_exec($this->proxyConfig." curl -sS --cookie ".$this->dir."WOKCookie https://jcr2-clarivate-com.insis.bib.cnrs.fr/JcrYearListJson.action --connect-timeout 10");
            // If unreachable
            if($jsonResponse === null)
                throw new \Exception($this->translator->trans('bibliometry.admin.import_wok.error_wok_portal_down'));
            $decodedJson = json_decode($jsonResponse);
            $years = array();
            foreach($decodedJson->{"data"} as $yearObject)
                $years[] = $yearObject->{"jcrYear"};
            return $years;
        }catch(\Exception $e)
        {
            echo "ERROR : " . $e->getMessage();
            throw $e;
        }
    }
    
    public function searchJournal($request)
    {
        try
        {
            $this->loginToWOK();
            $jsonResponse = shell_exec($this->proxyConfig." curl -sS --cookie ".$this->dir."WOKCookie 'https://jcr2-clarivate-com.insis.bib.cnrs.fr/SearchJournalsJson.action?limit=1000&query=".urlencode($request)."' --connect-timeout 10");
            // If unreachable
            if($jsonResponse === null)
                throw new \Exception($this->translator->trans('bibliometry.admin.import_wok.error_wok_portal_down'));
            $decodedJson = json_decode($jsonResponse);
            return $decodedJson->{"data"};
        }catch(\Exception $e)
        {
            echo "ERROR : " . $e->getMessage();
            throw $e;
        }
    }
}
