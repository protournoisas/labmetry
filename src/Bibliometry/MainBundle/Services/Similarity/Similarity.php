<?php

namespace Bibliometry\MainBundle\Services\Similarity;

use Bibliometry\MainBundle\Entity\Conference;

class Similarity
{
    private $ABBREVIATIONS = [
            "int." => "international",
            "conf." => "conference",
            "symp." => "symposium"
    ];
    private $FALSE_ACRONYMS = [
            "IEEE",
            "ACM"
    ];
    private $em;
    private $stopWordsService;
    // If the maximum similarity found is beyond this threshold, the conference from HAL match one in CORE.
    // Otherwise, the conference from HAL is dissimilar to all conferences from CORE.
    // The value of this threshold depends on the similarity function between two strings used.
    private $similarityThreshold;
    // The bonus to add to the similarity if the conference from HAL contains the acronym of a conference in CORE.
    // This bonus should not be too much (usually less than $similarityThreshold) but not to close from 0.
    private $valueBonusAcronym;
    // two next fields used to compute the IDF of a term (inverse document frequency)
    private $corupusSize;
    private $termsFrequency;
    // map associating a term to its document frequency
    public function __construct($em, $stopWordsService)
    {
        $this->em = $em;
        $this->stopWordsService = $stopWordsService;
        $this->similarityThreshold = 0.74; // to be changed if a new similarity function is used
        $this->valueBonusAcronym = 0.6; // to be changed if a new similarity function is used
    }

    public function getMatchingConference($conferenceHAL, $conferencesCORE, $corupusSize, $termsFrequency)
    {
        $this->termsFrequency = $termsFrequency;
        $this->corupusSize = $corupusSize;
        
        $maxSimilarity = 0.;
        $matchedConference = NULL;
        $conferenceHALpreProcessed = $this->preProcess($conferenceHAL);
        $conferenceHALtokens = explode(" ", $conferenceHALpreProcessed);
        $conferenceHALisWorkshop = in_array("workshop", $conferenceHALtokens) || in_array("workshops", $conferenceHALtokens);
        
        foreach($conferencesCORE as $conferenceCORE)
        {
            $computedSimilarity = (float) $this->getSimilarity($conferenceHAL, $conferenceHALpreProcessed, $conferenceHALtokens, $conferenceCORE, $conferenceHALisWorkshop);
            if($computedSimilarity > $maxSimilarity)
            {
                $maxSimilarity = $computedSimilarity;
                $matchedConference = $conferenceCORE;
            }
        }
        
        if($maxSimilarity >= $this->similarityThreshold)
            return $matchedConference;
        else
            return NULL;
    }

    public function getSimilarity($conferenceHAL, $conferenceHALpreProcessed, $conferenceHALtokens, $conferenceCORE, $conferenceHALisWorkshop)
    {
        $acronymConferenceCORE = strtoupper($conferenceCORE->getAcronym());
        $conferenceCOREpreProcessed = $conferenceCORE->getPreProcessedTitle();
        $conferenceCOREtokens = explode(" ", $conferenceCOREpreProcessed);
        $bonusAcronym = 0.0;
        
        // If $conferenceHALpreProcessed contains the word "workshop" or "workshops" but $conferenceCOREpreProcessed
        // does not contain "workshop", returns a similarity of 0. This is to avoid matching
        // a workshop to the conference where it takes place if the researcher wrote
        // both title of workshop and title of conference where the workshop takes place in HAL.
        if($conferenceHALisWorkshop && !in_array("workshop", $conferenceCOREtokens)
                && $conferenceCORE->getTitle() != "Structural and Syntactical Pattern Recognition") // this conference is a workshop
            return 0;
        
        if((in_array("european", $conferenceHALtokens) && in_array("international", $conferenceCOREtokens)) || (in_array("international", $conferenceHALtokens) && in_array("european", $conferenceCOREtokens)))
            return 0; // avoid to match a european conference with an internatinal even if they have almost the same name
        
        $possibleAcronyms = $this->extractAcronyms($this->removePunctNumbers($conferenceHAL));
        
        // Particular case for ECML/PKDD (distinct conferences for Core, but actually the same one)
        if((in_array("ECML", $possibleAcronyms) || in_array("PKDD", $possibleAcronyms)) && $conferenceCORE->getAcronym() == "ECML-PKDD")
            return 2;
        
        foreach($possibleAcronyms as $possibleAcronym)
            if($possibleAcronym == $acronymConferenceCORE)
            {
                if(strtoupper($conferenceHALpreProcessed) == $possibleAcronym)
                    return 1;
                else
                    // We will give a "bonus" to the final score
                    $bonusAcronym = $this->valueBonusAcronym;
            }
        return $this->similarity($conferenceHALtokens, $conferenceCOREtokens) + $bonusAcronym;
    }

    public function preProcess($input)
    { // Preprocess of strings
        $input = strtolower($input); // Lowercase
        
        foreach($this->ABBREVIATIONS as $abr => $full) // Remove abbreviations
            $input = str_replace($abr, $full, $input);
        
        $input = $this->removePunctNumbers($input); // Remove punctuation and numbers
                                                    
        // Remove stop words
        $input = $this->stopWordsService->removeStopWordsSimilarity(explode(' ', $input));
        
        return implode(' ', $input);
    }

    private function removePunctNumbers($input)
    { // Remove punctuation and numbers
        return trim(preg_replace("/[^[:alpha:]+]+/u", " ", $input));
    }

    private function extractAcronyms($conferenceTitle)
    { // Sequences of upper case letters of length at least 2 are stored (they are considered as acronyms).
        preg_match_all("/[A-Z]{2,}/", $conferenceTitle, $possibleAcronyms);
        $possibleAcronyms = array_unique($possibleAcronyms[0]); // remove duplicate
        return array_diff($possibleAcronyms, $this->FALSE_ACRONYMS); // remove "stop word" acronyms
    }
    
    // The string similarity used is called Soft Cardinality Spectra
    // The definition can be found in:
    // Sergio Jimenez, Alexander Gelbukh, 2012.
    // Baselines for natural language processing tasks based on soft cardinality spectra.
    // Applied and Computational Mathematics Journal.
    
    // The similarity of the two strings is computed using a resemblance coefficient with as argument the cardinality of:
    // - the set of tokens in first string
    // - the set of tokens in second strings
    // - the set of tokens in first string union second string
    
    // For one set of token, its cardinality is computed as follows
    // - Each term (no duplicate) of the set is divided into q-grams (possibly for different size q) and we keep a set of the different q-grams.
    // - The cardinality is a weigthed sum for each q-gram in the set. The weight of a q-gram depends on:
    // - - the number of term (no duplicate) having at least of occurence of this q-gram in the set
    // - - for each of those terms:
    // - - - the idf of this term
    // - - - the number of unique q-gram in this term
    private function similarity($tokensU, $tokensV)
    {
        // [qs, qe] define the range of q-gram size to use during the computation
        // using [5, 5] means that we use only 1 q-gram size which is 5
        $qs = 5;
        $qe = 5;
        $mapTerms = array();
        // fill the mapTerms with all the terms (no duplicate) of the first and second string.
        // add a boolean to this term to know if it belongs to the first string or (inclusive) the second string
        foreach($tokensU as $token)
        {
            if(!array_key_exists($token, $mapTerms))
            {
                $mapTerms[$token]["isInFirstString"] = true;
                $mapTerms[$token]["isInSecondString"] = false;
                $mapTerms[$token]["weight"] = $this->getTermWeight($token);
            }
        }
        foreach($tokensV as $token)
        {
            if(!array_key_exists($token, $mapTerms))
            {
                $mapTerms[$token]["isInFirstString"] = false;
                $mapTerms[$token]["isInSecondString"] = true;
                $mapTerms[$token]["weight"] = $this->getTermWeight($token);
            }
            else
                $mapTerms[$token]["isInSecondString"] = true;
        }
        
        // U, V and UuV will store the Soft Cardinality spectra of the three sets of respectivly the tokens
        // of the first string, the tokens of the second string and the tokens of the union of the two strings
        $U = 0.0;
        $V = 0.0;
        $UuV = 0.0;
        for($q = $qs; $q <= $qe; $q++)
        {
            $mapQgrams = array();
            // for each term, compute all its q-grams.
            foreach(array_keys($mapTerms) as $term)
            {
                $mapTerms[$term]["numberOfUniqueQgrams"] = 0.0;
                // for each q-gram, of this term, if this q-gram already exists, add the term to the list of term having at least one
                // occurence of the q-gram. Otherwise, create a new q-gram with only this term in the list.
                foreach($this->getUniqueQgrams($term, $q) as $qgram)
                {
                    $mapTerms[$term]["numberOfUniqueQgrams"]++;
                    if(!array_key_exists($qgram, $mapQgrams))
                        $mapQgrams[$qgram] = array();
                    array_push($mapQgrams[$qgram], $term);
                }
            }
            // for each q-gram, compute its weight that depends on the number of q-grams possessed by each term where this q-gram occurs.
            // compute the weight for the three sets at the same time.
            foreach(array_keys($mapQgrams) as $qgram)
            {
                $weightQgramsUuV = 0.0;
                $weightQgramsU = 0.0;
                $weightQgramsV = 0.0;
                $numberTermsUuV = 0.0;
                $numberTermsU = 0.0;
                $numberTermsV = 0.0;
                foreach($mapQgrams[$qgram] as $term)
                {
                    $weight = $mapTerms[$term]["weight"] / $mapTerms[$term]["numberOfUniqueQgrams"];
                    $weightQgramsUuV += $weight;
                    $numberTermsUuV++;
                    if($mapTerms[$term]["isInFirstString"])
                    {
                        $weightQgramsU += $weight;
                        $numberTermsU++;
                    }
                    if($mapTerms[$term]["isInSecondString"])
                    {
                        $weightQgramsV += $weight;
                        $numberTermsV++;
                    }
                }
                
                if($numberTermsU > 0)
                    $U += $weightQgramsU / $numberTermsU;
                if($numberTermsV > 0)
                    $V += $weightQgramsV / $numberTermsV;
                $UuV += $weightQgramsUuV / $numberTermsUuV;
            } // at the end of this loop, we have added to U, V and UuV the SC-q-spectrum of the sets for the q of this iteration.
        } // at the end of this loop, we have added to U, V and UuV all the SC-q-spectrum for the range of q. This gives the SC spectra of the sets.
        return (($U + $V - $UuV) * ($U + $V)) / (2 * $U * $V); // harmonic coefficient
    }

    private function getTermWeight($token)
    {
        if($this->corupusSize == 0)
            return 1;
        else
        {
            $documentFrequency = 1;
            if(array_key_exists($token, $this->termsFrequency))
                $documentFrequency = $this->termsFrequency[$token];
            return log($this->corupusSize / $documentFrequency); // IDF: inverse document frequency
        }
    }

    private function getUniqueQgrams($term, $q)
    {
        $qgrams = array();
        $qgram = "";
        for($j = 0; $j < $q && $j < strlen($term); $j++) // the first q-gram is the only one if q is larger than the string length
            $qgram .= $term[$j];
        array_push($qgrams, $qgram);
        
        for($i = 1; $i <= strlen($term) - $q; $i++)
        { // if there is more than one q-gram add them in the set
            $qgram = "";
            for($j = 0; $j < $q; $j++)
                $qgram .= $term[$j + $i];
            array_push($qgrams, $qgram);
        }
        return array_unique($qgrams);
    }
}
