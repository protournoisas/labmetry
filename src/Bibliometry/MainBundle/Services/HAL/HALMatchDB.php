<?php

namespace Bibliometry\MainBundle\Services\HAL;

use Bibliometry\MainBundle\Entity\Laboratory;
use Bibliometry\MainBundle\Entity\Journal;
use Bibliometry\MainBundle\Entity\Researcher;

class HALMatchDB
{
    private $em;
    private $similarity;
    private $googleGeocodingService;
    private $exportLog;

    public function __construct($em, $similarity)
    {
        $this->em = $em;
        $this->similarity = $similarity;
        $this->exportLog = __DIR__ . "/../../../../../web/exportLog.html";
        
        // file_put_contents($this->exportLog, "<ul>", FILE_APPEND);
    }
    
    // Try to find a researcher called firstName, lastName if he/she exists and return it. If doesn't exist, create and return it
    public function findOrCreateResearcher($existingResearchers, $firstName, $lastName)
    {
        // Strange PHP bug : we pass strings, and sometimes, they become an array !
        // We convert it again in strings...
        if(is_array($firstName))
        {
            $firstName = implode(" ", $firstName);
        }
        if(is_array($lastName))
        {
            $lastName = implode(" ", $lastName);
        }
        
        foreach($existingResearchers as $researcher)
        {
            $matchedResearcher = $this->compareNameSurname($researcher, $firstName, $lastName);
            if($matchedResearcher != NULL)
            {
                return $matchedResearcher;
            }
        } // else
        
        $newResearcher = new Researcher();
        $newResearcher->setName($firstName);
        $newResearcher->setSurname($lastName);
        $this->em->persist($newResearcher);
        return $newResearcher;
    }
    
    // Compare the HAL input name/surname with all the alternatives we got in DB
    private function compareNameSurname($researcher, $firstName, $lastName)
    {
        if(levenshtein($researcher->getName(), $firstName) + levenshtein($researcher->getSurname(), $lastName) < 2)
        {
            return $researcher;
        } // else
          
        // Test with all alternative Names/Surnames
        if(count($researcher->getAltNames()) > 0)
        {
            foreach($researcher->getAltNames() as $index => $altName)
            {
                $altSurname = $researcher->getAltSurnames()[$index];
                if(levenshtein($altName, $firstName) + levenshtein($altSurname, $lastName) < 2)
                {
                    return $researcher;
                }
            }
        }
        
        // Not found
        return NULL;
    }
    
    // Try to find a laboratory if it exists and return it. If doesn't exist, create and return it
    public function findOrCreateLaboratory($existingLaboratories, $labName, $HALID)
    {
        foreach($existingLaboratories as $laboratory)
        {
            if($laboratory->getHALID() == $HALID)
            {
                return $laboratory;
            }
        } // else
        $newLaboratory = new Laboratory($labName);
        $newLaboratory->setHALID($HALID);
        
        // We set the team hierarchical level to "Laboratory" when creating a new laboratory
        $hierarchicalLevelRepository = $this->em->getRepository("BibliometryMainBundle:HierarchicalLevel");
        $laboratoryHierarchicalLevel = $hierarchicalLevelRepository->findOneByLevelName("Laboratory");
        $newLaboratory->getLaboratoryTeam()->setHierarchicalLevel($laboratoryHierarchicalLevel);
        $this->em->persist($newLaboratory);
        return $newLaboratory;
    }
    
    // Try to find a journal if it exists and return it. If doesn't exist, create and return it
    public function findOrCreateJournal($existingJournals, $journalTitle, $issn)
    {
        foreach($existingJournals as $journal)
        {
            if($journal->getTitle() == $journalTitle)
            {
                // If the matched journal has been merged with another (duplicate), return the right one
                if($journal->getMergedTo() != NULL)
                {
                    // file_put_contents($this->exportLog, "<li>JOURNAL : <strong>".$journalTitle."</strong> matched with <strong>".$journal->getMergedTo()->getTitle()."</strong></li>".PHP_EOL, FILE_APPEND);
                    return $journal->getMergedTo();
                } // else
                  // file_put_contents($this->exportLog, "<li>JOURNAL : <strong>".$journalTitle."</strong> matched with <strong>".$journal->getTitle()."</strong></li>".PHP_EOL, FILE_APPEND);
                return $journal;
            }
        } // else
        $newJournal = new Journal();
        $newJournal->setTitle($journalTitle);
        if($issn != "")
            $journal->setISSN($issn);
        
        // file_put_contents($this->exportLog, "<li>JOURNAL : <strong>".$journalTitle."</strong> created.</li>".PHP_EOL, FILE_APPEND);
        return $newJournal;
    }
    
    // Try to find a conference in CORE that match the given title and return it if it exists or NULL if not.
    public function findConference($conferenceTitle, $conferencesCORE, $corupusSize, $termsFrequency)
    {
        return $this->similarity->getMatchingConference($conferenceTitle, $conferencesCORE, $corupusSize, $termsFrequency);
    }
}
