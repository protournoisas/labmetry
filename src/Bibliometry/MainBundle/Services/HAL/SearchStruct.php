<?php
// Class for SOAP API Search

namespace Bibliometry\MainBundle\Services\HAL;

class SearchStruct
{
    public $field;
    public $searchType;
    public $value;
    public $join;

    public function __construct($field, $searchType, $value, $join = 'and')
    {
        $this->field = $field;
        $this->searchType = $searchType;
        $this->value = $value;
        $this->join = $join;
    }

}