<?php

namespace Bibliometry\MainBundle\Services\HAL;

use Bibliometry\MainBundle\Entity\Publication;
use Bibliometry\MainBundle\Entity\Laboratory;
use Bibliometry\MainBundle\Entity\PubliJournal;
use Bibliometry\MainBundle\Entity\PubliConference;
use Bibliometry\MainBundle\Entity\PubliOther;
use Bibliometry\MainBundle\Entity\Journal;
use Bibliometry\MainBundle\Entity\Conference;
use Bibliometry\MainBundle\Entity\ConferenceTermFrequency;
use Bibliometry\MainBundle\Entity\Researcher;

class HALV3
{
    private $postRequest;
    private $HALID_lab;
    private $em;
    private $translator;
    private $HALMatchDBService;
    private $useWOK = false;
    private $WOKService;

    public function __construct($postRequest, $HALID_lab, $em, $translator, $HALMatchDBService, $WOKService)
    {
        $this->postRequest = $postRequest;
        $this->HALID_lab = $HALID_lab;
        $this->em = $em;
        $this->translator = $translator;
        $this->HALMatchDBService = $HALMatchDBService;
        $this->WOKService = $WOKService;
    }
    
    // Method to query the HAL V3 API and return an array of publications
    private function getJSONPublications($year, $offset)
    {
        $JSONUrl = ("http://api.archives-ouvertes.fr/search/index?q=structId_i:"
                    . $this->HALID_lab . "%20AND%20(docType_s:ART%20OR%20docType_s:COMM%20OR%20docType_s:OUV%20OR%20docType_s:COUV%20OR%20docType_s:DOUV%20OR%20docType_s:THESE%20OR%20docType_s:HDR%20OR%20docType_s:POSTER)%20AND%20producedDateY_i:"
                    . $year . "&wt=json&rows=200&start="
                    . $offset . "&fl=halId_s,version_i,modifiedDate_s,uri_s,files_s,abstract_s,title_s,authLastName_s,authFirstName_s,authStructId_i,labStructName_s,labStructId_i,docType_s,producedDateY_i,volume_s,issue_s,page_s,journalTitle_s,journalIssn_s,journalTitleAbbr_s,city_s,country_s,conferenceTitle_s,domain_s,language_s,bookTitle_s,publisher_s,director_s,producedDateM_i%20AND%20structCountry_s%20AND%20sort=halId_s%20asc");
        // Query the API
        $JSONresultsPage = file_get_contents($JSONUrl, false, $this->postRequest->getProxy());
        // API unreachable error
        if($JSONresultsPage === false)
        {
            throw new \Exception($this->translator->trans('bibliometry.admin.import_hal.error_api_down'));
        }
        
        // Parse the JSON
        $parsedJSON = json_decode($JSONresultsPage, TRUE);
        // Unvalid JSON (error page from HAL for example)
        if($parsedJSON === NULL)
        {
            throw new \Exception($this->translator->trans('bibliometry.admin.import_hal.error_api_down'));
        }
        
        return $parsedJSON;
    }
    
    // From an associative array (thanks to JSON deconding) we parse it and store publications in DB
    private function parseAndStoreInDBJSONPublications($publications, $totalNumberOfPublications, $totalProgress, $outputInterface, $outputOffset, $outputFactor)
    {
        $conferencesRepository = $this->em->getRepository("BibliometryMainBundle:Conference");
        $conferencesCORE = $conferencesRepository->findAll();
        $corupusSize = count($conferencesCORE); // used to compute the Inverse Document Frequency of a token during the computation of conference similarity
        $conferenceCoreTermsFrequencyRepository = $this->em->getRepository("BibliometryMainBundle:ConferenceTermFrequency");
        $termsFrequency = array(); // used to compute the Inverse Document Frequency of a token during the computation of conference similarity
        foreach($conferenceCoreTermsFrequencyRepository->findAll() as $termFrequencyObjet)
            $termsFrequency[$termFrequencyObjet->getTerm()] = $termFrequencyObjet->getFrequency();
        
        foreach($publications["response"]["docs"] as $publication)
        {
            // Write progress
            $totalProgress++;
            $outputInterface->writeln((round(($totalProgress) * 100 / $totalNumberOfPublications)) * $outputFactor + $outputOffset);
            
            // Search for an already existing publication with the same ID
            $publicationRepository = $this->em->getRepository("BibliometryMainBundle:Publication");
            $DBPubli = $publicationRepository->findOneByHALId($publication["halId_s"]);
            
            // If there is an existing one
            if($DBPubli != NULL)
            {
                if($DBPubli->getNotFromLab())
                {
                    continue;
                }
                // We check the version, if it has changed...
                if($DBPubli->getVersion() < $publication["version_i"] || $DBPubli->getLastModificationDate() < new \DateTime($publication["modifiedDate_s"]))
                {
                    // We remove the informations that may have changed (authors, labs, journals/conferences) but keep the merge infos
                    // The other informations (title, abstract...) will be overwritten
                    foreach($DBPubli->getPublicationAuthors() as $publicationAuthor)
                    {
                        $publicationAuthor->setResearcher(null);
                        $this->em->remove($publicationAuthor);
                    }
                    if($DBPubli->getType() == "conference")
                    {
                        $this->em->remove($DBPubli->getPubliConference());
                        $DBPubli->getPubliConference()->setPublication(null);
                    }
                    else if($DBPubli->getType() == "journal")
                    {
                        $this->em->remove($DBPubli->getPubliJournal());
                        $DBPubli->getPubliJournal()->setPublication(null);
                    }
                    else if($DBPubli->getType() != NULL)
                    {
                        $this->em->remove($DBPubli->getPubliOther());
                        $DBPubli->getPubliOther()->setPublication(null);
                    }
                    $DBPubli->removeLaboratories();
                    
                    $this->em->flush();
                    
                    $newPublication = $DBPubli;
                }
                else
                {
                    if(array_key_exists("structCountry_s", $publication))
                    {
                        $nationalities = array();
                        foreach($publication["structCountry_s"] as $index1 => $country)
                        {
                            $nationalities[$country] = true;
                        }
                        ksort($nationalities);
                        $affiliationsNationalities = "";
                        foreach($nationalities as $country => $trueVal)
                        {
                            if($affiliationsNationalities == "")
                            {
                                $affiliationsNationalities = $country;
                            }
                            else
                            {
                                $affiliationsNationalities .= ";" . $country;
                            }
                        }
                        $DBPubli->setAffiliationsNationalities($affiliationsNationalities);
                    }
                    continue;
                }
            }
            else
            {
                // We create a new publication in DB
                $newPublication = new Publication();
            }
            
            $this->em->persist($newPublication);
            $newPublication->setHALId($publication["halId_s"]);
            $newPublication->setVersion($publication["version_i"]);
            $newPublication->setLastModificationDate(new \DateTime($publication["modifiedDate_s"]));
            $newPublication->setYear($publication["producedDateY_i"]);
            if(!array_key_exists("producedDateM_i", $publication) || $publication["producedDateM_i"] == 0)
            {
                $newPublication->setMonth(1);
            }
            else
            {
                $newPublication->setMonth($publication["producedDateM_i"]);
            }

            if(array_key_exists("page_s", $publication))
            {
                $newPublication->setPages($publication["page_s"]);
            }
            if(array_key_exists("uri_s", $publication))
            {
                $newPublication->setHALURL($publication["uri_s"]);
            }
            if(array_key_exists("files_s", $publication))
            {
                $newPublication->setPDFURL($publication["files_s"][0]);
            }
            $newPublication->setTitle($publication["title_s"][0]);
            if(array_key_exists("abstract_s", $publication))
            {
                $newPublication->setAbstract($publication["abstract_s"][0]);
            }
            if(array_key_exists("language_s", $publication))
            {
                $newPublication->setLanguage($publication["language_s"][0]);
            }
            if(array_key_exists("domain_s", $publication))
            {
                preg_match("/\d\.([a-z]+)/", $publication["domain_s"][0], $domain);
                $domain = $domain[1];
                $fieldsRepository = $this->em->getRepository("BibliometryMainBundle:ResearchField");
                
                $field = $fieldsRepository->findOneByAbbreviation($domain);
                if($field != NULL)
                {
                    $newPublication->setResearchField($field);
                }
            }
            if(array_key_exists("structCountry_s", $publication))
            {
                $nationalities = array();
                foreach($publication["structCountry_s"] as $index1 => $country)
                {
                    $nationalities[$country] = true;
                }
                ksort($nationalities);
                $affiliationsNationalities = "";
                foreach($nationalities as $country => $trueVal)
                {
                    if($affiliationsNationalities == "")
                    {
                        $affiliationsNationalities = $country;
                    }
                    else
                    {
                        $affiliationsNationalities .= ";" . $country;
                    }
                }
                $newPublication->setAffiliationsNationalities($affiliationsNationalities);
            }
            
            // Processing the laboratories
            $laboratoriesRepository = $this->em->getRepository("BibliometryMainBundle:Laboratory");
            $existingLaboratories = $laboratoriesRepository->findAll();
            
            foreach($publication["labStructName_s"] as $index => $laboratory)
            {
                // We retrieve a lab by its ID
                $foundLab = $this->HALMatchDBService->findOrCreateLaboratory($existingLaboratories, $laboratory, $publication["labStructId_i"][$index]);
                if(!$newPublication->getLaboratories()->contains($foundLab))
                {
                    $newPublication->addLaboratory($foundLab);
                }
            }
            $this->em->flush();
            
            $researchersRepository = $this->em->getRepository("BibliometryMainBundle:Researcher");
            $existingResearchers = $researchersRepository->findAll();
            
            // Processing the authors
            foreach($publication["authLastName_s"] as $index => $lastName)
            {
                $firstName = $publication["authFirstName_s"][$index];
                $foundAuthor = $this->HALMatchDBService->findOrCreateResearcher($existingResearchers, $firstName, $lastName);
                if(!in_array($foundAuthor, $newPublication->getAuthors()))
                {
                    // $foundAuthor->addPublication($newPublication);
                    $newPublication->addAuthor($foundAuthor, $index + 1);
                }
                if(array_key_exists("authStructId_i", $publication) && count($publication['authStructId_i']) == count($publication['authLastName_s']))
                {
                    $matchedLaboratory = $laboratoriesRepository->findOneBy(array(
                            "HALID" => $publication['authStructId_i'][$index]
                    ));
                    if($matchedLaboratory != NULL && !in_array($foundAuthor, $matchedLaboratory->getLaboratoryTeam()->getResearchers()))
                    {
                        $matchedLaboratory->getLaboratoryTeam()->addResearcher($foundAuthor);
                    }
                }
            }
            
            // Processing the journal
            if($publication["docType_s"] == "ART")
            {
                $newPubliJournal = new PubliJournal();
                if(array_key_exists("volume_s", $publication))
                {
                    $newPubliJournal->setVolume($publication["volume_s"]);
                }
                if(array_key_exists("issue_s", $publication))
                {
                    $newPubliJournal->setNumber($publication["issue_s"][0]);
                }
                
                $journalsRepository = $this->em->getRepository("BibliometryMainBundle:Journal");
                $existingJournals = $journalsRepository->findAll();
                if(array_key_exists("journalTitle_s", $publication))
                {
                    $issn = "";
                    if(array_key_exists("journalIssn_s", $publication))
                        $issn = $publication["journalIssn_s"];
                    $journal = $this->HALMatchDBService->findOrCreateJournal($existingJournals, $publication["journalTitle_s"], $issn);
                    if(array_key_exists("journalTitleAbbr_s", $publication))
                    {
                        $journal->setISOAbbreviation($publication["journalTitleAbbr_s"]);
                    }
                    
                    if($this->useWOK)
                    {
                        $this->WOKService->importOneJournal($journal, "", "");
                    }
                }
                else
                {
                    $journal = $journalsRepository->findOneByTitle("Other");
                }
                $newPublication->setPubliJournal($newPubliJournal);
                $newPubliJournal->setJournal($journal);
            }
            
            // Processing the conference
            if($publication["docType_s"] == "COMM" || $publication["docType_s"] == "POSTER")
            {
                $newPubliConference = new PubliConference();
                $conference = NULL;
                if(array_key_exists("city_s", $publication))
                {
                    $newPubliConference->setCity($publication["city_s"]);
                }
                if(array_key_exists("country_s", $publication))
                {
                    $newPubliConference->setCountry($publication["country_s"]);
                }
                
                if(array_key_exists("conferenceTitle_s", $publication))
                {
                    if($publication["docType_s"] == "COMM")
                        $conference = $this->HALMatchDBService->findConference($publication["conferenceTitle_s"], $conferencesCORE, $corupusSize, $termsFrequency);
                    $newPubliConference->setTitleOfConferenceInHAL($publication["conferenceTitle_s"]);
                }
                else
                {
                    $newPubliConference->setTitleOfConferenceInHAL("Conference title from HAL not found");
                }
                $newPublication->setPubliConference($newPubliConference);
                if($conference != NULL)
                {
                    $newPubliConference->setConference($conference);
                }
            }
            
            // Processing the books and thesis/HDR
            if($publication["docType_s"] == "OUV" || $publication["docType_s"] == "COUV" || $publication["docType_s"] == "DOUV" || $publication["docType_s"] == "THESE" || $publication["docType_s"] == "HDR")
            {
                $newPubliOther = new PubliOther();
                // Books
                if($publication["docType_s"] == "OUV" || $publication["docType_s"] == "COUV" || $publication["docType_s"] == "DOUV")
                {
                    if(array_key_exists("publisher_s", $publication))
                    {
                        $newPubliOther->setPublisher($publication["publisher_s"][0]);
                    }
                    if($publication["docType_s"] == "OUV")
                    {
                        $newPubliOther->setType('book');
                    }
                    else if($publication["docType_s"] == "COUV")
                    {
                        $newPubliOther->setBookTitle($publication["bookTitle_s"]);
                        $newPubliOther->setType('chapter');
                    }
                    else if($publication["docType_s"] == "DOUV")
                    {
                        $newPubliOther->setType('direction_book');
                    }
                }
                else if($publication["docType_s"] == "THESE" || $publication["docType_s"] == "HDR")
                {
                    $supervisors = explode(";", $publication["director_s"][0]);
                    foreach($supervisors as $supervisor)
                    {
                        $supervisorNames = explode(" ", $supervisor);
                        $newPubliOther->addSupervisor($this->HALMatchDBService->findOrCreateResearcher($existingResearchers, $supervisorNames[0], array_slice($supervisorNames, 1)));
                    }
                    if($publication["docType_s"] == "THESE")
                    {
                        $newPubliOther->setType('thesis');
                    }
                    else if($publication["docType_s"] == "HDR")
                    {
                        $newPubliOther->setType('hdr');
                    }
                }
                
                $newPublication->setPubliOther($newPubliOther);
            }
            
            $this->em->persist($newPublication);
            $this->em->flush();
        }
        return $totalProgress;
    }

    public function importPublications($beginYear, $endYear, $useWOK, $outputInterface, $outputOffset = 0, $outputFactor = 1)
    {
        try
        {
            $this->useWOK = $useWOK;
            // Compute the total number of publications to parse (for progress percentage)
            $totalNumberOfPublications = 0;
            for($year = $beginYear; $year <= $endYear; $year++)
            {
                $totalNumberOfPublications += $this->getNumberOfPublications($year);
            }
            
            // Process the publications for each year
            $totalProgress = 0;
            for($year = $beginYear; $year <= $endYear; $year++)
            {
                $progress = 0;
                $numberOfPublications = $this->getNumberOfPublications($year);
                for($offset = 0; $offset <= $numberOfPublications; $offset += 200)
                {
                    $publications = $this->getJSONPublications($year, $offset);
                    $totalProgress = $this->parseAndStoreInDBJSONPublications($publications, $totalNumberOfPublications, $totalProgress, $outputInterface, $outputOffset, $outputFactor);
                }
            }
            $outputInterface->writeln(100 * $outputFactor + $outputOffset);
        }catch(\Exception $e)
        {
            $outputInterface->writeln("ERROR : " . $e->getMessage());
            throw $e;
        }
    }

    private function getNumberOfPublications($year)
    {
        $JSONUrl = "http://api.archives-ouvertes.fr/search/index?q=structId_i:" . $this->HALID_lab . "%20AND%20(docType_s:ART%20OR%20docType_s:COMM%20OR%20docType_s:OUV%20OR%20docType_s:COUV%20OR%20docType_s:DOUV%20OR%20docType_s:THESE%20OR%20docType_s:HDR%20OR%20docType_s:POSTER)%20AND%20producedDateY_i:" . $year . "&wt=json&rows=0";
        
        // Query the API
        $JSONresultsPage = file_get_contents($JSONUrl, false, $this->postRequest->getProxy());
        // API unreachable error
        if($JSONresultsPage === false)
        {
            throw new \Exception($this->translator->trans('bibliometry.admin.import_hal.error_api_down'));
        }
        
        // Parse the JSON
        $parsedJSON = json_decode($JSONresultsPage, TRUE);
        // Unvalid JSON (error page from HAL for example)
        if($parsedJSON === NULL)
        {
            throw new \Exception($this->translator->trans('bibliometry.admin.import_hal.error_api_down'));
        }
        
        return $parsedJSON["response"]["numFound"];
    }
}
