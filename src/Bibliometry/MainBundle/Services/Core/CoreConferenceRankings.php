<?php

namespace Bibliometry\MainBundle\Services\Core;

use Bibliometry\MainBundle\Entity\Conference;
use Bibliometry\MainBundle\Entity\ConferenceRanking;
use Bibliometry\MainBundle\Entity\ConferenceTermFrequency;

class CoreConferenceRankings
{
    private $postRequest;
    private $em;
    private $similarity;
    private $numberOfConferences;
    private $translator;
    private $CoreUrl;

    public function __construct($postRequest, $em, $similarity, $translator, $CoreUrl)
    {
        $this->postRequest = $postRequest;
        $this->em = $em;
        $this->similarity = $similarity;
        $this->translator = $translator;
        $this->CoreUrl = $CoreUrl;
    }

    private function getCSVCore()
    {
        $fileName = "CoreDB" . time();
        $path = __DIR__ . "/../../../../../web/uploads/CSV/" . $fileName;
        
        // Get the CSV file
        $CSVFile = fopen($this->CoreUrl, 'r', false, $this->postRequest->getProxy());
        // Fail to get the file
        if($CSVFile === false)
        {
            throw new \Exception($this->translator->trans('bibliometry.admin.import_core.error_csv_not_found'));
        }
        
        file_put_contents($path, $CSVFile);
        return $path;
    }

    private function parseCSVCore($CSVPath, $outputInterface)
    {
        $CSVFile = fopen($CSVPath, 'r');
        if($CSVFile === false)
        {
            throw new \Exception($this->translator->trans('bibliometry.admin.import_core.error_file_not_found'));
        }
        else
        {
            // Count the number of lines in file to get the number of conferences
            $this->numberOfConferences = 0;
            $handle = fopen($CSVPath, "r");
            while(!feof($handle))
            {
                $line = fgets($handle);
                $this->numberOfConferences++;
            }
            fclose($handle);
            
            $conferences = array();
            $CSVLine = fgetcsv($CSVFile, 0, ",", '"');
            $progress = 50;
            $validRanks = [
                    "A*",
                    "A",
                    "B",
                    "C"
            ];
            while($CSVLine !== false)
            {
                $conference = array();
                $conference["title"] = $CSVLine[1];
                $conference["acronym"] = $CSVLine[2];
                preg_match("/\d{4}$/", $CSVLine[3], $version);
                $conference["year"] = $version[0];
                $conference["rank"] = $CSVLine[4];
                // We are only interested in the valid ranks: "A*", "A", "B", "C", and not the ERA2010 ranking
                if(in_array($conference["rank"], $validRanks) && $conference["year"] != 2010)
                {
                    array_push($conferences, $conference);
                }
                $CSVLine = fgetcsv($CSVFile, 0, ",", '"');
                
                $progress += 1. / $this->numberOfConferences * 10;
                $outputInterface->writeln(round($progress));
            }
            return $conferences;
        }
    }

    private function storeConferencesInDB($conferences, $outputInterface)
    {
        $conferencesRepository = $this->em->getRepository("BibliometryMainBundle:Conference");
        $conferencesTermFrequencyRepository = $this->em->getRepository("BibliometryMainBundle:ConferenceTermFrequency");
        $mapTermFrequency = array();
        
        $progress = 60;
        foreach($conferences as $conference)
        {
            $existingConference = $conferencesRepository->findOneByTitle($conference["title"]);   
            if($existingConference != NULL)
            {
                if($existingConference->getLastConferenceRanking()->getYear() != $conference["year"])
                {
                    $newRanking = new ConferenceRanking();
                    $newRanking->setYear($conference["year"]);
                    $newRanking->setValue($conference["rank"]);
                    $existingConference->addConferenceRanking($newRanking);
                    $this->em->persist($existingConference);
                }
                $progress += 1. / $this->numberOfConferences * 40;
                $outputInterface->writeln(round($progress));
                continue;
            }
            else
            {
                $newConference = new Conference();
                if($conference["acronym"] == "ECML")
                { // Particular case for ECML/PKDD (distinct conferences for Core, but actually the same one)
                    $newConference->setTitle("European Conference on Machine Learning and Principles and Practice of Knowledge Discovery");
                    $newConference->setPreProcessedTitle($this->similarity->preProcess("European Conference on Machine Learning and Principles and Practice of Knowledge Discovery"));
                    $newConference->setAcronym("ECML-PKDD");
                }
                else if($conference["acronym"] == "PKDD")
                {
                    continue;
                }
                else
                {
                    $newConference->setTitle($conference["title"]);
                    $newConference->setPreProcessedTitle($this->similarity->preProcess($conference["title"]));
                    $newConference->setAcronym($conference["acronym"]);
                }
                $this->incrementFrequencyOfTermsInConferenceTitle($newConference->getPreProcessedTitle(), $mapTermFrequency);
                $ranking = new ConferenceRanking();
                $ranking->setYear($conference["year"]);
                $ranking->setValue($conference["rank"]);
                $newConference->addConferenceRanking($ranking);
                $this->em->persist($newConference);
            }
            $progress += 1. / $this->numberOfConferences * 40;
            $outputInterface->writeln(round($progress));
        }
        
        foreach($mapTermFrequency as $term => $frequency)
        {
            $termFrequency = $conferencesTermFrequencyRepository->findOneByTerm($term);
            if($termFrequency == null)
            { // term not in database -> add a new row for this term with the frequency computed in the map
                $termFrequency = new ConferenceTermFrequency();
                $termFrequency->setTerm($term);
                $termFrequency->setFrequency($frequency);
                $this->em->persist($termFrequency);
            }
            else
            { // term already in database -> update the row by adding the frequency computed in the map
                $termFrequency->setFrequency($termFrequency->getFrequency() + $frequency);
            }
        }
        $this->em->flush();
    }

    private function incrementFrequencyOfTermsInConferenceTitle($preProcessedTitle, &$mapTermFrequency)
    { // given the title, for each term (no duplicate), increase its frequency in the map passed by reference
        foreach(array_unique(explode(' ', $preProcessedTitle)) as $term)
        {
            if(!array_key_exists($term, $mapTermFrequency))
            { // term not present -> add it with a frequency of 1
                $mapTermFrequency[$term] = 1;
            }
            else
            { // term already present -> increment its frequency
                $mapTermFrequency[$term]++;
            }
        }
    }

    public function importConferences($outputInterface)
    {
        try
        {
            $CSVFile = $this->getCSVCore();
            $outputInterface->writeln(50);
            $conferences = $this->parseCSVCore($CSVFile, $outputInterface);
            $outputInterface->writeln(60);
            $this->storeConferencesInDB($conferences, $outputInterface);
            $outputInterface->writeln(100);
            unlink($CSVFile);
        }catch(\Exception $e)
        {
            $outputInterface->writeln("ERROR : " . $e->getMessage());
            throw $e;
        }
    }
}
