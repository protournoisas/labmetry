<?php

namespace Bibliometry\MainBundle\Services\GoogleScholar;

use Bibliometry\MainBundle\Entity\Researcher;

class GoogleScholar
{
    private $postRequest;
    private $em;
    private $translator;

    public function __construct($postRequest, $em, $translator)
    {
        $this->postRequest = $postRequest;
        $this->em = $em;
        $this->translator = $translator;
    }

    private function getGoogleProfile($researcher, $proxyConfig, $output)
    {
        // Search the researcher according to name and surname
        $url = "https://scholar.google.com/citations?view_op=search_authors&mauthors=" . urlencode($researcher->getName()) . "+" . urlencode($researcher->getSurname());
        $googleResultsPage = shell_exec($proxyConfig . " curl -sS '" . $url . "' --compressed --connect-timeout 10 --fail");
        // If not reachable
        if($googleResultsPage === false)
            throw new \Exception($this->translator->trans('bibliometry.admin.import_scholar.error_google_down'));
        // $output->writeln("\n\ngetGoogleProfile\n".$googleResultsPage);
        // Get the number of results
        preg_match_all("/<h3 class=\"gs_ai_name\">/", $googleResultsPage, $results);
        if(empty($results))
            return NULL;
        else
            $numberOfResults = count($results[0]);
        // If several results, ambiguity, we return NULL
        if($numberOfResults > 1 || $numberOfResults == 0)
            return NULL;
        // Take the first result
        $googleResultsPage = str_replace("<span class='gs_hlt'>", "", $googleResultsPage);
        $googleResultsPage = str_replace("</span>", "", $googleResultsPage);
        preg_match_all("/<h3 class=\"gs_ai_name\"><a href=\"\/citations\?hl=fr&amp;user=(.*?)\">(.*?)<\/a><\/h3>/", $googleResultsPage, $result);
        // Get the researcher name
        $researcherName = explode(" ", strtolower(strip_tags($result[2][0])));
        // Check if name/surname match with first or second part of the Google result
        if(($researcherName[0] != strtolower($researcher->getName()) && $researcherName[1] != strtolower($researcher->getName())) || ($researcherName[0] != strtolower($researcher->getSurname()) && $researcherName[1] != strtolower($researcher->getSurname())))
            return NULL;
        // Get the URL and store in DB
        $scholarURL = "https://scholar.google.com/citations?hl=en&user=" . $result[1][0];
        $researcher->setGoogleProfile($scholarURL);
        $this->em->persist($researcher);
    }

    private function getHIndex($researcher, $proxyConfig, $output)
    {
        // Get Google profile page
        $googleProfilePage = shell_exec($proxyConfig . " curl -sS '" . $researcher->getGoogleProfile() . "' --compressed --connect-timeout 10 --fail");
        // If not reachable
        if($googleProfilePage === false)
            throw new \Exception($this->translator->trans('bibliometry.admin.import_scholar.error_google_down'));
        // Retrieve the h-index
        preg_match("/h-index<\/a><\/td><td class=\"gsc_rsb_std\">(\d+)/", $googleProfilePage, $hIndex);
        if(isset($hIndex[1]))
        {
            $researcher->setHIndex($hIndex[1]);
            $this->em->persist($researcher);
        }
    }

    public function updateHIndex($researchers, $outputInterface)
    {
        $proxyConfig = "";
        if($this->postRequest->hasProxy)
        {
            $url = $this->postRequest->proxy_url;
            $port = $this->postRequest->proxy_port;
            $proxyConfig = "export http_proxy='" . $url . ":" . $port . "';";
            $proxyConfig .= "https_proxy='" . $url . ":" . $port . "';";
        }
        $progress = 0;
        $nbOfResearchers = count($researchers);
        try
        {
            foreach($researchers as $researcher)
            {
                $outputInterface->writeln(round($progress));
                // Get profile URL
                if($researcher->getGoogleProfile() == NULL)
                    $this->getGoogleProfile($researcher, $proxyConfig, $outputInterface);
                // Get HIndex
                if($researcher->getGoogleProfile() != NULL)
                    $this->getHIndex($researcher, $proxyConfig, $outputInterface);
                $progress += 1. / $nbOfResearchers * 100;
                // Sleep some time to prevent from being blocked by google scholar
                sleep(10);
            }
        }catch(\Exception $e)
        {
            $this->em->flush();
            $outputInterface->writeln("ERROR : " . $e->getMessage());
            throw $e;
        }
        $this->em->flush();
    }
}
