<?php

namespace Bibliometry\MainBundle\Services\PostRequest;

class PostRequest
{
	public $hasProxy;

	public $proxy_url;

	public $proxy_port;

	private $timeout = 10;

	public function __construct($hasProxy,$proxy_url,$proxy_port)
	{
		$this->hasProxy = $hasProxy;
		$this->proxy_url = $proxy_url;
		$this->proxy_port = $proxy_port;
	}

    public function getPostRequest($data)
    {

        if ($this->hasProxy)
        {
            $request = array(
					'proxy' => 'tcp://'.$this->proxy_url.':'.$this->proxy_port,
					'request_fulluri' => true,
					);
        }
        else
        {
            $request = array();
        }
        $postRequest = array(
                        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
                        'method'  => 'POST',
                        'content' => http_build_query($data),
                        'timeout' => $this->timeout,
                    );
        $request = array_merge($request,$postRequest);
        $aContext = array('http' => $request);
		$cxContext = stream_context_create($aContext);

        return $cxContext;
    }

    public function getProxy()
	{
        if ($this->hasProxy)
        {
            $aContext = array(
                        'http' => array(
                        'proxy' => 'tcp://'.$this->proxy_url.':'.$this->proxy_port,
                        'request_fulluri' => true,
                        'timeout' => $this->timeout,
                        ),
                    );
            $cxContext = stream_context_create($aContext);

            return $cxContext;
        }
        else
        {
            return stream_context_create(array());
        }
	}

	public function getProxyForSoap()
	{
		if ($this->hasProxy)
		{
			return array('exceptions' => 0, 'proxy_host' => $this->proxy_url, 'proxy_port' => $this->proxy_port);
		}
		else
		{
			return array('exceptions' => 0);
		}
	}

}
