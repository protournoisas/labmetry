<?php

namespace Bibliometry\MainBundle\Services\ResearchGate;

class ResearchGate
{
	private $postRequest;
    private $stopWordsService;
    private $researchGateUrl;
    
	public function __construct($postRequest, $stopWordsService, $researchGateUrl)
	{
        $this->postRequest = $postRequest;
        $this->stopWordsService = $stopWordsService;
        $this->researchGateUrl = $researchGateUrl;
	}
    
    public function getISSN($journalTitle, $proxyConfig)
    {
        // If "conference" contained in title
        if (strpos(strtolower($journalTitle), "conference") !== FALSE)
        {
            return NULL;
        }
        $url = $this->researchGateUrl."?type=journal&search-keyword=&search-abstract=&search-journal=".urlencode($journalTitle)."&search=Search";
        /*$curlRequest = curl_init();
        curl_setopt($curlRequest, CURLOPT_URL, $url);
        curl_setopt($curlRequest, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($curlRequest, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($curlRequest, CURLOPT_TIMEOUT, 10);
        curl_setopt($curlRequest, CURLOPT_FAILONERROR, TRUE);
        $researchGateResultsPage = curl_exec($curlRequest);

        if(curl_errno($curlRequest) != 0)
        {
            return NULL;
        }*/

        $researchGateResultsPage = shell_exec($proxyConfig." curl -sS '".$url."'");
        
        // Match the titles of the results
        preg_match_all("/\<a class=\"bold\" href=\"journal\/[^\"]+\"\>([^\<]+)\<\/a\>/", $researchGateResultsPage, $matchedTitles);
        
        // No result, return NULL
        if (empty($matchedTitles[1]))
        {
            return NULL;
        }
        
        // Match the ISSNs
        preg_match_all("/\<p class=\"small-text\">ISSN: (\d+\-\d+X?)/", $researchGateResultsPage, $matchedISSN);
        
        // We take the first result index that has the exact same title
        $matchedJournal = 0;
        $found = false;
        foreach ($matchedTitles[1] as $foundTitle)
        {
            $foundTitleProcessed = preg_replace("/[^[:alpha:]+]+/u", " ", $foundTitle);
            $journalTitleProcessed = preg_replace("/[^[:alpha:]+]+/u", " ", $journalTitle);
            
            $foundTitleProcessed = preg_replace('/\s+/', ' ', $foundTitleProcessed);
            $journalTitleProcessed = preg_replace('/\s+/', ' ', $journalTitleProcessed);
            
            $foundTitleProcessed = explode(' ', $foundTitleProcessed);
            $foundTitleProcessed = $this->stopWordsService->removeStopWordsKeywords($foundTitleProcessed);
            $foundTitleProcessed = strtolower(implode(' ', $foundTitleProcessed));
            
            $journalTitleProcessed = explode(' ', $journalTitleProcessed);
            $journalTitleProcessed = $this->stopWordsService->removeStopWordsKeywords($journalTitleProcessed);
            $journalTitleProcessed = strtolower(implode(' ', $journalTitleProcessed));
            
            if ($foundTitleProcessed != $journalTitleProcessed)
            {   
                $matchedJournal++;
            }
            else
            {
                $found = true;
                break;
            }
        }
        
        // If we haven't found our journal in the results, retrun empty array
        if (! $found)
        {
            return array();
        }
        
        // We take its ISSN
        $toReturn = array();
        $toReturn[] = $matchedISSN[1][$matchedJournal];
        
        // If the journal has 2 ISSN (print and electronic), take the other also.
        if (array_key_exists($matchedJournal + 1, $matchedTitles[1]))
        {
            $foundTitle2Processed = preg_replace("/[^[:alpha:]+]+/u", " ", $matchedTitles[1][$matchedJournal + 1]);
            $foundTitle2Processed = preg_replace('/\s+/', ' ', $foundTitle2Processed);
        
            $foundTitle2Processed = explode(' ', $foundTitle2Processed);
            $foundTitle2Processed = $this->stopWordsService->removeStopWordsKeywords($foundTitle2Processed);
            $foundTitle2Processed = strtolower(implode(' ', $foundTitle2Processed));
        
            if ($foundTitle2Processed == $journalTitleProcessed)
            {
                $toReturn[] = $matchedISSN[1][$matchedJournal + 1];
            }
        }
        curl_close($curlRequest);
        return $toReturn;       
    }
}
