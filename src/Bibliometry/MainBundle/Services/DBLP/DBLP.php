<?php

namespace Bibliometry\MainBundle\Services\DBLP;

use Bibliometry\MainBundle\Entity\Researcher;

class DBLP
{
    private $postRequest;
    private $em;
    private $translator;
    private $dblpUrl;

    public function __construct($postRequest, $em, $translator)
    {
        $this->postRequest = $postRequest;
        $this->em = $em;
        $this->translator = $translator;
        $this->dblpUrl = "http://dblp.uni-trier.de";
    }

    private function getDblpProfile($researcher, $proxyConfig, $outputInterface)
    {
        // Search the researcher according to name and surname
        $url = $this->dblpUrl . "/search/author/api?q=" . $researcher->getName() . "+" . $researcher->getSurname() . "&h=1000&format=json";
        $dblpJsonResult = shell_exec($proxyConfig . " curl -sS \"" . $url . "\" --compressed --connect-timeout 10 --fail");
        if($dblpJsonResult === false) // If not reachable
            throw new \Exception($this->translator->trans('bibliometry.admin.import_dblp.error_dblp_down'));
        $decodedJson = json_decode($dblpJsonResult);
        $numberOfResults = null; // Get the number of results
        if($decodedJson && $decodedJson->{"result"} && $decodedJson->{"result"}->{"hits"})
            $numberOfResults = $decodedJson->{"result"}->{"hits"}->{"@total"};
        if($numberOfResults == null || $numberOfResults != 1)
            return NULL;
        $url = $decodedJson->{"result"}->{"hits"}->{"hit"}[0]->{"info"}->{"url"};
        $researcher->setDblpProfile($url);
        $this->em->persist($researcher);
    }

    public function importDblpProfile($researchers, $outputInterface)
    {
        $proxyConfig = "";
        if($this->postRequest->hasProxy)
        {
            $url = $this->postRequest->proxy_url;
            $port = $this->postRequest->proxy_port;
            $proxyConfig = "export http_proxy='" . $url . ":" . $port . "';";
            $proxyConfig .= "https_proxy='" . $url . ":" . $port . "';";
        }
        $progress = 0;
        $nbOfResearchers = count($researchers);
        try
        {
            foreach($researchers as $researcher)
            {
                $outputInterface->writeln(round($progress));
                // Get profile URL
                if($researcher->getDblpProfile() == NULL)
                    $this->getDblpProfile($researcher, $proxyConfig, $outputInterface);
                $progress += 1. / $nbOfResearchers * 100;
            }
        }catch(\Exception $e)
        {
            $this->em->flush();
            $outputInterface->writeln("ERROR : " . $e->getMessage());
            throw $e;
        }
        
        $this->em->flush();
    }
}
