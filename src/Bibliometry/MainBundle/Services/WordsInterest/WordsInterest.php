<?php

namespace Bibliometry\MainBundle\Services\WordsInterest;

use Bibliometry\MainBundle\Entity\Researcher;

class WordsInterest
{
	private $stopWordsService;
	
	public function __construct($stopWordsService)
	{
        $this->stopWordsService = $stopWordsService;
	}
	
	public function getWordsInterest($publications)
	{
	    $words = array();
        foreach ($publications as $publication)
        {
            if ($publication["language"] == "en")
            {
                $titleWords = explode(" ", $publication["title"]);
                $titleWordsLowercase = array_map("strtolower", $titleWords);
                $titleWordsWithoutStopWords = $this->stopWordsService->removeStopWordsKeywords($this->stopWordsService->removeDigits($titleWordsLowercase));
                foreach ($titleWordsWithoutStopWords as $word)
                {
                    if (isset($words[$word]))
                    {
                        $words[$word]++;
                    }
                    else
                    {
                        $words[$word] = 1;
                    }
                }
            }
        }
        arsort($words);
        return array("maxOccurences" => empty($words) ? 0 : max($words), "words" => array_slice($words, 0, 20));
	}
}
