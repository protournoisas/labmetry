<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * HierarchicalLevel
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\HierarchicalLevelRepository")
 */
class HierarchicalLevel
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="levelName", type="string", length=255)
     */
    private $levelName;
    
    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\Team", mappedBy="hierarchicalLevel")
    */
    private $teams;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set levelName
     *
     * @param string $levelName
     * @return HierarchicalLevel
     */
    public function setLevelName($levelName)
    {
        $this->levelName = $levelName;

        return $this;
    }

    /**
     * Get levelName
     *
     * @return string 
     */
    public function getLevelName()
    {
        return $this->levelName;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add teams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $teams
     * @return HierarchicalLevel
     */
    public function addTeam(\Bibliometry\MainBundle\Entity\Team $teams)
    {
        $this->teams[] = $teams;

        return $this;
    }

    /**
     * Remove teams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $teams
     */
    public function removeTeam(\Bibliometry\MainBundle\Entity\Team $teams)
    {
        $this->teams->removeElement($teams);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeams()
    {
        return $this->teams;
    }
}
