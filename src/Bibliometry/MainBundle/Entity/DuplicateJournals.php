<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Bibliometry\MainBundle\Validator\Constraints as ErrorsAssert;

/**
 * DuplicateJournals
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ErrorsAssert\DifferentRightWrongJournal
 */
class DuplicateJournals
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\ErrorReport", mappedBy="duplicateJournals", cascade={"remove"})
     */
    private $errorReport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Journal", inversedBy="rightJournals")
     */
    private $rightJournal;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Journal", inversedBy="wrongJournals")
     */
    private $wrongJournal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set errorReport
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $errorReport
     * @return ErrorReport
     */
    public function setErrorReport(\Bibliometry\MainBundle\Entity\ErrorReport $errorReport = null)
    {
        $this->errorReport = $errorReport;

        return $this;
    }

    /**
     * Get errorReport
     *
     * @return \Bibliometry\MainBundle\Entity\ErrorReport 
     */
    public function getErrorReport()
    {
        return $this->errorReport;
    }

    /**
     * Set rightJournal
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $rightJournal
     * @return DuplicateJournals
     */
    public function setRightJournal(\Bibliometry\MainBundle\Entity\Journal $rightJournal = null)
    {
        $this->rightJournal = $rightJournal;
        $rightJournal->addRightJournal($this);
        return $this;
    }

    /**
     * Get rightJournal
     *
     * @return \Bibliometry\MainBundle\Entity\Journal 
     */
    public function getRightJournal()
    {
        return $this->rightJournal;
    }

    /**
     * Set wrongJournal
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $wrongJournal
     * @return DuplicateJournals
     */
    public function setWrongJournal(\Bibliometry\MainBundle\Entity\Journal $wrongJournal = null)
    {
        $this->wrongJournal = $wrongJournal;
        $wrongJournal->addWrongJournal($this);
        return $this;
    }

    /**
     * Get wrongJournal
     *
     * @return \Bibliometry\MainBundle\Entity\Journal 
     */
    public function getWrongJournal()
    {
        return $this->wrongJournal;
    }
    
    public function getSelectedWrongJournal($selectedRightJournal)
    {
        if ($this->getRightJournal() == $selectedRightJournal)
        {
            return $this->getWrongJournal();
        }
        else
        {
            return $this->getRightJournal();
        }
    }
}
