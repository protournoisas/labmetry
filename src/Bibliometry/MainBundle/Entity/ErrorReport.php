<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ErrorReport
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class ErrorReport
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\User", inversedBy="reportedErrors")
     */
    private $reporter;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="reportDate", type="datetime")
     */
    private $reportDate;

    /**
     * @var string
     *
     * @ORM\Column(name="comments", type="text", nullable = true)
     */
    private $comments;

    /**
     * @var boolean
     *
     * @ORM\Column(name="handled", type="boolean")
     */
    private $handled;

    /**
     * @var boolean
     *
     * @ORM\Column(name="validated", type="boolean", nullable = true)
     */
    private $validated;
    
    /**
     * @var string
     *
     * @ORM\Column(name="finalComments", type="text", nullable = true)
     */
    private $finalComments;

    
    /**
     * @var boolean
     *
     * @ORM\Column(name="sendMail", type="boolean")
     */
    private $sendMail;
    
    /**
     * @var string
     *
     * @ORM\Column(name="errorType", type="string")
     */
    private $errorType;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\DuplicatePublications", inversedBy="errorReport", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $duplicatePublications;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\DuplicateJournals", inversedBy="errorReport", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $duplicateJournals;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\WrongPublicationMatch", inversedBy="errorReport", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $wrongPublicationMatch;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\DuplicateResearchers", inversedBy="errorReport", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $duplicateResearchers;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\SplitResearchers", inversedBy="errorReport", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $splitResearchers;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\NotLabPublication", inversedBy="errorReport", cascade={"persist","remove"})
     * @Assert\Valid
     */
    private $notLabPublication;

    public function __construct($reporter)
    {
        $this->reporter = $reporter;
        $this->reportDate = new \DateTime();
        $this->handled = false;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set reportDate
     *
     * @param \DateTime $reportDate
     * @return ErrorReport
     */
    public function setReportDate($reportDate)
    {
        $this->reportDate = $reportDate;

        return $this;
    }

    /**
     * Get reportDate
     *
     * @return \DateTime 
     */
    public function getReportDate()
    {
        return $this->reportDate;
    }

    /**
     * Set comments
     *
     * @param string $comments
     * @return ErrorReport
     */
    public function setComments($comments)
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * Get comments
     *
     * @return string 
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Set handled
     *
     * @param boolean $handled
     * @return ErrorReport
     */
    public function setHandled($handled)
    {
        $this->handled = $handled;

        return $this;
    }

    /**
     * Get handled
     *
     * @return boolean 
     */
    public function getHandled()
    {
        return $this->handled;
    }

    /**
     * Set validated
     *
     * @param boolean $validated
     * @return ErrorReport
     */
    public function setValidated($validated)
    {
        $this->validated = $validated;

        return $this;
    }

    /**
     * Get validated
     *
     * @return boolean 
     */
    public function getValidated()
    {
        return $this->validated;
    }

    /**
     * Set reporter
     *
     * @param \Bibliometry\MainBundle\Entity\User $reporter
     * @return ErrorReport
     */
    public function setReporter(\Bibliometry\MainBundle\Entity\User $reporter = null)
    {
        $this->reporter = $reporter;
        $reporter->addReportedError($this);
        return $this;
    }

    /**
     * Get reporter
     *
     * @return \Bibliometry\MainBundle\Entity\User 
     */
    public function getReporter()
    {
        return $this->reporter;
    }

    /**
     * Set sendMail
     *
     * @param boolean $sendMail
     * @return ErrorReport
     */
    public function setSendMail($sendMail)
    {
        $this->sendMail = $sendMail;

        return $this;
    }

    /**
     * Get sendMail
     *
     * @return boolean 
     */
    public function getSendMail()
    {
        return $this->sendMail;
    }

    /**
     * Set duplicatePublications
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicatePublications $duplicatePublications
     * @return ErrorReport
     */
    public function setDuplicatePublications(\Bibliometry\MainBundle\Entity\DuplicatePublications $duplicatePublications = null)
    {
        $this->duplicatePublications = $duplicatePublications;
        $duplicatePublications->setErrorReport($this);
        return $this;
    }

    /**
     * Get duplicatePublications
     *
     * @return \Bibliometry\MainBundle\Entity\DuplicatePublications 
     */
    public function getDuplicatePublications()
    {
        return $this->duplicatePublications;
    }

    /**
     * Set duplicateJournals
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateJournals $duplicateJournals
     * @return ErrorReport
     */
    public function setDuplicateJournals(\Bibliometry\MainBundle\Entity\DuplicateJournals $duplicateJournals = null)
    {
        $this->duplicateJournals = $duplicateJournals;
        $duplicateJournals->setErrorReport($this);
        return $this;
    }

    /**
     * Get duplicateJournals
     *
     * @return \Bibliometry\MainBundle\Entity\DuplicateJournals 
     */
    public function getDuplicateJournals()
    {
        return $this->duplicateJournals;
    }

    /**
     * Set wrongPublicationMatch
     *
     * @param \Bibliometry\MainBundle\Entity\WrongPublicationMatch $wrongPublicationMatch
     * @return ErrorReport
     */
    public function setWrongPublicationMatch(\Bibliometry\MainBundle\Entity\WrongPublicationMatch $wrongPublicationMatch = null)
    {
        $this->wrongPublicationMatch = $wrongPublicationMatch;
        $wrongPublicationMatch->setErrorReport($this);
        return $this;
    }

    /**
     * Get wrongPublicationMatch
     *
     * @return \Bibliometry\MainBundle\Entity\WrongPublicationMatch 
     */
    public function getWrongPublicationMatch()
    {
        return $this->wrongPublicationMatch;
    }

    /**
     * Set duplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateResearchers $duplicateResearchers
     * @return ErrorReport
     */
    public function setDuplicateResearchers(\Bibliometry\MainBundle\Entity\DuplicateResearchers $duplicateResearchers = null)
    {
        $this->duplicateResearchers = $duplicateResearchers;
        $duplicateResearchers->setErrorReport($this);
        return $this;
    }

    /**
     * Get duplicateResearchers
     *
     * @return \Bibliometry\MainBundle\Entity\DuplicateResearchers 
     */
    public function getDuplicateResearchers()
    {
        return $this->duplicateResearchers;
    }
    
    /**
     * Set splitResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\SplitResearchers $splitResearchers
     * @return ErrorReport
     */
    public function setSplitResearchers(\Bibliometry\MainBundle\Entity\SplitResearchers $splitResearchers = null)
    {
        $this->splitResearchers = $splitResearchers;
        $splitResearchers->setErrorReport($this);
        return $this;
    }

    /**
     * Get splitResearchers
     *
     * @return \Bibliometry\MainBundle\Entity\SplitResearchers 
     */
    public function getSplitResearchers()
    {
        return $this->splitResearchers;
    }

    /**
     * Set errorType
     *
     * @param string $errorType
     * @return ErrorReport
     */
    public function setErrorType($errorType)
    {
        $this->errorType = $errorType;

        return $this;
    }

    /**
     * Get errorType
     *
     * @return string 
     */
    public function getErrorType()
    {
        return $this->errorType;
    }
    
    // Function that computes the teams that are concerned by this error
    public function getConcernedTeams()
    {
        $concernedTeams = array();
        
        // Duplicate publications
        if ($this->duplicatePublications)
        {
            foreach (array($this->duplicatePublications->getRightPublication(), $this->duplicatePublications->getWrongPublication()) as $publication)
            {
                foreach ($publication->getAuthors() as $author)
                {
                    foreach ($author->getTeams() as $team)
                    {
                        if (! array_key_exists($team->getId(), $concernedTeams))
                        {
                            $concernedTeams[$team->getId()] = $team;
                        }
                    }
                }
            }
        }
        else if ($this->duplicateJournals)
        {
            foreach (array($this->duplicateJournals->getRightJournal(), $this->duplicateJournals->getWrongJournal()) as $journal)
            {
                foreach ($journal->getPublications() as $publication)
                {
                    foreach ($publication->getAuthors() as $author)
                    {
                        foreach ($author->getTeams() as $team)
                        {
                            if (! array_key_exists($team->getId(), $concernedTeams))
                            {
                                $concernedTeams[$team->getId()] = $team;
                            }
                        }
                    }
                }
            }
        }
        else if ($this->wrongPublicationMatch)
        {
            foreach ($this->wrongPublicationMatch->getPublication()->getAuthors() as $author)
            {
                foreach ($author->getTeams() as $team)
                {
                    if (! array_key_exists($team->getId(), $concernedTeams))
                    {
                        $concernedTeams[$team->getId()] = $team;
                    }
                }
            }
        }
        else if ($this->duplicateResearchers)
        {
            foreach (array_merge([$this->duplicateResearchers->getRightResearcher()], $this->duplicateResearchers->getDuplicateResearchers()->toArray()) as $researcher)
            {
                foreach ($researcher->getTeams() as $team)
                {
                    if (! array_key_exists($team->getId(), $concernedTeams))
                    {
                        $concernedTeams[$team->getId()] = $team;
                    }
                }
            }
        }
        else if ($this->splitResearchers)
        {
            foreach (array_merge([$this->splitResearchers->getMultipleResearcher()], $this->splitResearchers->getSplitResearchers()->toArray()) as $researcher)
            {
                foreach ($researcher->getTeams() as $team)
                {
                    if (! array_key_exists($team->getId(), $concernedTeams))
                    {
                        $concernedTeams[$team->getId()] = $team;
                    }
                }
            }
        }
        else if ($this->notLabPublication)
        {
            foreach ($this->notLabPublication->getPublication()->getAuthors() as $author)
            {
                foreach ($author->getTeams() as $team)
                {
                    if (! array_key_exists($team->getId(), $concernedTeams))
                    {
                        $concernedTeams[$team->getId()] = $team;
                    }
                }
            }
        }
        
        return $concernedTeams;
    }
    
    public function __toString()
    {   
        // Duplicate publications
        if ($this->duplicatePublications)
        {
            return $this->duplicatePublications->getRightPublication()->getTitle()." / ".$this->duplicatePublications->getWrongPublication()->getTitle();
        }
        else if ($this->duplicateJournals)
        {
            return $this->duplicateJournals->getRightJournal()->getTitle()." / ".$this->duplicateJournals->getWrongJournal()->getTitle();
        }
        else if ($this->wrongPublicationMatch)
        {
            return $this->wrongPublicationMatch->getPublication()->getTitle()." in ".($this->wrongPublicationMatch->getRightMatch()? $this->wrongPublicationMatch->getRightMatch()->getTitle() : "Not Matched");
        }
        else if ($this->duplicateResearchers)
        {
            $toReturn = $this->duplicateResearchers->getRightResearcher()." and ";
            foreach ($this->duplicateResearchers->getDuplicateResearchers() as $researcher)
            {
                $toReturn .= $researcher.", ";
            }
            return rtrim($toReturn, ", ");
        }
        else if ($this->splitResearchers)
        {
            $toReturn = $this->splitResearchers->getMultipleResearcher()." splits in ";
            foreach ($this->splitResearchers->getSplitResearchers() as $researcher)
            {
                $toReturn .= $researcher.", ";
            }
            return rtrim($toReturn, ", ");
        }
        else if ($this->notLabPublication)
        {
            return $this->notLabPublication->getPublication()->getTitle();
        }
    }

    /**
     * Set finalComments
     *
     * @param string $finalComments
     * @return ErrorReport
     */
    public function setFinalComments($finalComments)
    {
        $this->finalComments = $finalComments;

        return $this;
    }

    /**
     * Get finalComments
     *
     * @return string 
     */
    public function getFinalComments()
    {
        return $this->finalComments;
    }

    /**
     * Set notLabPublication
     *
     * @param \Bibliometry\MainBundle\Entity\NotLabPublication $notLabPublication
     * @return ErrorReport
     */
    public function setNotLabPublication(\Bibliometry\MainBundle\Entity\NotLabPublication $notLabPublication = null)
    {
        $this->notLabPublication = $notLabPublication;
        $notLabPublication->setErrorReport($this);
        return $this;
    }

    /**
     * Get notLabPublication
     *
     * @return \Bibliometry\MainBundle\Entity\NotLabPublication 
     */
    public function getNotLabPublication()
    {
        return $this->notLabPublication;
    }
}
