<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Notification
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 */
class Notification
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="shortMessage", type="string", length=255)
     */
    private $shortMessage;

    /**
     * @var string
     *
     * @ORM\Column(name="fullMessage", type="text")
     */
    private $fullMessage;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="notificationDate", type="datetime")
     */
    private $notificationDate;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isRead", type="boolean")
     */
    private $isRead;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\User", inversedBy="notifications")
     */
    private $user;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication")
     */
    private $publication;

    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Team")
     */
    private $team;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set shortMessage
     *
     * @param string $shortMessage
     * @return Notification
     */
    public function setShortMessage($shortMessage)
    {
        $this->shortMessage = $shortMessage;

        return $this;
    }

    /**
     * Get shortMessage
     *
     * @return string 
     */
    public function getShortMessage()
    {
        return $this->shortMessage;
    }

    /**
     * Set fullMessage
     *
     * @param string $fullMessage
     * @return Notification
     */
    public function setFullMessage($fullMessage)
    {
        $this->fullMessage = $fullMessage;

        return $this;
    }

    /**
     * Get fullMessage
     *
     * @return string 
     */
    public function getFullMessage()
    {
        return $this->fullMessage;
    }

    /**
     * Set notificationDate
     *
     * @param \DateTime $notificationDate
     * @return Notification
     */
    public function setNotificationDate($notificationDate)
    {
        $this->notificationDate = $notificationDate;

        return $this;
    }

    /**
     * Get notificationDate
     *
     * @return \DateTime 
     */
    public function getNotificationDate()
    {
        return $this->notificationDate;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function setNotificationDateValue()
    {
        $this->notificationDate = new \DateTime();
    }

    /**
     * Set isRead
     *
     * @param boolean $isRead
     * @return Notification
     */
    public function setIsRead($isRead)
    {
        $this->isRead = $isRead;

        return $this;
    }

    /**
     * Get isRead
     *
     * @return boolean 
     */
    public function getIsRead()
    {
        return $this->isRead;
    }

    /**
     * Set user
     *
     * @param \Bibliometry\MainBundle\Entity\User $user
     * @return Notification
     */
    public function setUser(\Bibliometry\MainBundle\Entity\User $user = null)
    {
        $this->user = $user;
        $user->addNotification($this);
        return $this;
    }

    /**
     * Get user
     *
     * @return \Bibliometry\MainBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->publications = new \Doctrine\Common\Collections\ArrayCollection();
        $this->isRead = false;
    }
    
    /**
     * Set type
     *
     * @param string $type
     * @return Notification
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set team
     *
     * @param \Bibliometry\MainBundle\Entity\Team $team
     * @return Notification
     */
    public function setTeam(\Bibliometry\MainBundle\Entity\Team $team = null)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return \Bibliometry\MainBundle\Entity\Team 
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Set publication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publication
     * @return Notification
     */
    public function setPublication(\Bibliometry\MainBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }
}
