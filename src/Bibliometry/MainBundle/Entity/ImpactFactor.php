<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ImpactFactor
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\ImpactFactorRepository")
 */
class ImpactFactor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(name="quartile", type="integer")
     */
    private $quartile;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Journal", cascade={"persist"}, inversedBy="IFs")
    */
    private $journal;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return ImpactFactor
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set value
     *
     * @param float $value
     * @return ImpactFactor
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set quartile
     *
     * @param integer $quartile
     * @return ImpactFactor
     */
    public function setQuartile($quartile)
    {
        $this->quartile = $quartile;

        return $this;
    }

    /**
     * Get quartile
     *
     * @return integer 
     */
    public function getQuartile()
    {
        return $this->quartile;
    }

    /**
     * Set journal
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $journal
     * @return ImpactFactor
     */
    public function setJournal(\Bibliometry\MainBundle\Entity\Journal $journal = null)
    {
        $this->journal = $journal;

        return $this;
    }

    /**
     * Get journal
     *
     * @return \Bibliometry\MainBundle\Entity\Journal 
     */
    public function getJournal()
    {
        return $this->journal;
    }
}
