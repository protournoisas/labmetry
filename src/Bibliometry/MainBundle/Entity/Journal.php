<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Journal
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\JournalRepository")
 */
class Journal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="ISOAbbreviation", type="string", length=255, nullable=true)
     */
    private $iSOAbbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="JCRAbbreviation", type="string", length=255, nullable=true)
     */
    private $jCRAbbreviation;

    /**
     * @var string
     *
     * @ORM\Column(name="ISSN", type="string", length=255, nullable=true)
     */
    private $iSSN;
    
    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\PubliJournal", mappedBy="journal")
    */
    private $publiJournals;
    
    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\ImpactFactor", cascade={"persist"}, mappedBy="journal")
    */
    private $IFs;

    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\DuplicateJournals", mappedBy="rightJournal")
     */
    private $rightJournals;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\DuplicateJournals", mappedBy="wrongJournal")
     */
    private $wrongJournals;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Journal", cascade={"persist"}, inversedBy="mergedFrom")
     */
    private $mergedTo;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\Journal", mappedBy="mergedTo")
     */
    private $mergedFrom;
    
    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Journal
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set iSOAbbreviation
     *
     * @param string $iSOAbbreviation
     * @return Journal
     */
    public function setISOAbbreviation($iSOAbbreviation)
    {
        $this->iSOAbbreviation = $iSOAbbreviation;

        return $this;
    }

    /**
     * Get iSOAbbreviation
     *
     * @return string 
     */
    public function getISOAbbreviation()
    {
        return $this->iSOAbbreviation;
    }

    /**
     * Set jCRAbbreviation
     *
     * @param string $jCRAbbreviation
     * @return Journal
     */
    public function setJCRAbbreviation($jCRAbbreviation)
    {
        $this->jCRAbbreviation = $jCRAbbreviation;

        return $this;
    }

    /**
     * Get jCRAbbreviation
     *
     * @return string 
     */
    public function getJCRAbbreviation()
    {
        return $this->jCRAbbreviation;
    }

    /**
     * Set iSSN
     *
     * @param string $iSSN
     * @return Journal
     */
    public function setISSN($iSSN)
    {
        $this->iSSN = $iSSN;

        return $this;
    }

    /**
     * Get iSSN
     *
     * @return string 
     */
    public function getISSN()
    {
        return $this->iSSN;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->publiJournals = new \Doctrine\Common\Collections\ArrayCollection();
        $this->IFs = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add publiJournals
     *
     * @param \Bibliometry\MainBundle\Entity\PubliJournal $publiJournals
     * @return Journal
     */
    public function addPubliJournal(\Bibliometry\MainBundle\Entity\PubliJournal $publiJournals)
    {
        $this->publiJournals[] = $publiJournals;

        return $this;
    }

    /**
     * Remove publiJournals
     *
     * @param \Bibliometry\MainBundle\Entity\PubliJournal $publiJournals
     */
    public function removePubliJournal(\Bibliometry\MainBundle\Entity\PubliJournal $publiJournals)
    {
        $this->publiJournals->removeElement($publiJournals);
    }
    
    /**
     * Get publiJournals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPubliJournals()
    {
        return $this->publiJournals;
    }
    
    /**
     * Get Number of publication in this journal
     */
    public function getNbPubli()
    {
        $nb = 0;
        foreach($this->publiJournals as $publiJournal)
            if ($publiJournal->getPublication())
                $nb += 1;
        return $nb;
    }

    /**
     * Add IFs
     *
     * @param \Bibliometry\MainBundle\Entity\ImpactFactor $iFs
     * @return Journal
     */
    public function addIF(\Bibliometry\MainBundle\Entity\ImpactFactor $iFs)
    {
        $this->IFs[] = $iFs;
        $iFs->setJournal($this);
        return $this;
    }

    /**
     * Remove IFs
     *
     * @param \Bibliometry\MainBundle\Entity\ImpactFactor $iFs
     */
    public function removeIF(\Bibliometry\MainBundle\Entity\ImpactFactor $iFs)
    {
        $this->IFs->removeElement($iFs);
    }

    /**
     * Get IFs
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getIFs()
    {
        $toReturn = $this->IFs->toArray();
        usort($toReturn, array('\Bibliometry\MainBundle\Entity\Journal', 'compareIFsByYear'));
        return $toReturn;
    }
    
    private static function compareIFsByYear($IF1, $IF2)
    {
        if ($IF1->getYear() > $IF2->getYear())
        {
            return -1;
        }
        else if ($IF1->getYear() < $IF2->getYear())
        {
            return 1;
        }
        else 
        {
            return 0;
        }
    }
    
    /**
     * Get lastIF
     *
     * @return \Bibliometry\MainBundle\Entity\ImpactFactor $iFs
     */
    public function getLastIF()
    {
        $maxYear = 0;
        $lastIF = NULL;
        foreach ($this->getIFs() as $if)
        {
            if ($if->getYear() > $maxYear)
            {
                $maxYear = $if->getYear();
                $lastIF = $if;
            }
        }
        
        return $lastIF;
    }
    
    /**
     * Get IF by year
     *
     * @return \Bibliometry\MainBundle\Entity\ImpactFactor $iFs
     */
    public function getIFYear($year)
    {
        foreach($this->getIFs() as $IF)
        {
            if ($IF->getYear() == $year)
            {
                return $IF;
            }
        }
        return $this->getLastIF();
    }
    
    public function getIFYearNullable($year)
    {
        foreach($this->getIFs() as $IF)
        {
            if ($IF->getYear() == $year)
            {
                return $IF;
            }
        }
        return NULL;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Journal
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * Add rightJournals
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateJournals $rightJournals
     * @return Journal
     */
    public function addRightJournal(\Bibliometry\MainBundle\Entity\DuplicateJournals $rightJournals)
    {
        $this->rightJournals[] = $rightJournals;

        return $this;
    }

    /**
     * Remove rightJournals
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateJournals $rightJournals
     */
    public function removeRightJournal(\Bibliometry\MainBundle\Entity\DuplicateJournals $rightJournals)
    {
        $this->rightJournals->removeElement($rightJournals);
    }

    /**
     * Get rightJournals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRightJournals()
    {
        return $this->rightJournals;
    }

    /**
     * Add wrongJournals
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateJournals $wrongJournals
     * @return Journal
     */
    public function addWrongJournal(\Bibliometry\MainBundle\Entity\DuplicateJournals $wrongJournals)
    {
        $this->wrongJournals[] = $wrongJournals;

        return $this;
    }

    /**
     * Remove wrongJournals
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateJournals $wrongJournals
     */
    public function removeWrongJournal(\Bibliometry\MainBundle\Entity\DuplicateJournals $wrongJournals)
    {
        $this->wrongJournals->removeElement($wrongJournals);
    }

    /**
     * Get wrongJournals
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWrongJournals()
    {
        return $this->wrongJournals;
    }
    
    public function getPublications()
    {
        $toReturn = array();
        foreach ($this->getPubliJournals() as $publiJournal)
        {
            $toReturn[] = $publiJournal->getPublication();
        }
        return $toReturn;
    }

    /**
     * Set mergedTo
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $mergedTo
     * @return Journal
     */
    public function setMergedTo(\Bibliometry\MainBundle\Entity\Journal $mergedTo = null)
    {
        $this->mergedTo = $mergedTo;

        return $this;
    }

    /**
     * Get mergedTo
     *
     * @return \Bibliometry\MainBundle\Entity\Journal 
     */
    public function getMergedTo()
    {
        return $this->mergedTo;
    }

    /**
     * Add mergedFrom
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $mergedFrom
     * @return Journal
     */
    public function addMergedFrom(\Bibliometry\MainBundle\Entity\Journal $mergedFrom)
    {
        $this->mergedFrom[] = $mergedFrom;

        return $this;
    }

    /**
     * Remove mergedFrom
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $mergedFrom
     */
    public function removeMergedFrom(\Bibliometry\MainBundle\Entity\Journal $mergedFrom)
    {
        $this->mergedFrom->removeElement($mergedFrom);
    }

    /**
     * Get mergedFrom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMergedFrom()
    {
        return $this->mergedFrom;
    }
    
    public function mergeWith($rightJournal)
    {
        foreach ($this->getPubliJournals() as $publiJournal)
        {
            $publiJournal->setJournal($rightJournal);
        }
        $this->IFs = new \Doctrine\Common\Collections\ArrayCollection();
        $this->setMergedTo($rightJournal);
    }
}
