<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * PubliJournalRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class PubliJournalRepository extends EntityRepository
{
}
