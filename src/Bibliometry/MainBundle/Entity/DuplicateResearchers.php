<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Bibliometry\MainBundle\Validator\Constraints as ErrorsAssert;

/**
 * DuplicateResearchers
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ErrorsAssert\DifferentRightWrongResearcher
 */
class DuplicateResearchers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\ErrorReport", mappedBy="duplicateResearchers", cascade={"remove"})
     */
    private $errorReport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Researcher", inversedBy="rightDuplicateResearchers")
     */
    private $rightResearcher;
    
    /**
     * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Researcher", inversedBy="duplicateResearchers")
     */
    private $duplicateResearchers;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->duplicateResearchers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set errorReport
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $errorReport
     * @return DuplicateResearchers
     */
    public function setErrorReport(\Bibliometry\MainBundle\Entity\ErrorReport $errorReport = null)
    {
        $this->errorReport = $errorReport;

        return $this;
    }

    /**
     * Get errorReport
     *
     * @return \Bibliometry\MainBundle\Entity\ErrorReport 
     */
    public function getErrorReport()
    {
        return $this->errorReport;
    }

    /**
     * Set rightResearcher
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $rightResearcher
     * @return DuplicateResearchers
     */
    public function setRightResearcher(\Bibliometry\MainBundle\Entity\Researcher $rightResearcher = null)
    {
        $this->rightResearcher = $rightResearcher;
        $rightResearcher->addRightDuplicateResearcher($this);
        return $this;
    }

    /**
     * Get rightResearcher
     *
     * @return \Bibliometry\MainBundle\Entity\Researcher 
     */
    public function getRightResearcher()
    {
        return $this->rightResearcher;
    }

    /**
     * Add duplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $duplicateResearchers
     * @return DuplicateResearchers
     */
    public function addDuplicateResearcher(\Bibliometry\MainBundle\Entity\Researcher $duplicateResearchers)
    {
        $this->duplicateResearchers[] = $duplicateResearchers;
        $duplicateResearchers->addDuplicateResearcher($this);
        return $this;
    }

    /**
     * Remove duplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $duplicateResearchers
     */
    public function removeDuplicateResearcher(\Bibliometry\MainBundle\Entity\Researcher $duplicateResearchers)
    {
        $this->duplicateResearchers->removeElement($duplicateResearchers);
    }

    /**
     * Get duplicateResearchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDuplicateResearchers()
    {
        return $this->duplicateResearchers;
    }
    
    public function getSelectedDuplicateResearchers($selectedResearcher)
    {
        $wrongResearchers = array();
        
        if ($this->getRightResearcher() != $selectedResearcher)
        {
            $wrongResearchers[] = $this->getRightResearcher();
        }
        
        foreach ($this->getDuplicateResearchers() as $duplicateResearcher)
        {
            if ($duplicateResearcher != $selectedResearcher)
            {
                $wrongResearchers[] = $duplicateResearcher;
            }
        }
        
        return $wrongResearchers;
    }
}
