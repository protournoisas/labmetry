<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PublicationAuthor
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PublicationAuthor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="authorRank", type="integer")
     */
    private $authorRank;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="publicationAuthors")
     */
    private $publication;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Researcher", inversedBy="publicationAuthors")
     */
    private $researcher;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set authorRank
     *
     * @param integer $authorRank
     * @return PublicationAuthor
     */
    public function setAuthorRank($authorRank)
    {
        $this->authorRank = $authorRank;

        return $this;
    }

    /**
     * Get authorRank
     *
     * @return integer 
     */
    public function getAuthorRank()
    {
        return $this->authorRank;
    }

    /**
     * Set publication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publication
     * @return PublicationAuthor
     */
    public function setPublication(\Bibliometry\MainBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set researcher
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $researcher
     * @return PublicationAuthor
     */
    public function setResearcher(\Bibliometry\MainBundle\Entity\Researcher $researcher = null)
    {
        $this->researcher = $researcher;

        return $this;
    }

    /**
     * Get researcher
     *
     * @return \Bibliometry\MainBundle\Entity\Researcher 
     */
    public function getResearcher()
    {
        return $this->researcher;
    }
}
