<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FR3D\LdapBundle\Model\LdapUserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

use Bibliometry\MainBundle\Entity\UserInterface;
/**
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\UserRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="UserTable")
 * @UniqueEntity(
 *     fields={"usernameCanonical"},
 *     errorPath="username",
 *     message="bibliometry.errors.fos_user.username.already_used"
 * )
 * @UniqueEntity(
 *     fields={"emailCanonical"},
 *     errorPath="email",
 *     message="bibliometry.errors.fos_user.email.already_used"
 * )
 */
class User implements LdapUserInterface, UserInterface
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(name="username", type="string", length=255)
     * @Assert\NotBlank(message="bibliometry.errors.fos_user.username.blank", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = 3,
     *      max = 255,
     *      minMessage="bibliometry.errors.fos_user.username.short",
     *      maxMessage="bibliometry.errors.fos_user.username.long",
     *      groups={"Registration", "Profile"}
     * )
     */
    protected $username;

    /**
     * @var string
     * @ORM\Column(name="username_canonical", type="string", length=255, unique=true)
     */
    protected $usernameCanonical;

    /**
     * @var string
     * @ORM\Column(name="email", type="string", length=255)
     * @Assert\NotBlank(message="bibliometry.errors.fos_user.email.blank", groups={"Registration", "Profile"})
     * @Assert\Length(
     *      min = 2,
     *      max = 254,
     *      minMessage="bibliometry.errors.fos_user.email.short",
     *      maxMessage="bibliometry.errors.fos_user.email.long",
     *      groups={"Registration", "Profile"}
     * )
     * @Assert\Email(message="bibliometry.errors.fos_user.email.invalid", groups={"Registration", "Profile"})
     */
    protected $email;

    /**
     * @var string
     * @ORM\Column(name="email_canonical", type="string", length=255, unique=true)
     */
    protected $emailCanonical;

    /**
     * @var boolean
     * @ORM\Column(name="enabled", type="boolean")
     */
    protected $enabled;

    /**
     * The salt to use for hashing
     *
     * @var string
     * @ORM\Column(name="salt", type="string")
     */
    protected $salt;

    /**
     * Encrypted password. Must be persisted.
     *
     * @var string
     * @ORM\Column(name="password", type="string")
     */
    protected $password;

    /**
     * Plain password. Used for model validation. Must not be persisted.
     *
     * @var string
     * @Assert\NotBlank(message="bibliometry.errors.fos_user.password.blank", groups={"ResetPassword", "ChangePassword"})
     * @Assert\Length(
     *      min = 2,
     *      minMessage="bibliometry.errors.fos_user.password.short"),
     *      groups={"Registration", "Profile", "ResetPassword", "ChangePassword"}
     */
    protected $plainPassword;

    /**
     * @var \DateTime
     * @ORM\Column(name="last_login", type="datetime", nullable=true)
     */
    protected $lastLogin;

    /**
     * Random string sent to the user email address in order to verify it
     *
     * @var string
     * @ORM\Column(name="confirmation_token", type="string", nullable=true)
     */
    protected $confirmationToken;

    /**
     * @var \DateTime
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true)
     */
    protected $passwordRequestedAt;

    /**
     * @var boolean
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked;

    /**
     * @var boolean
     * @ORM\Column(name="expired", type="boolean")
     */
    protected $expired;

    /**
     * @var \DateTime
     * @ORM\Column(name="expires_at", type="datetime", nullable=true)
     */
    protected $expiresAt;

    /**
     * @var array
     * @ORM\Column(name="roles", type="array")
     */
    protected $roles;

    /**
     * @var boolean
     * @ORM\Column(name="credentials_expired", type="boolean")
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime
     * @ORM\Column(name="credentials_expire_at", type="datetime", nullable=true)
     */
    protected $credentialsExpireAt;

    /**
     * @ORM\Column(name="dn", type="string", length=255, nullable=true)
     */
    protected $dn;

    /**
     * @ORM\Column(name="cn", type="string", length=255, nullable=true)
     */
    protected $cn;

    protected $attributes;

    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Photo", cascade={"persist"})
    */
    private $photo;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true)
     */
    private $surname;

    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Researcher", cascade={"persist"}, inversedBy="user")
    */
    private $researcher;

    /**
     * @var boolean $registrationCompleted
     *
     * @ORM\Column(name="registrationCompleted", type="boolean")
     */
    private $registrationCompleted;

    /**
     * @var datetime $registrationDate
     *
     * @ORM\Column(name="registrationDate", type="datetime")
     */
    private $registrationDate;

    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\ErrorReport", mappedBy="reporter")
     */
    private $reportedErrors;

    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\Notification", mappedBy="user")
     */
    private $notifications;

    /**
     * @var string
     *
     * @ORM\Column(name="apiKey", type="string", length=255)
     */
    private $apiKey;

    public function __construct()
    {
        $this->salt = base_convert(sha1(uniqid(mt_rand(), true)), 16, 36);
        $this->enabled = false;
        $this->locked = false;
        $this->expired = false;
        $this->roles = array();
        $this->credentialsExpired = false;
        $this->registrationDate = new \DateTime();
        $this->registrationCompleted = false;
    }

    public function addRole($role)
    {
        $role = strtoupper($role);
        if ($role === 'ROLE_USER')
            return $this;
        if (!in_array($role, $this->roles, true))
            $this->roles[] = $role;
        return $this;
    }

    /**
     * Removes sensitive data from the user.
     */
    public function eraseCredentials()
    {
        $this->plainPassword = null;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function getUsernameCanonical()
    {
        return $this->usernameCanonical;
    }

    public function getSalt()
    {
        return $this->salt;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getEmailCanonical()
    {
        return $this->emailCanonical;
    }

    /**
     * Gets the encrypted password.
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    /**
     * Gets the last login time.
     *
     * @return \DateTime
     */
    public function getLastLogin()
    {
        return $this->lastLogin;
    }

    public function getConfirmationToken()
    {
        return $this->confirmationToken;
    }

    /**
     * Returns the user roles
     *
     * @return array The roles
     */
    public function getRoles()
    {
        $roles = $this->roles;
        // we need to make sure to have at least one role
        $roles[] = 'ROLE_USER';
        return array_unique($roles);
    }

    /**
     * Never use this to check if this user has access to anything!
     *
     * Use the SecurityContext, or an implementation of AccessDecisionManager
     * instead, e.g.
     *
     *         $securityContext->isGranted('ROLE_USER');
     *
     * @param string $role
     *
     * @return boolean
     */
    public function hasRole($role)
    {
        return in_array(strtoupper($role), $this->getRoles(), true);
    }

    public function isAccountNonExpired()
    {
        if (true === $this->expired)
            return false;
        if (null !== $this->expiresAt && $this->expiresAt->getTimestamp() < time())
            return false;
        return true;
    }

    public function isAccountNonLocked()
    {
        return !$this->locked;
    }

    public function isCredentialsNonExpired()
    {
        if (true === $this->credentialsExpired)
            return false;
        if (null !== $this->credentialsExpireAt && $this->credentialsExpireAt->getTimestamp() < time())
            return false;
        return true;
    }

    public function isCredentialsExpired()
    {
        return !$this->isCredentialsNonExpired();
    }

    public function isEnabled()
    {
        return $this->enabled;
    }

    public function isExpired()
    {
        return !$this->isAccountNonExpired();
    }

    public function isLocked()
    {
        return !$this->isAccountNonLocked();
    }

    public function isSuperAdmin()
    {
        return $this->hasRole('ROLE_SUPER_ADMIN');
    }

    public function removeRole($role)
    {
        if (false !== $key = array_search(strtoupper($role), $this->roles, true)) {
            unset($this->roles[$key]);
            $this->roles = array_values($this->roles);
        }
        return $this;
    }

    public function setUsername($username)
    {
        $this->username = $username;
        return $this;
    }

    public function setUsernameCanonical($usernameCanonical)
    {
        $this->usernameCanonical = $usernameCanonical;
        return $this;
    }

    /**
     * @param \DateTime $date
     *
     * @return User
     */
    public function setCredentialsExpireAt(\DateTime $date = null)
    {
        $this->credentialsExpireAt = $date;
        return $this;
    }

    /**
     * @param boolean $boolean
     *
     * @return User
     */
    public function setCredentialsExpired($boolean)
    {
        $this->credentialsExpired = $boolean;
        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    public function setEmailCanonical($emailCanonical)
    {
        $this->emailCanonical = $emailCanonical;
        return $this;
    }

    public function setEnabled($boolean)
    {
        $this->enabled = (Boolean) $boolean;
        return $this;
    }

    /**
     * Sets this user to expired.
     *
     * @param Boolean $boolean
     *
     * @return User
     */
    public function setExpired($boolean)
    {
        $this->expired = (Boolean) $boolean;
        return $this;
    }

    /**
     * @param \DateTime $date
     *
     * @return User
     */
    public function setExpiresAt(\DateTime $date = null)
    {
        $this->expiresAt = $date;
        return $this;
    }

    public function setPassword($password)
    {
        $this->password = $password;
        return $this;
    }

    public function setSuperAdmin($boolean)
    {
        if (true === $boolean)
            $this->addRole('ROLE_SUPER_ADMIN');
        else
            $this->removeRole('ROLE_SUPER_ADMIN');
        return $this;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
        return $this;
    }

    public function setLastLogin(\DateTime $time = null)
    {
        $this->lastLogin = $time;
        return $this;
    }

    public function setLocked($boolean)
    {
        $this->locked = $boolean;
        return $this;
    }

    public function setConfirmationToken($confirmationToken)
    {
        $this->confirmationToken = $confirmationToken;
        return $this;
    }

    public function setPasswordRequestedAt(\DateTime $date = null)
    {
        $this->passwordRequestedAt = $date;
        return $this;
    }

    /**
     * Gets the timestamp that the user requested a password reset.
     *
     * @return null|\DateTime
     */
    public function getPasswordRequestedAt()
    {
        return $this->passwordRequestedAt;
    }

    public function isPasswordRequestNonExpired()
    {
        return $this->getPasswordRequestedAt() instanceof \DateTime &&
        $this->getPasswordRequestedAt()->getTimestamp() + 86400 > time();
    }

    public function setRoles(array $roles)
    {
        $this->roles = array();
        foreach ($roles as $role)
            $this->addRole($role);
        return $this;
    }

    public function getDn()
    {
        return $this->dn;
    }

    public function setDn($dn)
    {
        $this->dn = $dn;
        return $this;
    }

    public function getCn()
    {
        return $this->cn;
    }

    public function setCn($cn)
    {
        $this->cn = $cn;
        return $this;
    }

    public function getAttributes()
    {
        return $this->attributes;
    }

    public function setAttributes(array $attributes)
    {
        $this->attributes = $attributes;
        return $this;
    }

    public function getAttribute($name)
    {
        return isset($this->attributes[$name]) ? $this->attributes[$name] : null;
    }

    public function isEqualTo(UserInterface $user)
    {
        if (!$user instanceof LdapUserInterface
            || $user->getUsername() !== $this->username
            || $user->getEmail() !== $this->email
            || count(array_diff($user->getRoles(), $this->getRoles())) > 0
            || $user->getDn() !== $this->dn
        )
            return false;
        return true;
    }

    public function serialize()
    {
        return serialize(array(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->emailCanonical,
            $this->email,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->roles,
            $this->dn,
        ));
    }

    public function unserialize($serialized)
    {
        list(
            $this->password,
            $this->salt,
            $this->usernameCanonical,
            $this->username,
            $this->emailCanonical,
            $this->email,
            $this->expired,
            $this->locked,
            $this->credentialsExpired,
            $this->enabled,
            $this->id,
            $this->roles,
            $this->dn,
        ) = unserialize($serialized);
    }

    /**
     * Set photo
     *
     * @param \Bibliometry\MainBundle\Entity\Photo $photo
     * @return User
     */
    public function setPhoto(\Bibliometry\MainBundle\Entity\Photo $photo)
    {
        $this->photo = $photo;
        return $this;
    }

    /**
     * Get photo
     *
     * @return \Bibliometry\MainBundle\Entity\Photo 
     */
    public function getPhoto()
    {
        return $this->photo;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Researcher
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Researcher
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set researcher
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $researcher
     * @return User
     */
    public function setResearcher(\Bibliometry\MainBundle\Entity\Researcher $researcher)
    {
        $this->researcher = $researcher;
        $researcher->setUser($this);
        return $this;
    }

    /**
     * Get researcher
     *
     * @return \Bibliometry\MainBundle\Entity\Researcher 
     */
    public function getResearcher()
    {
        return $this->researcher;
    }

    /**
     * Set registrationCompleted
     *
     * @param boolean $registrationCompleted
     * @return User
     */
    public function setRegistrationCompleted($registrationCompleted)
    {
        $this->registrationCompleted = $registrationCompleted;
        return $this;
    }

    /**
     * Get registrationCompleted
     *
     * @return boolean 
     */
    public function getRegistrationCompleted()
    {
        return $this->registrationCompleted;
    }

    /**
     * Set registrationDate
     *
     * @param \DateTime $registrationDate
     * @return User
     */
    public function setRegistrationDate($registrationDate)
    {
        $this->registrationDate = $registrationDate;
        return $this;
    }

    /**
     * Get registrationDate
     *
     * @return \DateTime 
     */
    public function getRegistrationDate()
    {
        return $this->registrationDate;
    }

    /**
     * Add reportedErrors
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $reportedErrors
     * @return User
     */
    public function addReportedError(\Bibliometry\MainBundle\Entity\ErrorReport $reportedErrors)
    {
        $this->reportedErrors[] = $reportedErrors;
        return $this;
    }

    /**
     * Remove reportedErrors
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $reportedErrors
     */
    public function removeReportedError(\Bibliometry\MainBundle\Entity\ErrorReport $reportedErrors)
    {
        $this->reportedErrors->removeElement($reportedErrors);
    }

    /**
     * Get reportedErrors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getReportedErrors()
    {
        return $this->reportedErrors;
    }

    public function __toString()
    {
        return $this->name." ".$this->surname;
    }

    /**
     * Add notifications
     *
     * @param \Bibliometry\MainBundle\Entity\Notification $notifications
     * @return User
     */
    public function addNotification(\Bibliometry\MainBundle\Entity\Notification $notifications)
    {
        $this->notifications[] = $notifications;
        return $this;
    }

    /**
     * Remove notifications
     *
     * @param \Bibliometry\MainBundle\Entity\Notification $notifications
     */
    public function removeNotification(\Bibliometry\MainBundle\Entity\Notification $notifications)
    {
        $this->notifications->removeElement($notifications);
    }

    /**
     * Get notifications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
    
    public function getUnreadNotifications()
    {
        $unreadNotifications = array();
        foreach ($this->getNotifications() as $notification)
            if ($notification->getIsRead() == false)
                $unreadNotifications[] = $notification;
        return array_reverse($unreadNotifications);
    }

    /**
     * Set apiKey
     *
     * @param string $apiKey
     * @return User
     */
    public function setApiKey($apiKey)
    {
        $this->apiKey = $apiKey;
        return $this;
    }

    /**
     * Get apiKey
     *
     * @return string 
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }
    
    /**
     * @ORM\PrePersist
     */
    public function generateApiKey()
    {
        $this->apiKey = md5($this->getUsername());
    }
}
