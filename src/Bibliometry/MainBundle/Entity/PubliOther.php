<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PubliOther
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class PubliOther
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=255)
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="booktitle", type="string", length=255, nullable=true)
     */
    private $booktitle;

    /**
     * @var string
     *
     * @ORM\Column(name="publisher", type="string", length=255, nullable=true)
     */
    private $publisher;

    /**
     * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Researcher", inversedBy="supervisations")
     */
    private $supervisor;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="publiOther")
     */
    private $publication;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return PubliOther
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set booktitle
     *
     * @param string $booktitle
     * @return PubliOther
     */
    public function setBooktitle($booktitle)
    {
        $this->booktitle = $booktitle;

        return $this;
    }

    /**
     * Get booktitle
     *
     * @return string 
     */
    public function getBooktitle()
    {
        return $this->booktitle;
    }

    /**
     * Set publisher
     *
     * @param string $publisher
     * @return PubliOther
     */
    public function setPublisher($publisher)
    {
        $this->publisher = $publisher;

        return $this;
    }

    /**
     * Get publisher
     *
     * @return string 
     */
    public function getPublisher()
    {
        return $this->publisher;
    }

    /**
     * Set publication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publication
     * @return PubliOther
     */
    public function setPublication(\Bibliometry\MainBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->supervisor = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add supervisor
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $supervisor
     * @return PubliOther
     */
    public function addSupervisor(\Bibliometry\MainBundle\Entity\Researcher $supervisor)
    {
        $this->supervisor[] = $supervisor;
        $supervisor->addSupervisation($this);
        return $this;
    }

    /**
     * Remove supervisor
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $supervisor
     */
    public function removeSupervisor(\Bibliometry\MainBundle\Entity\Researcher $supervisor)
    {
        $this->supervisor->removeElement($supervisor);
    }

    /**
     * Get supervisor
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupervisor()
    {
        return $this->supervisor;
    }
}
