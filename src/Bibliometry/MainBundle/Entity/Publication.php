<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Publication
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\PublicationRepository")
 */
class Publication
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="HALId", type="string", length=255)
     */
    private $hALId;
    
    /**
     * @var string
     *
     * @ORM\Column(name="HALURL", type="string", length=255)
     */
    private $hALURL;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="text")
     */
    private $title;
    
    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="abstract", type="text", nullable=true)
     */
    private $abstract;
    
    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=255, nullable=true)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="version", type="integer", nullable=true)
     */
    private $version;
    
    /**
     * @var datetime $lastModificationDate
     *
     * @ORM\Column(name="lastModificationDate", type="datetime", nullable=true)
     */
    private $lastModificationDate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="PDFURL", type="string", length=255, nullable=true)
     */
    private $pDFURL;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="month", type="integer")
     */
    private $month;

    /**
     * @var string
     *
     * @ORM\Column(name="pages", type="string", length=255, nullable=true)
     */
    private $pages;
    
    /**
    * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Laboratory", cascade={"persist"}, inversedBy="publications")
    * @ORM\JoinColumn(nullable=false)
    */
    private $laboratories;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\PubliJournal", cascade={"persist","remove"}, mappedBy="publication", orphanRemoval=true)
    */
    private $publiJournal;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\PubliConference", cascade={"persist","remove"}, mappedBy="publication", orphanRemoval=true)
    */
    private $publiConference;
    
   /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\PubliOther", cascade={"persist","remove"}, mappedBy="publication", orphanRemoval=true)
    */
    private $publiOther;
    
    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\PublicationAuthor", cascade={"persist", "remove"}, mappedBy="publication")
    * @ORM\OrderBy({"authorRank" = "ASC"})
    */
    private $publicationAuthors;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\ResearchField", cascade={"persist"}, inversedBy="publications")
    */
    private $researchField;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\DuplicatePublications", mappedBy="rightPublication")
     */
    private $rightPublications;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\DuplicatePublications", mappedBy="wrongPublication")
     */
    private $wrongPublications;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\WrongPublicationMatch", mappedBy="publication")
     */
    private $wrongPublicationsMatches;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", cascade={"persist"}, inversedBy="mergedFrom")
     */
    private $mergedTo;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\Publication", mappedBy="mergedTo")
     */
    private $mergedFrom;
    
    /**
     * @var boolean $notFromLab
     *
     * @ORM\Column(name="notFromLab", type="boolean")
     */
    private $notFromLab;
    
    public function __toString()
    {
        return "#".$this->id." ".$this->title;
    }
    
    /**
     * @var string
     *
     * @ORM\Column(name="affiliationsNationalities", type="string", length=255)
     */
    private $affiliationsNationalities;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set hALId
     *
     * @param string $hALId
     * @return Publication
     */
    public function setHALId($hALId)
    {
        $this->hALId = $hALId;

        return $this;
    }

    /**
     * Get hALId
     *
     * @return string 
     */
    public function getHALId()
    {
        return $this->hALId;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Publication
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set abstract
     *
     * @param string $abstract
     * @return Publication
     */
    public function setAbstract($abstract)
    {
        $this->abstract = $abstract;

        return $this;
    }

    /**
     * Get abstract
     *
     * @return string 
     */
    public function getAbstract()
    {
        return $this->abstract;
    }

    /**
     * Set version
     *
     * @param integer $version
     * @return Publication
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer 
     */
    public function getVersion()
    {
        return $this->version;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->laboratories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publicationAuthors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->wrongPublicationsMatches = new \Doctrine\Common\Collections\ArrayCollection();
        $this->notFromLab = false;
    }

    /**
     * Add laboratories
     *
     * @param \Bibliometry\MainBundle\Entity\Laboratory $laboratories
     * @return Publication
     */
    public function addLaboratory(\Bibliometry\MainBundle\Entity\Laboratory $laboratories)
    {
        $this->laboratories[] = $laboratories;
        $laboratories->addPublication($this);
        return $this;
    }

    /**
     * Remove laboratories
     *
     * @param \Bibliometry\MainBundle\Entity\Laboratory $laboratories
     */
    public function removeLaboratory(\Bibliometry\MainBundle\Entity\Laboratory $laboratories)
    {
        $this->laboratories->removeElement($laboratories);
    }
    
    public function removeLaboratories()
    {
        $this->laboratories = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get laboratories
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLaboratories()
    {
        return $this->laboratories;
    }

    /**
     * Set publiJournal
     *
     * @param \Bibliometry\MainBundle\Entity\PubliJournal $publiJournal
     * @return Publication
     */
    public function setPubliJournal(\Bibliometry\MainBundle\Entity\PubliJournal $publiJournal)
    {
        $this->publiJournal = $publiJournal;
        $publiJournal->setPublication($this);
        return $this;
    }

    /**
     * Get publiJournal
     *
     * @return \Bibliometry\MainBundle\Entity\PubliJournal 
     */
    public function getPubliJournal()
    {
        return $this->publiJournal;
    }

    /**
     * Set publiConference
     *
     * @param \Bibliometry\MainBundle\Entity\PubliConference $publiConference
     * @return Publication
     */
    public function setPubliConference(\Bibliometry\MainBundle\Entity\PubliConference $publiConference)
    {
        $this->publiConference = $publiConference;
        $publiConference->setPublication($this);
        return $this;
    }

    /**
     * Get publiConference
     *
     * @return \Bibliometry\MainBundle\Entity\PubliConference 
     */
    public function getPubliConference()
    {
        return $this->publiConference;
    }
    
    /**
     * Set publiOther
     *
     * @param \Bibliometry\MainBundle\Entity\PubliOther $publiOther
     * @return Publication
     */
    public function setPubliOther(\Bibliometry\MainBundle\Entity\PubliOther $publiOther = null)
    {
        $this->publiOther = $publiOther;
        $publiOther->setPublication($this);
        return $this;
    }

    /**
     * Get publiOther
     *
     * @return \Bibliometry\MainBundle\Entity\PubliOther 
     */
    public function getPubliOther()
    {
        return $this->publiOther;
    }
    
    public function getType()
    {
        if ($this->getPubliConference() != NULL)
        {
            return "conference";
        }
        else if ($this->getPubliJournal() != NULL) {
            return "journal";
        }
        else if ($this->getPubliOther() != NULL) {
            return $this->getPubliOther()->getType();
        }
    }
    
    public function getDate()
    {
        return new \DateTime($this->getYear() . "-" . $this->getMonth());
    }
    
    public function getLinkedPubli()
    {
        if ($this->getPubliConference() != NULL)
        {
            return $this->getPubliConference();
        }
        else if ($this->getPubliJournal() != NULL) {
            return $this->getPubliJournal();
        }
        else {
            return NULL;
        }
    }

    /**
     * Set hALURL
     *
     * @param string $hALURL
     * @return Publication
     */
    public function setHALURL($hALURL)
    {
        $this->hALURL = $hALURL;

        return $this;
    }

    /**
     * Get hALURL
     *
     * @return string 
     */
    public function getHALURL()
    {
        return "http://hal.archives-ouvertes.fr/".$this->getHALId();
    }

    /**
     * Set pDFURL
     *
     * @param string $pDFURL
     * @return Publication
     */
    public function setPDFURL($pDFURL)
    {
        $this->pDFURL = $pDFURL;

        return $this;
    }

    /**
     * Get pDFURL
     *
     * @return string 
     */
    public function getPDFURL()
    {
        return $this->pDFURL;
    }

    /**
     * Add authors
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $authors
     * @param $rank
     * @return Publication
     */
    public function addAuthor(\Bibliometry\MainBundle\Entity\Researcher $authors, $rank = 0)
    {
        $publicationAuthor = new \Bibliometry\MainBundle\Entity\PublicationAuthor();
        $publicationAuthor->setPublication($this);
        $publicationAuthor->setResearcher($authors);
        $publicationAuthor->setAuthorRank($rank);
        
        $authors->addPublicationAuthor($publicationAuthor);
        $this->addPublicationAuthor($publicationAuthor);
    }

    /**
     * Remove authors
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $authors
     */
    public function removeAuthor(\Bibliometry\MainBundle\Entity\Researcher $authors)
    {
        foreach ($this->getPublicationAuthors() as $publicationAuthor)
        {
            if ($publicationAuthor->getResearcher() == $authors)
            {
                $rankToReturn = $publicationAuthor->getAuthorRank();
                $this->removePublicationAuthor($publicationAuthor);
                return $rankToReturn;
            }
        }
    }

    /**
     * Get authors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAuthors()
    {
        $authors = [];
        foreach ($this->getPublicationAuthors() as $publicationAuthor)
        {
            $authors[] = $publicationAuthor->getResearcher();
        }
        
        return $authors;
    }

    /**
     * Set researchField
     *
     * @param \Bibliometry\MainBundle\Entity\ResearchField $researchField
     * @return Publication
     */
    public function setResearchField(\Bibliometry\MainBundle\Entity\ResearchField $researchField = null)
    {
        $this->researchField = $researchField;
        $researchField->addPublication($this);
        return $this;
    }

    /**
     * Get researchField
     *
     * @return \Bibliometry\MainBundle\Entity\ResearchField 
     */
    public function getResearchField()
    {
        return $this->researchField;
    }
    
    private static function comparePublications($publication1, $publication2)
    {
        $year1 = $publication1->getYear();
        $year2 = $publication2->getYear();
        
        $month1 = $publication1->getMonth();
        $month2 = $publication2->getMonth();
        
        // Compare on years
        if ($year1 > $year2)
        {
            return -1;
        }
        else if ($year1 < $year2)
        {
            return 1;
        }
        // Compare on months
        if ($month1 > $month2)
        {
            return -1;
        }
        else if ($month1 < $month2)
        {
            return 1;
        }
        // If equals, compare on first author surname
        else 
        {
            $author1Surname = $publication1->getAuthors()[0]->getSurname();
            $author2Surname = $publication2->getAuthors()[0]->getSurname();
            
            $authorCmp = strcmp($author1Surname, $author2Surname);
            if ($authorCmp != 0)
            {
                return $authorCmp;
            }
            else
            {
                return \Bibliometry\MainBundle\Entity\Publication::comparePublicationsAccordingToRanking($publication1, $publication2);
            }
        }
    }
    
    private static function compareHydratedPublications($publication1, $publication2)
    {
        $year1 = $publication1["year"];
        $year2 = $publication2["year"];
        
        $month1 = $publication1["month"];
        $month2 = $publication2["month"];
        
        // Compare on years
        if ($year1 > $year2)
        {
            return -1;
        }
        else if ($year1 < $year2)
        {
            return 1;
        }
        // Compare on months
        if ($month1 > $month2)
        {
            return -1;
        }
        else if ($month1 < $month2)
        {
            return 1;
        }
        // If equals, compare on first author surname
        else 
        {
            $author1Surname = $publication1["publicationAuthors"][0]["researcher"]["surname"];
            $author2Surname = $publication2["publicationAuthors"][0]["researcher"]["surname"];
            
            $authorCmp = strcmp($author1Surname, $author2Surname);
            if ($authorCmp != 0)
            {
                return $authorCmp;
            }
            else
            {
                return \Bibliometry\MainBundle\Entity\Publication::compareHydratedPublicationsAccordingToRanking($publication1, $publication2);
            }
        }
    }
    
    private static function comparePublicationsAlphabetical($publication1, $publication2)
    {
        $author1Surname = $publication1->getAuthors()[0]->getSurname();
        $author2Surname = $publication2->getAuthors()[0]->getSurname();
        
        $authorCmp = strcmp($author1Surname, $author2Surname);
        if ($authorCmp != 0)
        {
            return $authorCmp;
        }
        else
        {
            $year1 = $publication1->getYear();
            $year2 = $publication2->getYear();
            
            $month1 = $publication1->getMonth();
            $month2 = $publication2->getMonth();
            
            // Compare on years
            if ($year1 > $year2)
            {
                return -1;
            }
            else if ($year1 < $year2)
            {
                return 1;
            }
            // Compare on months
            if ($month1 > $month2)
            {
                return -1;
            }
            else if ($month1 < $month2)
            {
                return 1;
            }
            else
            {
                return \Bibliometry\MainBundle\Entity\Publication::comparePublicationsAccordingToRanking($publication1, $publication2);
            }
        }
    }
    
    private static function compareHydratedPublicationsAlphabetical($publication1, $publication2)
    {
        $author1Surname = $publication1["publicationAuthors"][0]["researcher"]["surname"];
        $author2Surname = $publication2["publicationAuthors"][0]["researcher"]["surname"];
        
        $authorCmp = strcmp($author1Surname, $author2Surname);
        if ($authorCmp != 0)
        {
            return $authorCmp;
        }
        else
        {
            $year1 = $publication1["year"];
            $year2 = $publication2["year"];
            
            $month1 = $publication1["month"];
            $month2 = $publication2["month"];
            
            // Compare on years
            if ($year1 > $year2)
            {
                return -1;
            }
            else if ($year1 < $year2)
            {
                return 1;
            }
            // Compare on months
            if ($month1 > $month2)
            {
                return -1;
            }
            else if ($month1 < $month2)
            {
                return 1;
            }
            else
            {
                return \Bibliometry\MainBundle\Entity\Publication::compareHydratedPublicationsAccordingToRanking($publication1, $publication2);
            }
        }
    }
    
    public static function sortPublicationsByTime($publications)
    {
        if (! is_array($publications))
        {
            $arrayPublis = $publications->toArray();
        }
        else
        {
            $arrayPublis = $publications;
        }
        usort($arrayPublis, array('\Bibliometry\MainBundle\Entity\Publication', 'comparePublications'));
        return $arrayPublis;
    }
    
    public static function sortHydratedPublicationsByTime($publications)
    {
        if (! is_array($publications))
        {
            $arrayPublis = $publications->toArray();
        }
        else
        {
            $arrayPublis = $publications;
        }
        usort($arrayPublis, array('\Bibliometry\MainBundle\Entity\Publication', 'compareHydratedPublications'));
        return $arrayPublis;
    }
    
    public static function sortPublicationsByRanking($publications)
    {
        if (! is_array($publications))
        {
            $arrayPublis = $publications->toArray();
        }
        else
        {
            $arrayPublis = $publications;
        }
        usort($arrayPublis, array('\Bibliometry\MainBundle\Entity\Publication', 'comparePublicationsAccordingToTypeAndRanking'));
        return $arrayPublis;
    }
    
    public static function sortHydratedPublicationsByRanking($publications)
    {
        if (! is_array($publications))
        {
            $arrayPublis = $publications->toArray();
        }
        else
        {
            $arrayPublis = $publications;
        }
        usort($arrayPublis, array('\Bibliometry\MainBundle\Entity\Publication', 'compareHydratedPublicationsAccordingToTypeAndRanking'));
        return $arrayPublis;
    }
    
    public static function sortPublicationsByAlphabetical($publications)
    {
        if (! is_array($publications))
        {
            $arrayPublis = $publications->toArray();
        }
        else
        {
            $arrayPublis = $publications;
        }
        usort($arrayPublis, array('\Bibliometry\MainBundle\Entity\Publication', 'comparePublicationsAlphabetical'));
        return $arrayPublis;
    }
    
    public static function sortHydratedPublicationsByAlphabetical($publications)
    {
        if (! is_array($publications))
        {
            $arrayPublis = $publications->toArray();
        }
        else
        {
            $arrayPublis = $publications;
        }
        usort($arrayPublis, array('\Bibliometry\MainBundle\Entity\Publication', 'compareHydratedPublicationsAlphabetical'));
        return $arrayPublis;
    }
    
    public static function getReferencesPublications($publications)
    {
        $references = array();
        foreach ($publications as $publication)
        {
            // Author display : 1, 2 or more
            if (count($publication->getAuthors()) == 1)
            {
                $author = $publication->getAuthors()[0]->getSurname();
            }
            else if (count($publication->getAuthors()) == 2)
            {
                $author = $publication->getAuthors()[0]->getSurname()." and ".$publication->getAuthors()[1]->getSurname();
            }
            else
            {
                $author = $publication->getAuthors()[0]->getSurname()." et al.";
            }
            // Concat year
            $reference = $author." ".$publication->getYear();
            
            if (in_array($reference, array_keys($references)))
            {
                $references[$reference][] = $publication;
            }
            else
            {
                $references[$reference] = array($publication);
            }
        }
        
        $toReturn = array();
        foreach ($references as $reference => $publications)
        {
            if (count($publications) == 1)
            {
                $toReturn[$publications[0]->getId()] = $reference;
            }
            else
            {
                $letter = "a";
                usort($publications, array('\Bibliometry\MainBundle\Entity\Publication', 'comparePublicationsAccordingToTypeAndRanking'));
                foreach ($publications as $publication)
                {
                    $toReturn[$publication->getId()] = $reference.$letter;
                    $letter++;
                }
            }
        }
        
        return $toReturn;
    }
    
    public static function getReferencesHydratedPublications($publications)
    {
        $references = array();
        foreach ($publications as $publication)
        {
            // Author display : 1, 2 or more
            if (count($publication["publicationAuthors"]) == 1)
            {
                $author = $publication["publicationAuthors"][0]["researcher"]["surname"];
            }
            else if (count($publication["publicationAuthors"]) == 2)
            {
                $author = $publication["publicationAuthors"][0]["researcher"]["surname"]." and ".$publication["publicationAuthors"][1]["researcher"]["surname"];
            }
            else
            {
                $author = $publication["publicationAuthors"][0]["researcher"]["surname"]." et al.";
            }
            // Concat year
            $reference = $author." ".$publication["year"];
            
            if (in_array($reference, array_keys($references)))
            {
                $references[$reference][] = $publication;
            }
            else
            {
                $references[$reference] = array($publication);
            }
        }
        
        $toReturn = array();
        foreach ($references as $reference => $publications)
        {
            if (count($publications) == 1)
            {
                $toReturn[$publications[0]["id"]] = $reference;
            }
            else
            {
                $letter = "a";
                usort($publications, array('\Bibliometry\MainBundle\Entity\Publication', 'compareHydratedPublicationsAccordingToTypeAndRanking'));
                foreach ($publications as $publication)
                {
                    $toReturn[$publication["id"]] = $reference.$letter;
                    $letter++;
                }
            }
        }
        
        return $toReturn;
    }
    
    private static function comparePublicationsAccordingToTypeAndRanking($publication1, $publication2)
    {
        $type1 = $publication1->getType();
        $type2 = $publication2->getType();
        // Journal is always before conference
        if ($type1 === "journal" and $type2 === "conference")
        {
            return -1;
        }
        // Conference is always after journal
        else if ($type1 === "conference" and $type2 == "journal")
        {
            return 1;
        }
        // Conference and journal are always before other publications type
        else if(($type1 === "journal" || $type1 === "conference") && !($type2 === "journal" || $type2 === "conference"))
        {
            return -1;
        }
        // Other publications type  are always after Conference and journal
        else if(!($type1 === "journal" || $type1 === "conference") && ($type2 === "journal" || $type2 === "conference"))
        {
            return 1;
        }

        // If same type, compare according to publication ranking
        if (
            ($publication1->getType() == "journal" and $publication2->getType() == "journal") ||
            ($publication1->getType() == "conference" and $publication2->getType() == "conference")
           )
        {
            return \Bibliometry\MainBundle\Entity\Publication::comparePublicationsAccordingToRanking($publication1, $publication2);
        }
        return 0;
    }
    
    private static function compareHydratedPublicationsAccordingToTypeAndRanking($publication1, $publication2)
    {
        $type1 = $publication1["type"];
        $type2 = $publication2["type"];
        // Journal is always before conference
        if ($type1 === "journal" and $type2 === "conference")
        {
            return -1;
        }
        // Conference is always after journal
        else if ($type1 === "conference" and $type2 == "journal")
        {
            return 1;
        }
        // Conference and journal are always before other publications type
        else if(($type1 === "journal" || $type1 === "conference") && !($type2 === "journal" || $type2 === "conference"))
        {
            return -1;
        }
        // Other publications type  are always after Conference and journal
        else if(!($type1 === "journal" || $type1 === "conference") && ($type2 === "journal" || $type2 === "conference"))
        {
            return 1;
        }

        return \Bibliometry\MainBundle\Entity\Publication::compareHydratedPublicationsAccordingToRanking($publication1, $publication2);
    }
    
    private static function comparePublicationsAccordingToRanking($publication1, $publication2)
    {
        // Not the same type of publication
        if ($publication1->getType() != $publication2->getType())
        {
            // This trick ensure that we get always the same order if EVERY parameters are equals
            return $publication1->getId() - $publication2->getId();
        }
        
        // Compare according to journal quartile
        if ($publication1->getType() == "journal")
        {
            $IF1 = $publication1->getPubliJournal()->getImpactFactor();
            $IF2 = $publication2->getPubliJournal()->getImpactFactor();
            
            // If one or both journal don't have IF
            if ($IF1 == NULL && $IF2 == NULL)
            {
                // This trick ensure that we get always the same order if EVERY parameters are equals
                return $publication1->getId() - $publication2->getId();
            }
            else if ($IF1 == NULL && $IF2 != NULL)
            {
                return 1;
            }
            else if ($IF1 != NULL && $IF2 == NULL)
            {
                return -1;
            }
            else
            {
                $quartileCmp = $IF1->getQuartile() - $IF2->getQuartile();
                if ($quartileCmp == 0)
                {
                    // This trick ensure that we get always the same order if EVERY parameters are equals
                    return $publication1->getId() - $publication2->getId();
                }
                else
                {
                    return $quartileCmp;
                }
            }
        }
        // Compare according to conference ranking
        else if ($publication1->getType() == "conference")
        {
            $ranking1 = $publication1->getPubliConference()->getConferenceRanking();
            $ranking2 = $publication2->getPubliConference()->getConferenceRanking();
            // If one or both conferences don't have ranking
            if ($ranking1 == NULL && $ranking2 == NULL)
            {
                // This trick ensure that we get always the same order if EVERY parameters are equals
                return $publication1->getId() - $publication2->getId();
            }
            else if ($ranking1 == NULL && $ranking2 != NULL)
            {
                return 1;
            }
            else if ($ranking1 != NULL && $ranking2 == NULL)
            {
                return -1;
            }
            else
            {
                $rankingCmp = $ranking1->getValueInteger() - $ranking2->getValueInteger();
                if ($rankingCmp == 0)
                {
                    // This trick ensure that we get always the same order if EVERY parameters are equals
                    return $publication1->getId() - $publication2->getId();
                }
                else
                {
                    return $rankingCmp;
                }
            }
        }
    }
    
    private static function compareHydratedPublicationsAccordingToRanking($publication1, $publication2)
    {
        
        $year1 = $publication1["year"];
        $year2 = $publication2["year"];
        
        $month1 = $publication1["month"];
        $month2 = $publication2["month"];
        
        // Compare according to journal quartile
        if ($publication1["type"] == "journal" && $publication2["type"] == "journal")
        {
            // If one or both journal don't have IF
            if (!$publication1["publiJournal"]["impactFactor"] && !$publication2["publiJournal"]["impactFactor"])
            {
                // Compare on years
                if ($year1 > $year2)
                {
                    return -1;
                }
                else if ($year1 < $year2)
                {
                    return 1;
                }
                // Compare on months
                if ($month1 > $month2)
                {
                    return -1;
                }
                else if ($month1 < $month2)
                {
                    return 1;
                }
                
                // This trick ensure that we get always the same order if EVERY parameters are equals
                return $publication1["id"] - $publication2["id"];
            }
            else if (!$publication1["publiJournal"]["impactFactor"])
            {
                return 1;
            }
            else if (!$publication2["publiJournal"]["impactFactor"])
            {
                return -1;
            }
            else
            {
                $IF1 = $publication1["publiJournal"]["impactFactor"];
                $IF2 = $publication2["publiJournal"]["impactFactor"];
                $quartileCmp = $IF1["quartile"] - $IF2["quartile"];
                if ($quartileCmp == 0)
                {
                    // Compare on years
                    if ($year1 > $year2)
                    {
                        return -1;
                    }
                    else if ($year1 < $year2)
                    {
                        return 1;
                    }
                    // Compare on months
                    if ($month1 > $month2)
                    {
                        return -1;
                    }
                    else if ($month1 < $month2)
                    {
                        return 1;
                    }
                    
                    // This trick ensure that we get always the same order if EVERY parameters are equals
                    return $publication1["id"] - $publication2["id"];
                }
                else
                {
                    return $quartileCmp;
                }
            }
        }
        // Compare according to conference ranking
        else if ($publication1["type"] == "conference" && $publication2["type"] == "conference")
        {
            // If one or both conferences don't have ranking
            if (!$publication1["publiConference"]["conferenceRanking"] && !$publication2["publiConference"]["conferenceRanking"])
            {
                // Compare on years
                if ($year1 > $year2)
                {
                    return -1;
                }
                else if ($year1 < $year2)
                {
                    return 1;
                }
                // Compare on months
                if ($month1 > $month2)
                {
                    return -1;
                }
                else if ($month1 < $month2)
                {
                    return 1;
                }
                
                // This trick ensure that we get always the same order if EVERY parameters are equals
                return $publication1["id"] - $publication2["id"];
            }
            else if (!$publication1["publiConference"]["conferenceRanking"])
            {
                return 1;
            }
            else if (!$publication2["publiConference"]["conferenceRanking"])
            {
                return -1;
            }
            else
            {
                $ranking1 = $publication1["publiConference"]["conferenceRanking"];
                $ranking2 = $publication2["publiConference"]["conferenceRanking"];
                $rankingCmp = \Bibliometry\MainBundle\Entity\ConferenceRanking::getValueIntegerOfHydratedRanking($ranking1);
                $rankingCmp -= \Bibliometry\MainBundle\Entity\ConferenceRanking::getValueIntegerOfHydratedRanking($ranking2);
                if ($rankingCmp == 0)
                {
                    // Compare on years
                    if ($year1 > $year2)
                    {
                        return -1;
                    }
                    else if ($year1 < $year2)
                    {
                        return 1;
                    }
                    // Compare on months
                    if ($month1 > $month2)
                    {
                        return -1;
                    }
                    else if ($month1 < $month2)
                    {
                        return 1;
                    }
                    // This trick ensure that we get always the same order if EVERY parameters are equals
                    return $publication1["id"] - $publication2["id"];
                }
                else
                {
                    return $rankingCmp;
                }
            }
        }
        

        // Compare on years
        if ($year1 > $year2)
        {
            return -1;
        }
        else if ($year1 < $year2)
        {
            return 1;
        }
        // Compare on months
        if ($month1 > $month2)
        {
            return -1;
        }
        else if ($month1 < $month2)
        {
            return 1;
        }

        // This trick ensure that we get always the same order if EVERY parameters are equals
        return $publication1["id"] - $publication2["id"];
    }

    /**
     * Set language
     *
     * @param string $language
     * @return Publication
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string 
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Publication
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function getRankingOrIF()
    {
        if ($this->getType() == "conference")
        {
            $ranking = $this->getPubliConference()->getConferenceRanking();
            if ($ranking == NULL)
            {
                if ($this->getLanguage() == "fr")
                {
                    return "FR";
                }
                else
                {
                    return "NC";
                }
            }
            else
            {
                return $ranking->getValue();
            }
        }
        else if ($this->getType() == "journal")
        {
            $IF = $this->getPubliJournal()->getImpactFactor();
            if ($IF == NULL)
            {
                return "NC";
            }
            else
            {
                return $IF->getValue()." (Q".$IF->getQuartile().")";
            }
        }
    }

    /**
     * Add rightPublications
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicatePublications $rightPublications
     * @return Publication
     */
    public function addRightPublication(\Bibliometry\MainBundle\Entity\DuplicatePublications $rightPublications)
    {
        $this->rightPublications[] = $rightPublications;

        return $this;
    }

    /**
     * Remove rightPublications
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicatePublications $rightPublications
     */
    public function removeRightPublication(\Bibliometry\MainBundle\Entity\DuplicatePublications $rightPublications)
    {
        $this->rightPublications->removeElement($rightPublications);
    }

    /**
     * Get rightPublications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRightPublications()
    {
        return $this->rightPublications;
    }

    /**
     * Add wrongPublications
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicatePublications $wrongPublications
     * @return Publication
     */
    public function addWrongPublication(\Bibliometry\MainBundle\Entity\DuplicatePublications $wrongPublications)
    {
        $this->wrongPublications[] = $wrongPublications;

        return $this;
    }

    /**
     * Remove wrongPublications
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicatePublications $wrongPublications
     */
    public function removeWrongPublication(\Bibliometry\MainBundle\Entity\DuplicatePublications $wrongPublications)
    {
        $this->wrongPublications->removeElement($wrongPublications);
    }

    /**
     * Get wrongPublications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWrongPublications()
    {
        return $this->wrongPublications;
    }

    /**
     * Add wrongPublicationsMatches
     *
     * @param \Bibliometry\MainBundle\Entity\WrongPublicationMatch $wrongPublicationsMatches
     * @return Publication
     */
    public function addWrongPublicationsMatch(\Bibliometry\MainBundle\Entity\WrongPublicationMatch $wrongPublicationsMatches)
    {
        $this->wrongPublicationsMatches[] = $wrongPublicationsMatches;

        return $this;
    }

    /**
     * Remove wrongPublicationsMatches
     *
     * @param \Bibliometry\MainBundle\Entity\WrongPublicationMatch $wrongPublicationsMatches
     */
    public function removeWrongPublicationsMatch(\Bibliometry\MainBundle\Entity\WrongPublicationMatch $wrongPublicationsMatches)
    {
        $this->wrongPublicationsMatches->removeElement($wrongPublicationsMatches);
    }

    /**
     * Get wrongPublicationsMatches
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getWrongPublicationsMatches()
    {
        return $this->wrongPublicationsMatches;
    }

    /**
     * Set mergedTo
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $mergedTo
     * @return Publication
     */
    public function setMergedTo(\Bibliometry\MainBundle\Entity\Publication $mergedTo = null)
    {
        $this->mergedTo = $mergedTo;
        $mergedTo->addMergedFrom($this);
        return $this;
    }

    /**
     * Get mergedTo
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getMergedTo()
    {
        return $this->mergedTo;
    }

    /**
     * Add mergedFrom
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $mergedFrom
     * @return Publication
     */
    public function addMergedFrom(\Bibliometry\MainBundle\Entity\Publication $mergedFrom)
    {
        $this->mergedFrom[] = $mergedFrom;

        return $this;
    }

    /**
     * Remove mergedFrom
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $mergedFrom
     */
    public function removeMergedFrom(\Bibliometry\MainBundle\Entity\Publication $mergedFrom)
    {
        $this->mergedFrom->removeElement($mergedFrom);
    }

    /**
     * Get mergedFrom
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getMergedFrom()
    {
        return $this->mergedFrom;
    }
    
    public function mergeWith($rightPublication)
    {
        // Clear all linked data
        $this->laboratories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publicationAuthors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publiConference = null;
        $this->publiJournal = null;
        
        $this->setMergedTo($rightPublication);
    }
    
    public function removeFromLab()
    {
        // Clear all linked data
        $this->laboratories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publicationAuthors = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publiConference = null;
        $this->publiJournal = null;
        
        $this->setNotFromLab(true);
    }
    
    public function changeJournalOrConference($newSupport, $supportType)
    {
        // The type of support doesn't change
        if ($this->getType() == "conference" && $supportType == "conference")
        {
            $this->getPubliConference()->setConference($newSupport);
        }
        else if ($this->getType() == "journal" && $supportType == "journal")
        {
            $this->getPubliJournal()->setJournal($newSupport);
        }
        else
        {
            // It was a journal, now it's a conference
            if ($supportType == "conference")
            {
                $publiConference = new PubliConference();
                $publiConference->setTitleOfConferenceInHAL($newSupport->getTitle());
                $publiConference->setConference($newSupport);
                $this->setPubliConference($publiConference);
                $this->publiJournal = null;
            }
            // It was a conference, now it's a journal
            else if ($supportType == "journal")
            {
                $publiJournal = new PubliJournal();
                $publiJournal->setJournal($newSupport);
                $this->setPubliJournal($publiJournal);
                $this->publiConference = null;
            }
        }
    }

    /**
     * Set notFromLab
     *
     * @param boolean $notFromLab
     * @return Publication
     */
    public function setNotFromLab($notFromLab)
    {
        $this->notFromLab = $notFromLab;

        return $this;
    }

    /**
     * Get notFromLab
     *
     * @return boolean 
     */
    public function getNotFromLab()
    {
        return $this->notFromLab;
    }

    /**
     * Add publicationAuthors
     *
     * @param \Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors
     * @return Publication
     */
    public function addPublicationAuthor(\Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors)
    {
        $this->publicationAuthors[] = $publicationAuthors;
        return $this;
    }

    /**
     * Remove publicationAuthors
     *
     * @param \Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors
     */
    public function removePublicationAuthor(\Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors)
    {
        $this->publicationAuthors->removeElement($publicationAuthors);
    }

    /**
     * Get publicationAuthors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublicationAuthors()
    {
        return $this->publicationAuthors;
    }

    /**
     * Set lastModificationDate
     *
     * @param \DateTime $lastModificationDate
     * @return Publication
     */
    public function setLastModificationDate($lastModificationDate)
    {
        $this->lastModificationDate = $lastModificationDate;

        return $this;
    }

    /**
     * Get lastModificationDate
     *
     * @return \DateTime 
     */
    public function getLastModificationDate()
    {
        return $this->lastModificationDate;
    }


    /**
     * Set year
     *
     * @param integer $year
     * @return PubliConference
     */
    public function setYear($year)
    {
        $this->year = $year;
    
        return $this;
    }
    
    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set month
     *
     * @param integer $month
     * @return PubliConference
     */
    public function setMonth($month)
    {
        $this->month = $month;

        return $this;
    }

    /**
     * Get month
     *
     * @return integer 
     */
    public function getMonth()
    {
        return $this->month;
    }

    /**
     * Set pages
     *
     * @param string $pages
     * @return PubliConference
     */
    public function setPages($pages)
    {
        $this->pages = $pages;

        return $this;
    }

    /**
     * Get pages
     *
     * @return string 
     */
    public function getPages()
    {
        if (preg_match("/\d+/", $this->pages) == 1)
        {
            return $this->pages;
        }
        else
        {
            return NULL;
        }
    }

    /**
     * Set affiliationsNationalities
     *
     * @param string $affiliationsNationalities
     *
     * @return Publication
     */
    public function setAffiliationsNationalities($affiliationsNationalities)
    {
        $this->affiliationsNationalities = $affiliationsNationalities;

        return $this;
    }

    /**
     * Get affiliationsNationalities
     *
     * @return string
     */
    public function getAffiliationsNationalities()
    {
        return $this->affiliationsNationalities;
    }
    
    private $halidEurLabs = [453061,     // SAINBIOSE
                             134235,     // BiiGC
                             139739, 700,// CREATIS
                             31214,      // LaMCoS
                             31551,      // MATEIS
                             209650,     // LGF
                             698];       // LTDS    
    public function satisfyConstraint($constraint = "all")
    {
        if ($constraint == "all")
            return true;
        else if ($constraint == "foreignCoauthor")
            return count(explode(";", $this->affiliationsNationalities)) > 1;
        else if ($constraint == "eurSleightCoauthor") {
            foreach($this->laboratories as $lab)
                if (in_array($lab->HALID, $this->halidEurLabs))
                    return true;
            return false;
        }
        else if ($constraint == "frenchConferences" || $constraint == "internationalConferences") {
            if ($this->getType() != "conference")
                return false;
            if($this->getPubliConference()->getConferenceRanking() != NULL)  // ranked conferences are considered international
                return $constraint == "internationalConferences";
            else if (strtolower($this->language) == "fr")
                return $constraint == "frenchConferences";
            else
                return $constraint == "internationalConferences";
        }
    }
}
