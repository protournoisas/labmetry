<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Conference
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\ConferenceRepository")
 */
class Conference
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;
    
    /**
     * @Gedmo\Slug(fields={"title"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;
    
     /**
     * @var string
     *
     * @ORM\Column(name="preProcessedTitle", type="string", length=255)
     */
    private $preProcessedTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="acronym", type="string", length=255, nullable=true)
     */
    private $acronym;
    
    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\PubliConference", mappedBy="conference")
    */
    private $publiConferences;
    
    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\ConferenceRanking", mappedBy="conference", cascade = {"persist"})
    */
    private $conferenceRankings;

    public function __toString()
    {
        return $this->title;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Conference
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set acronym
     *
     * @param string $acronym
     * @return Conference
     */
    public function setAcronym($acronym)
    {
        $this->acronym = $acronym;

        return $this;
    }

    /**
     * Get acronym
     *
     * @return string 
     */
    public function getAcronym()
    {
        return $this->acronym;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->publiConferences = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add publiConferences
     *
     * @param \Bibliometry\MainBundle\Entity\PubliConference $publiConferences
     * @return Conference
     */
    public function addPubliConference(\Bibliometry\MainBundle\Entity\PubliConference $publiConferences)
    {
        $this->publiConferences[] = $publiConferences;

        return $this;
    }

    /**
     * Remove publiConferences
     *
     * @param \Bibliometry\MainBundle\Entity\PubliConference $publiConferences
     */
    public function removePubliConference(\Bibliometry\MainBundle\Entity\PubliConference $publiConferences)
    {
        $this->publiConferences->removeElement($publiConferences);
    }

    /**
     * Get publiConferences
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPubliConferences()
    {
        return $this->publiConferences;
    }
    
    /**
     * Get Number of publication in this conference
     */
    public function getNbPubli()
    {
        $nb = 0;
        foreach($this->publiConferences as $publiConference)
            if ($publiConference->getPublication())
                $nb += 1;
        return $nb;
    }
    
    public function getEditions()
    {
        $toReturn = array();
        $seenYears = array();
        foreach ($this->getPubliConferences() as $publiConference)
        {
            if (!in_array($publiConference->getYear(), $seenYears))
            {
                $toReturn[] = $publiConference;
                $seenYears[] = $publiConference->getYear();
            }
        }
        usort($toReturn, array('\Bibliometry\MainBundle\Entity\Conference', 'compareEditions'));
        return $toReturn;
    }
    
    private static function compareEditions($edition1, $edition2)
    {
        if ($edition1->getYear() > $edition2->getYear())
        {
            return -1;
        }
        else if ($edition1->getYear() < $edition2->getYear())
        {
            return 1;
        }
        else 
        {
            return 0;
        }
    }

    /**
     * Add conferenceRankings
     *
     * @param \Bibliometry\MainBundle\Entity\ConferenceRanking $conferenceRanking
     * @return Conference
     */
    public function addConferenceRanking(\Bibliometry\MainBundle\Entity\ConferenceRanking $conferenceRanking)
    {
        $this->conferenceRankings[] = $conferenceRanking;
        $conferenceRanking->setConference($this);
        return $this;
    }

    /**
     * Remove conferenceRankings
     *
     * @param \Bibliometry\MainBundle\Entity\ConferenceRanking $conferenceRanking
     */
    public function removeConferenceRanking(\Bibliometry\MainBundle\Entity\ConferenceRanking $conferenceRanking)
    {
        $this->conferenceRankings->removeElement($conferenceRanking);
    }

    /**
     * Get conferenceRankings
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getConferenceRankings()
    {
        return $this->conferenceRankings;
    }
    
    /**
     * Get lastConferenceRanking
     *
     * @return \Bibliometry\MainBundle\Entity\ConferenceRanking 
     */
    public function getLastConferenceRanking($limitYear = 9999)
    {
        $maxYear = 0;
        $lastRanking = NULL;
        foreach ($this->getConferenceRankings() as $ranking)
        {
            if ($ranking->getYear() > $maxYear && $ranking->getYear() <= $limitYear)
            {
                $maxYear = $ranking->getYear();
                $lastRanking = $ranking;
            }
        }
        
        return $lastRanking;
    }
    
    /**
     * Get ConferenceRanking of a year
     *
     * @return \Bibliometry\MainBundle\Entity\ConferenceRanking 
     */
    public function getConferenceRankingYear($year)
    {
        foreach ($this->getConferenceRankings() as $ranking)
        {
            if ($ranking->getYear() == $year)
            {
                return $ranking;
            }
        }
        // If not found for this year, return last
        return $this->getLastConferenceRanking($year);
    }
    
    public function getConferenceRankingLabel()
    {
        $ranking = $this->getLastConferenceRanking();
        
        // No ranking
        if ($ranking == NULL)
        {
            // Check if conference is FR
            $publiConferences = $this->getPubliConferences();
            $languages = [ "fr" => 0, "other" => 0 ];
            foreach ($publiConferences as $publiConference)
            {
                if ($publiConference->getPublication()->getLanguage() == "fr")
                {
                    $languages["fr"]++;
                }
                else
                {
                    $languages["other"]++;
                }
            }
            if ($languages["fr"] > $languages["other"])
            {
                return "FR";
            }
            else
            {
                return "NC";
            }
        }
        // Return ranking
        else
        {
            return $ranking->getValue();
        }
    }
    
    public function getCoreLink()
    {
        if ($this->getLastConferenceRanking() != NULL)
        {
            return "http://portal.core.edu.au/conf-ranks/?search=".$this->getTitle()."&by=all&source=all&sort=atitle&page=1";
        }
        else {
            return NULL;
        }
    }

    /**
     * Set preProcessedTitle
     *
     * @param string $preProcessedTitle
     * @return Conference
     */
    public function setPreProcessedTitle($preProcessedTitle)
    {
        $this->preProcessedTitle = $preProcessedTitle;

        return $this;
    }

    /**
     * Get preProcessedTitle
     *
     * @return string 
     */
    public function getPreProcessedTitle()
    {
        return $this->preProcessedTitle;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Conference
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }

    public function getPublications()
    {
        $toReturn = array();
        foreach ($this->getPubliConferences() as $publiConference)
        {
            $toReturn[] = $publiConference->getPublication();
        }
        return $toReturn;
    }
}
