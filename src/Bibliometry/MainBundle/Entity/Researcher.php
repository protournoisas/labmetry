<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Researcher
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\ResearcherRepository")
 */
class Researcher
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string")
     */
    private $surname;

    /**
     * @var array
     *
     * @ORM\Column(name="altNames", type="array")
     */
    private $altNames;

    /**
     * @var array
     *
     * @ORM\Column(name="altSurnames", type="array")
     */
    private $altSurnames;
    
    /**
     * @Gedmo\Slug(fields={"name","surname"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;
    
    /**
     * @var string
     *
     * @ORM\Column(name="googleProfile", type="string", length=255, nullable=true)
     */
    private $googleProfile;
    
    /**
     * @var string
     *
     * @ORM\Column(name="hIndex", type="integer", nullable=true)
     */
    private $hIndex;
    
    /**
     * @var string
     *
     * @ORM\Column(name="dblpProfile", type="string", length=255, nullable=true)
     */
    private $dblpProfile;


    /**
    * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\Team", mappedBy="leader")
    */
    private $leadTeams;
    
    /**
    * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Team", mappedBy="researchers")
    */
    private $teams;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\PublicationAuthor", cascade={"persist", "remove"}, mappedBy="researcher")
     * @ORM\OrderBy({"authorRank" = "ASC"})
     */
    private $publicationAuthors;
    
    /**
    * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\PubliOther", mappedBy="supervisor")
    */
    private $supervisations;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\User", mappedBy="researcher")
    * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
    */
    private $user;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\DuplicateResearchers", mappedBy="rightResearcher", cascade={"remove"})
     */
    private $rightDuplicateResearchers;
    
    /**
     * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\DuplicateResearchers", mappedBy="duplicateResearchers", cascade={"remove"})
     */
    private $duplicateResearchers;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\SplitResearchers", mappedBy="multipleResearcher", cascade={"remove"})
     */
    private $splitMultipleResearchers;
    
    /**
     * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\SplitResearchers", mappedBy="splitResearchers", cascade={"remove"})
     */
    private $splitResearchers;
    
    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Researcher
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     * @return Researcher
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string 
     */
    public function getSurname()
    {
        return $this->surname;
    }
    
    public function __toString()
    {
        return $this->getName()." ".$this->getSurname();
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->leadTeams = new \Doctrine\Common\Collections\ArrayCollection();
        $this->publicationAuthors = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add teams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $teams
     * @return Researcher
     */
    public function addTeam(\Bibliometry\MainBundle\Entity\Team $teams)
    {
        $this->teams[] = $teams;

        return $this;
    }

    /**
     * Remove teams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $teams
     */
    public function removeTeam(\Bibliometry\MainBundle\Entity\Team $teams)
    {
        $this->teams->removeElement($teams);
    }

    /**
     * Get teams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getTeams()
    {
        $toReturn = $this->teams->toArray();
        foreach ($this->teams as $team)
        {
            $motherTeams = $team->getAllMotherTeams();
            foreach ($motherTeams as $motherTeam)
            {
                if (! in_array($motherTeam, $toReturn))
                {
                    $toReturn[] = $motherTeam;
                }
            }
        }
        
        // Sort by hierarchical level
        uasort($toReturn, function ($t1, $t2)
        {
            if (in_array($t2, $t1->getAllMotherTeams()))
            {
                return 1;
            }
            else if (in_array($t1, $t2->getAllMotherTeams()))
            {
                return -1;
            }
            else
            {
                return 0;
            }
        });
        return $toReturn;
    }
    
    public function getLaboratory()
    {
        if (count($this->getTeams()) > 0)
        {
            return $this->teams[0]->getRootTeam();
        }
        return NULL;
    }

    /**
     * Add publications
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publications
     * @return Researcher
     */
    public function addPublication(\Bibliometry\MainBundle\Entity\Publication $publications)
    {
        $this->publications[] = $publications;
        return $this;
    }

    /**
     * Remove publications
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publications
     */
    public function removePublication(\Bibliometry\MainBundle\Entity\Publication $publications)
    {
        $this->publications->removeElement($publications);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublications($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $publications = array();
        foreach ($this->getPublicationAuthors() as $publicationAuthor)
        {
            if ($publicationAuthor->getPublication()->getMergedTo() == NULL)
            {
                $publications[] = $publicationAuthor->getPublication();
            }
        }
        
        if ($beginYear == NULL && $endYear == NULL)
        {
            return $publications;
        }
        
        if ($beginYear != NULL && $endYear == NULL)
        {
            $endYear = $beginYear;
        }
        
        if ($beginMonth != NULL && $endMonth == NULL)
        {
            $endMonth = $beginMonth;
        }
        
        if ($beginMonth == NULL && $endMonth == NULL)
        {
            $beginDate = new \DateTime($beginYear . "-01");
            $endDate = new \DateTime($endYear . "-12");
        }
        else
        {
            $beginDate = new \DateTime($beginYear . "-" . $beginMonth);
            $endDate = new \DateTime($endYear . "-" . $endMonth);
        }
        
        $toReturn = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($publications as $publication)
            if ($publication->satisfyConstraint($constraint) && $publication->getDate() >= $beginDate &&  $publication->getDate() <= $endDate)
                $toReturn->add($publication);
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($toReturn);
    }
    
    public function getPublicationsIndexedPerYear($publications, $beginYear, $endYear, $constraint = "all")
    {
        $toReturn = array();
        for ($year = $beginYear ; $year <= $endYear ; $year++)
            $toReturn[$year] = array();
        foreach ($publications as $publication)
            if ($publication->satisfyConstraint($constraint))
                $toReturn[$publication->getYear()][] = $publication;
        return $toReturn;
    }
    
    /**
     * Get conference publications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublicationsConferences($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        if ($beginYear != NULL && $endYear == NULL)
        {
            $endYear = $beginYear;
        }
        
        if ($beginMonth != NULL && $endMonth == NULL)
        {
            $endMonth = $beginMonth;
        }
        
        if ($beginMonth == NULL && $endMonth == NULL)
        {
            $beginDate = new \DateTime($beginYear . "-01");
            $endDate = new \DateTime($endYear . "-12");
        }
        else
        {
            $beginDate = new \DateTime($beginYear . "-" . $beginMonth);
            $endDate = new \DateTime($endYear . "-" . $endMonth);
        }
        $publiConferences = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->getPublications() as $publication)
            if ($publication->getPubliConference() != NULL &&
                $publication->satisfyConstraint($constraint) &&
                (($beginYear == NULL && $endYear == NULL) ||
                 ($publication->getDate() >= $beginDate &&  $publication->getDate() <= $endDate)))
                $publiConferences->add($publication);
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($publiConferences);
    }
    
    /**
     * Get journal publications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublicationsJournals($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        if ($beginYear != NULL && $endYear == NULL)
        {
            $endYear = $beginYear;
        }
        
        if ($beginMonth != NULL && $endMonth == NULL)
        {
            $endMonth = $beginMonth;
        }
        
        if ($beginMonth == NULL && $endMonth == NULL)
        {
            $beginDate = new \DateTime($beginYear . "-01");
            $endDate = new \DateTime($endYear . "-12");
        }
        else
        {
            $beginDate = new \DateTime($beginYear . "-" . $beginMonth);
            $endDate = new \DateTime($endYear . "-" . $endMonth);
        }
        $publiJournals = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->getPublications() as $publication)
            if ($publication->getPubliJournal() != NULL &&
                $publication->satisfyConstraint($constraint) &&
                (($beginYear == NULL && $endYear == NULL)  ||
                 ($publication->getDate() >= $beginDate &&  $publication->getDate() <= $endDate)))
                $publiJournals->add($publication);
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($publiJournals);
    }
    
    /**
     * Get other publications (not conference and not journal)
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublicationsOthers($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        if ($beginYear != NULL && $endYear == NULL)
        {
            $endYear = $beginYear;
        }
        
        if ($beginMonth != NULL && $endMonth == NULL)
        {
            $endMonth = $beginMonth;
        }
        
        if ($beginMonth == NULL && $endMonth == NULL)
        {
            $beginDate = new \DateTime($beginYear . "-01");
            $endDate = new \DateTime($endYear . "-12");
        }
        else
        {
            $beginDate = new \DateTime($beginYear . "-" . $beginMonth);
            $endDate = new \DateTime($endYear . "-" . $endMonth);
        }
        $publiOthers = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->getPublications() as $publication)
            if ($publication->getPubliOther() != NULL &&
                $publication->satisfyConstraint($constraint) &&
                (($beginYear == NULL && $endYear == NULL) ||
                 ($publication->getDate() >= $beginDate &&  $publication->getDate() <= $endDate)))
                $publiOthers->add($publication);
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($publiOthers);
    }
    
    public function getQuartileRepartition($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        $toReturn['NC'] = 0;
        $toReturn['Q1'] = 0;
        $toReturn['Q2'] = 0;
        $toReturn['Q3'] = 0;
        $toReturn['Q4'] = 0;
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the journal of the year in which the paper has been published
            $IF = $publication->getPubliJournal()->getImpactFactor();
            if ($IF == NULL)
            {
                $toReturn['NC']++;
            }
            else
            {
                $toReturn['Q'.$IF->getQuartile()]++;
            }
        }
        return $toReturn;
    }
    
    public function getJournalRepartitionQuartile($beginYear, $endYear, $quartile, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the journal of the year in which the paper has been published
            $publiJournal = $publication->getPubliJournal();
            $journal = $publiJournal->getJournal();
            $IF = $publiJournal->getImpactFactor();
            if ( ($IF == NULL && $quartile == "NC") || ($IF != NULL && $IF->getQuartile() == $quartile) )
            {
                if (array_key_exists($journal->getId(), $toReturn))
                {
                    $toReturn[$journal->getId()][1]++;
                }
                else
                {
                    $toReturn[$journal->getId()] = [$journal, 1];
                }
            }
        }
        
        // Sort this array by frequency (to have a nice sorted pie chart)
        uasort($toReturn, function ($j1, $j2)
        {
            if ($j1[1] > $j2[1])
                return -1;
            elseif ($j1[1] < $j2[1])
                return 1;
            else
                return 0;
        });
        
        return $toReturn;
    }
    
    public function getQuartileEvolution($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        for ($year = $beginYear ; $year <= $endYear ; $year++)
        {
            $repartitionYear = array();
            $repartitionYear['NC'] = 0;
            $repartitionYear['Q1'] = 0;
            $repartitionYear['Q2'] = 0;
            $repartitionYear['Q3'] = 0;
            $repartitionYear['Q4'] = 0;
        
            foreach ($publications as $publication)
            {
                if ($publication->getYear() == $year)
                {
                    // Get the ranking of the journal of the year in which the paper has been published
                    $IF = $publication->getPubliJournal()->getImpactFactor();
                    if ($IF == NULL)
                    {
                        $repartitionYear['NC']++;
                    }
                    else
                    {
                        $repartitionYear['Q'.$IF->getQuartile()]++;
                    }
                }
            }
            $toReturn[$year] = $repartitionYear;
        }
        return $toReturn;
    }
    
    public function getAverageIFEvolution($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $evolutionYear = array();
        
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        for ($year = $beginYear ; $year <= $endYear ; $year++)
        {
            $evolutionYear[$year] = 0.;
			$numberOfNotNullIF = 0;

            foreach ($publications as $publication)
            {
                if ($publication->getYear() == $year)
                {
                    // Get the ranking of the journal of the year in which the paper has been published
                    $IF = $publication->getPubliJournal()->getImpactFactor();
                    if ($IF != NULL)
                    {
                        $evolutionYear[$year] += $IF->getValue();
                        $numberOfNotNullIF++;
                    }
                }
            }
            if ($numberOfNotNullIF != 0)
            {
                $evolutionYear[$year] /= $numberOfNotNullIF;
            }
        }
        
        return $evolutionYear;
    }
    
    public function getConferenceRankingRepartition($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        $toReturn['NC'] = 0;
        $toReturn['FR'] = 0;
        $toReturn['C'] = 0;
        $toReturn['B'] = 0;
        $toReturn['A'] = 0;
        $toReturn['A*'] = 0;
        
        $publications = $this->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the conference of the year in which the paper has been published
            $ranking = $publication->getPubliConference()->getConferenceRanking();
            if ($ranking == NULL)
            {
                if (strtolower($publication->getLanguage()) == "fr")
                {
                    $toReturn['FR']++;
                }
                else
                {
                    $toReturn['NC']++;
                }
            }
            else
            {
                $toReturn[$ranking->getValue()]++;
            }
        }
        
        return $toReturn;
    }
    
    public function getConferenceRepartitionRanking($beginYear, $endYear, $ranking, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the conference of the year in which the paper has been published
            $conference = $publication->getPubliConference()->getConference();
            $conferenceRanking = $publication->getPubliConference()->getConferenceRanking();
            if ($conferenceRanking != NULL && $conferenceRanking->getValue() == $ranking)
            {
                if (array_key_exists($conference->getId(), $toReturn))
                {
                    $toReturn[$conference->getId()][1]++;
                }
                else
                {
                    $toReturn[$conference->getId()] = [$conference, 1];
                }
            }
        }
        
        // Sort this array by frequency (to have a nice sorted pie chart)
        uasort($toReturn, function ($c1, $c2)
        {
            if ($c1[1] > $c2[1])
                return -1;
            elseif ($c1[1] < $c2[1])
                return 1;
            else
                return 0;
        });
        
        return $toReturn;
    }
    
    public function getConferenceRankingEvolution($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        for ($year = $beginYear ; $year <= $endYear ; $year++)
        {
            $repartitionYear = array();
            $repartitionYear['A*'] = 0;
            $repartitionYear['A'] = 0;
            $repartitionYear['B'] = 0;
            $repartitionYear['C'] = 0;
            $repartitionYear['FR'] = 0;
            $repartitionYear['NC'] = 0;
        
            foreach ($publications as $publication)
            {
                if ($publication->getYear() == $year)
                {
                    // Get the ranking of the journal of the year in which the paper has been published
                    $ranking = $publication->getPubliConference()->getConferenceRanking();
                    if ($ranking == NULL)
                    {
                        if ($publication->getLanguage() == "fr")
                        {
                            $repartitionYear['FR']++;
                        }
                        else
                        {
                            $repartitionYear['NC']++;
                        }
                    }
                    else
                    {
                        $repartitionYear[$ranking->getValue()]++;
                    }
                }
            }
            $toReturn[$year] = $repartitionYear;
        }
        return $toReturn;
    }
    
    public function getCollaborations($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublications($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            foreach ($publication->getAuthors() as $author)
            {
                if ($author != $this)
                {
                    if(!isset($toReturn[$author->getId()]))
                    {
                        $toReturn[$author->getId()] = [$author, 1, array($publication->getId() => $publication)];
                    }
                    else {
                        $toReturn[$author->getId()][1]++;
                        if(!isset($toReturn[$author->getId()][2][$publication->getId()]))
                        {
                            $toReturn[$author->getId()][2][$publication->getId()] = $publication;
                        }
                    }
                }
            }
        }
        foreach ($toReturn as $authorId => $collaboration)
        {
            $toReturn[$authorId][2] = \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($collaboration[2]);
        }
        
        usort($toReturn, array('Bibliometry\MainBundle\Entity\Researcher','sortCollaborations'));
        return $toReturn;    
    }
    
    private static function sortCollaborations($collaboration1, $collaboration2)
    {
        if ($collaboration1[1] > $collaboration2[1])
        {
            return -1;
        }
        else if ($collaboration2[1] > $collaboration1[1])
        {
            return 1;
        }
        else {
            return 0;
        }
    }
    
    /**
     * Add leadTeams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $leadTeams
     * @return Researcher
     */
    public function addLeadTeam(\Bibliometry\MainBundle\Entity\Team $leadTeams)
    {
        $this->leadTeams[] = $leadTeams;

        return $this;
    }

    /**
     * Remove leadTeams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $leadTeams
     */
    public function removeLeadTeam(\Bibliometry\MainBundle\Entity\Team $leadTeams)
    {
        $this->leadTeams->removeElement($leadTeams);
    }

    /**
     * Get leadTeams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getLeadTeams()
    {
        return $this->leadTeams;
    }
    
    public function getAllLeadTeams()
    {
        $toReturn = array();

        foreach ($this->getLeadTeams() as $team)
        {
            $toReturn = array_merge($toReturn, [$team], $team->getAllDaughterTeamsArray());
        }
        
        return $toReturn;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Researcher
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    private function getAllTeamsAndAncestorsArray()
    {
        $toReturn = array();
        foreach ($this->getTeams() as $team)
        {
            $toReturn = array_merge($toReturn, $team->getAllMotherTeams());
        }
        return $toReturn;
    }
    
    public function getAllTeamsAndAncestors()
    {
        return new \Doctrine\Common\Collections\ArrayCollection($this->getAllTeamsAndAncestorsArray());
    }

    /**
     * Set user
     *
     * @param \Bibliometry\MainBundle\Entity\User $user
     * @return Researcher
     */
    public function setUser(\Bibliometry\MainBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Bibliometry\MainBundle\Entity\Researcher 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Get photo
     *
     * @return \Bibliometry\MainBundle\Entity\Photo 
     */
    public function getPhoto()
    {
        if ($this->getUser())
        {
            return $this->getUser()->getPhoto();
        }
        
        return NULL;
    }
    
    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        if ($this->getUser() === NULL)
        {
            return NULL;
        }// else
        return $this->getUser()->getEmail();
    }

    /**
     * Set googleProfile
     *
     * @param string $googleProfile
     * @return Researcher
     */
    public function setGoogleProfile($googleProfile)
    {
        $this->googleProfile = $googleProfile;

        return $this;
    }

    /**
     * Get googleProfile
     *
     * @return string 
     */
    public function getGoogleProfile()
    {
        return $this->googleProfile;
    }

    /**
     * Set hIndex
     *
     * @param string $hIndex
     * @return Researcher
     */
    public function setHIndex($hIndex)
    {
        $this->hIndex = $hIndex;

        return $this;
    }

    /**
     * Get hIndex
     *
     * @return string 
     */
    public function getHIndex()
    {
        return $this->hIndex;
    }

    /**
     * Set altNames
     *
     * @param array $altNames
     * @return Researcher
     */
    public function setAltNames($altNames)
    {
        $this->altNames = $altNames;

        return $this;
    }

    /**
     * Get altNames
     *
     * @return array 
     */
    public function getAltNames()
    {
        return $this->altNames;
    }

    /**
     * Set altSurnames
     *
     * @param array $altSurnames
     * @return Researcher
     */
    public function setAltSurnames($altSurnames)
    {
        $this->altSurnames = $altSurnames;

        return $this;
    }

    /**
     * Get altSurnames
     *
     * @return array 
     */
    public function getAltSurnames()
    {
        return $this->altSurnames;
    }
    
    public function mergeWith($researcher)
    {
        $altNames = $this->getAltNames();
        $altNames[] = $researcher->getName();
        $this->setAltNames($altNames);
        
        $altSurnames = $this->getAltSurnames();
        $altSurnames[] = $researcher->getSurname();
        $this->setAltSurnames($altSurnames);
    
        $publications = $researcher->getPublications();
        foreach ($publications as $publication)
        {
            $rank = $publication->removeAuthor($researcher);
            $publication->addAuthor($this, $rank);
        }
        
        $supervisations = $researcher->getSupervisations();
        foreach ($supervisations as $supervisation)
        {
            $supervisation->addSupervisor($this);
        }
        
        $leadTeams = $researcher->getLeadTeams();
        foreach ($leadTeams as $leadTeam)
        {
            $leadTeam->setLeader($this);
        }
        
        $teams = $researcher->getTeams();
        foreach ($teams as $team)
        {
            $team->removeResearcher($researcher);
            if (! in_array($this, $team->getResearchers()))
            {
                $team->addResearcher($this);
            }
        }
    }
    
    public function splitWith($researchers)
    {
        foreach ($researchers as $index => $researcher)
        {
            $publications = $this->getPublications();
            foreach ($publications as $publication)
            {
                $publication->addAuthor($researcher, $index + 1);
            }
            
            $supervisations = $this->getSupervisations();
            foreach ($supervisations as $supervisation)
            {
                $supervisation->addSupervisor($researcher);
            }
            
            $teams = $this->getTeams();
            foreach ($teams as $team)
            {
                if (! in_array($researcher, $team->getResearchers()))
                {
                    $team->addResearcher($researcher);
                }
            }
            
            $splitResearchers = $this->getSplitMultipleResearchers();
            foreach ($splitResearchers as $splitResearcher)
            {
                $splitResearcher->setMultipleResearcher(null);
            }
        }
    }

    /**
     * Add rightDuplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateResearchers $rightDuplicateResearchers
     * @return Researcher
     */
    public function addRightDuplicateResearcher(\Bibliometry\MainBundle\Entity\DuplicateResearchers $rightDuplicateResearchers)
    {
        $this->rightDuplicateResearchers[] = $rightDuplicateResearchers;

        return $this;
    }

    /**
     * Remove rightDuplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateResearchers $rightDuplicateResearchers
     */
    public function removeRightDuplicateResearcher(\Bibliometry\MainBundle\Entity\DuplicateResearchers $rightDuplicateResearchers)
    {
        $this->rightDuplicateResearchers->removeElement($rightDuplicateResearchers);
    }

    /**
     * Get rightDuplicateResearchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getRightDuplicateResearchers()
    {
        return $this->rightDuplicateResearchers;
    }

    /**
     * Add duplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateResearchers $duplicateResearchers
     * @return Researcher
     */
    public function addDuplicateResearcher(\Bibliometry\MainBundle\Entity\DuplicateResearchers $duplicateResearchers)
    {
        $this->duplicateResearchers[] = $duplicateResearchers;

        return $this;
    }

    /**
     * Remove duplicateResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\DuplicateResearchers $duplicateResearchers
     */
    public function removeDuplicateResearcher(\Bibliometry\MainBundle\Entity\DuplicateResearchers $duplicateResearchers)
    {
        $this->duplicateResearchers->removeElement($duplicateResearchers);
    }

    /**
     * Get duplicateResearchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDuplicateResearchers()
    {
        return $this->duplicateResearchers;
    }

    /**
     * Add supervisations
     *
     * @param \Bibliometry\MainBundle\Entity\PubliOther $supervisations
     * @return Researcher
     */
    public function addSupervisation(\Bibliometry\MainBundle\Entity\PubliOther $supervisations)
    {
        $this->supervisations[] = $supervisations;

        return $this;
    }

    /**
     * Remove supervisations
     *
     * @param \Bibliometry\MainBundle\Entity\PubliOther $supervisations
     */
    public function removeSupervisation(\Bibliometry\MainBundle\Entity\PubliOther $supervisations)
    {
        $this->supervisations->removeElement($supervisations);
    }

    /**
     * Get supervisations
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSupervisations()
    {
        return $this->supervisations;
    }

    /**
     * Add splitMultipleResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\SplitResearchers $splitMultipleResearchers
     * @return Researcher
     */
    public function addSplitMultipleResearcher(\Bibliometry\MainBundle\Entity\SplitResearchers $splitMultipleResearchers)
    {
        $this->splitMultipleResearchers[] = $splitMultipleResearchers;

        return $this;
    }

    /**
     * Remove splitMultipleResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\SplitResearchers $splitMultipleResearchers
     */
    public function removeSplitMultipleResearcher(\Bibliometry\MainBundle\Entity\SplitResearchers $splitMultipleResearchers)
    {
        $this->splitMultipleResearchers->removeElement($splitMultipleResearchers);
    }

    /**
     * Get splitMultipleResearchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSplitMultipleResearchers()
    {
        return $this->splitMultipleResearchers;
    }

    /**
     * Add splitResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\SplitResearchers $splitResearchers
     * @return Researcher
     */
    public function addSplitResearcher(\Bibliometry\MainBundle\Entity\SplitResearchers $splitResearchers)
    {
        $this->splitResearchers[] = $splitResearchers;

        return $this;
    }

    /**
     * Remove splitResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\SplitResearchers $splitResearchers
     */
    public function removeSplitResearcher(\Bibliometry\MainBundle\Entity\SplitResearchers $splitResearchers)
    {
        $this->splitResearchers->removeElement($splitResearchers);
    }

    /**
     * Get splitResearchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSplitResearchers()
    {
        return $this->splitResearchers;
    }

    /**
     * Add publicationAuthors
     *
     * @param \Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors
     * @return Researcher
     */
    public function addPublicationAuthor(\Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors)
    {
        $this->publicationAuthors[] = $publicationAuthors;

        return $this;
    }

    /**
     * Remove publicationAuthors
     *
     * @param \Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors
     */
    public function removePublicationAuthor(\Bibliometry\MainBundle\Entity\PublicationAuthor $publicationAuthors)
    {
        $this->publicationAuthors->removeElement($publicationAuthors);
    }

    /**
     * Get publicationAuthors
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublicationAuthors()
    {
        return $this->publicationAuthors;
    }

    /**
     * Set dblpProfile
     *
     * @param string $dblpProfile
     * @return Researcher
     */
    public function setDblpProfile($dblpProfile)
    {
        $this->dblpProfile = $dblpProfile;

        return $this;
    }

    /**
     * Get dblpProfile
     *
     * @return string
     */
    public function getDblpProfile()
    {
        return $this->dblpProfile;
    }
}
