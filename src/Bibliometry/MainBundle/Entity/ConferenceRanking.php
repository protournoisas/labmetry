<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ConferenceRanking
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\ConferenceRankingRepository")
 */
class ConferenceRanking
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="year", type="integer")
     */
    private $year;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=255)
     */
    private $value;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Conference", cascade={"persist"}, inversedBy="conferenceRankings")
    */
    private $conference;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set year
     *
     * @param integer $year
     * @return ConferenceRanking
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer 
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return ConferenceRanking
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return string 
     */
    public function getValue()
    {
        return $this->value;
    }
    
    public function getValueInteger()
    {
        $value = $this->getValue();
        switch ($value)
        {
            case "A*":
                return 1;
            case "A":
                return 2;
            case "B":
                return 3;
            case "C":
                return 4;
            default:
                return 5;
        }
    }
    
    public static function getValueIntegerOfHydratedRanking($ranking)
    {
        $value = $ranking["value"];
        switch ($value)
        {
            case "A*":
                return 1;
            case "A":
                return 2;
            case "B":
                return 3;
            case "C":
                return 4;
            default:
                return 5;
        }
    }

    /**
     * Set conference
     *
     * @param \Bibliometry\MainBundle\Entity\Conference $conference
     * @return ConferenceRanking
     */
    public function setConference(\Bibliometry\MainBundle\Entity\Conference $conference = null)
    {
        $this->conference = $conference;

        return $this;
    }

    /**
     * Get conference
     *
     * @return \Bibliometry\MainBundle\Entity\Conference 
     */
    public function getConference()
    {
        return $this->conference;
    }
}
