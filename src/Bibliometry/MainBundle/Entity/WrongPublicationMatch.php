<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Bibliometry\MainBundle\Validator\Constraints as ErrorsAssert;

/**
 * WrongPublicationMatch
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ErrorsAssert\ConferenceOrJournal
 */
class WrongPublicationMatch
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\ErrorReport", mappedBy="wrongPublicationMatch", cascade={"remove"})
     */
    private $errorReport;

    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="wrongPublicationsMatches")
     */
    private $publication;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Conference")
     */
    private $rightConference;
    
    /**
     * @var boolean $conferenceNotReferenced
     * @ORM\Column(name="conferenceNotReferenced", type="boolean", nullable=false)
     */
    private $conferenceNotReferenced;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Journal")
     */
    private $rightJournal;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set errorReport
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $errorReport
     * @return ErrorReport
     */
    public function setErrorReport(\Bibliometry\MainBundle\Entity\ErrorReport $errorReport = null)
    {
        $this->errorReport = $errorReport;

        return $this;
    }

    /**
     * Get errorReport
     *
     * @return \Bibliometry\MainBundle\Entity\ErrorReport 
     */
    public function getErrorReport()
    {
        return $this->errorReport;
    }

    /**
     * Set publication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publication
     * @return WrongPublicationMatch
     */
    public function setPublication(\Bibliometry\MainBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;
        $publication->addWrongPublicationsMatch($this);
        return $this;
    }

    /**
     * Get publication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set rightConference
     *
     * @param \Bibliometry\MainBundle\Entity\Conference $rightConference
     * @return WrongPublicationMatch
     */
    public function setRightConference(\Bibliometry\MainBundle\Entity\Conference $rightConference = null)
    {
        $this->rightConference = $rightConference;

        return $this;
    }

    /**
     * Get rightConference
     *
     * @return \Bibliometry\MainBundle\Entity\Conference 
     */
    public function getRightConference()
    {
        return $this->rightConference;
    }

    /**
     * Set rightJournal
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $rightJournal
     * @return WrongPublicationMatch
     */
    public function setRightJournal(\Bibliometry\MainBundle\Entity\Journal $rightJournal = null)
    {
        $this->rightJournal = $rightJournal;

        return $this;
    }

    /**
     * Get rightJournal
     *
     * @return \Bibliometry\MainBundle\Entity\Journal 
     */
    public function getRightJournal()
    {
        return $this->rightJournal;
    }
    
    public function getRightMatch()
    {
        if ($this->getRightConference())
        {
            return $this->getRightConference();
        }
        else if($this->getRightJournal())
        {
            return $this->getRightJournal();
        }
        else
        {
            return NULL;
        }
    }

    /**
     * Set conferenceNotReferenced
     *
     * @param boolean $conferenceNotReferenced
     *
     * @return WrongPublicationMatch
     */
    public function setConferenceNotReferenced($conferenceNotReferenced)
    {
        $this->conferenceNotReferenced = $conferenceNotReferenced;

        return $this;
    }

    /**
     * Get conferenceNotReferenced
     *
     * @return boolean
     */
    public function getConferenceNotReferenced()
    {
        return $this->conferenceNotReferenced;
    }
}
