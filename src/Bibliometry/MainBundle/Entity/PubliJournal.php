<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PubliJournal
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\PubliJournalRepository")
 */
class PubliJournal
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="volume", type="integer", nullable=true)
     */
    private $volume;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="number", type="integer", nullable=true)
     */
    private $number;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="publiJournal")
    */
    private $publication;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Journal", cascade={"persist"}, inversedBy="publiJournals")
    */
    private $journal;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\ImpactFactor", cascade={"persist"})
    */
    private $impactFactor;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set volume
     *
     * @param integer $volume
     * @return PubliJournal
     */
    public function setVolume($volume)
    {
        $this->volume = $volume;

        return $this;
    }

    /**
     * Get volume
     *
     * @return integer 
     */
    public function getVolume()
    {
        return $this->volume;
    }

    /**
     * Set publication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publication
     * @return PubliJournal
     */
    public function setPublication(\Bibliometry\MainBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set journal
     *
     * @param \Bibliometry\MainBundle\Entity\Journal $journal
     * @return PubliJournal
     */
    public function setJournal(\Bibliometry\MainBundle\Entity\Journal $journal = null)
    {
        $this->journal = $journal;
        if ($journal && $this->publication)
        { // cache publication grade
            $journal->addPubliJournal($this);
            $IF = $journal->getIFYear($this->publication->getYear());
            $this->setImpactFactor($IF);
        }
        else
        {
            $this->setImpactFactor(null);
        }
        return $this;
    }

    /**
     * Get journal
     *
     * @return \Bibliometry\MainBundle\Entity\Journal 
     */
    public function getJournal()
    {
        return $this->journal;
    }

    /**
     * Set impactFactor
     *
     * @param \Bibliometry\MainBundle\Entity\ImpactFactor $impactFactor
     * @return PubliJournal
     */
    public function setImpactFactor(\Bibliometry\MainBundle\Entity\ImpactFactor $impactFactor = null)
    {
        $this->impactFactor = $impactFactor;
        return $this;
    }

    /**
     * Get impactFactor
     *
     * @return \Bibliometry\MainBundle\Entity\ImpactFactor 
     */
    public function getImpactFactor()
    {
        return $this->impactFactor;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return PubliJournal
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }
}
