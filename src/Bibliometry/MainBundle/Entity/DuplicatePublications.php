<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Bibliometry\MainBundle\Validator\Constraints as ErrorsAssert;

/**
 * DuplicatePublications
 *
 * @ORM\Table()
 * @ORM\Entity
 * @ErrorsAssert\DifferentRightWrongPublication
 */
class DuplicatePublications
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\ErrorReport", mappedBy="duplicatePublications", cascade={"remove"})
     */
    private $errorReport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="rightPublications")
     */
    private $rightPublication;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="wrongPublications")
     */
    private $wrongPublication;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set errorReport
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $errorReport
     * @return ErrorReport
     */
    public function setErrorReport(\Bibliometry\MainBundle\Entity\ErrorReport $errorReport = null)
    {
        $this->errorReport = $errorReport;

        return $this;
    }

    /**
     * Get errorReport
     *
     * @return \Bibliometry\MainBundle\Entity\ErrorReport 
     */
    public function getErrorReport()
    {
        return $this->errorReport;
    }

    /**
     * Set rightPublication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $rightPublication
     * @return DuplicatePublications
     */
    public function setRightPublication(\Bibliometry\MainBundle\Entity\Publication $rightPublication = null)
    {
        $this->rightPublication = $rightPublication;
        $rightPublication->addRightPublication($this);
        return $this;
    }

    /**
     * Get rightPublication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getRightPublication()
    {
        return $this->rightPublication;
    }

    /**
     * Set wrongPublication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $wrongPublication
     * @return DuplicatePublications
     */
    public function setWrongPublication(\Bibliometry\MainBundle\Entity\Publication $wrongPublication = null)
    {
        $this->wrongPublication = $wrongPublication;
        $wrongPublication->addWrongPublication($this);
        return $this;
    }

    /**
     * Get wrongPublication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getWrongPublication()
    {
        return $this->wrongPublication;
    }
    
    public function getSelectedWrongPublication($selectedRightPublication)
    {
        if ($this->getRightPublication() == $selectedRightPublication)
        {
            return $this->getWrongPublication();
        }
        else
        {
            return $this->getRightPublication();
        }
    }
}
