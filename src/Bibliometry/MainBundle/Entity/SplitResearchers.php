<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Bibliometry\MainBundle\Validator\Constraints as ErrorsAssert;

/**
 * SplitResearchers
 *
 * @ORM\Table()
 * @ORM\Entity
 */
class SplitResearchers
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\ErrorReport", mappedBy="splitResearchers", cascade={"remove"})
     */
    private $errorReport;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Researcher", inversedBy="splitMultipleResearchers")
     */
    private $multipleResearcher;
    
    /**
     * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Researcher", inversedBy="splitResearchers")
     */
    private $splitResearchers;
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->splitResearchers = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set errorReport
     *
     * @param \Bibliometry\MainBundle\Entity\ErrorReport $errorReport
     * @return SplitResearchers
     */
    public function setErrorReport(\Bibliometry\MainBundle\Entity\ErrorReport $errorReport = null)
    {
        $this->errorReport = $errorReport;

        return $this;
    }

    /**
     * Get errorReport
     *
     * @return \Bibliometry\MainBundle\Entity\ErrorReport 
     */
    public function getErrorReport()
    {
        return $this->errorReport;
    }

    /**
     * Set multipleResearcher
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $multipleResearcher
     * @return SplitResearchers
     */
    public function setMultipleResearcher(\Bibliometry\MainBundle\Entity\Researcher $multipleResearcher = null)
    {
        $this->multipleResearcher = $multipleResearcher;
        if ($multipleResearcher != null)
        {
            $multipleResearcher->addSplitMultipleResearcher($this);
        }
        return $this;
    }

    /**
     * Get multipleResearcher
     *
     * @return \Bibliometry\MainBundle\Entity\Researcher 
     */
    public function getMultipleResearcher()
    {
        return $this->multipleResearcher;
    }

    /**
     * Add splitResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $splitResearchers
     * @return SplitResearchers
     */
    public function addSplitResearcher(\Bibliometry\MainBundle\Entity\Researcher $splitResearchers)
    {
        $this->splitResearchers[] = $splitResearchers;
        $splitResearchers->addSplitResearcher($this);
        return $this;
    }

    /**
     * Remove splitResearchers
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $splitResearchers
     */
    public function removeSplitResearcher(\Bibliometry\MainBundle\Entity\Researcher $splitResearchers)
    {
        $this->splitResearchers->removeElement($splitResearchers);
    }

    /**
     * Get splitResearchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getSplitResearchers()
    {
        return $this->splitResearchers;
    }
}
