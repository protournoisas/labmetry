<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * TeamRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class TeamRepository extends EntityRepository
{

    public function searchTeams($keywords)
    {
        $queryBuilder = $this->createQueryBuilder('t');
        foreach($keywords as $index => $keyword)
        {
            $queryBuilder->andWhere('t.name LIKE :keyword' . $index);
            $queryBuilder->setParameter('keyword' . $index, '%' . $keyword . '%');
        }
        return $queryBuilder->getQuery()->getResult();
    }

    public function getDautherTeamsId($teamId)
    {
        $results = $this->getEntityManager()->createQuery('
                SELECT t.id FROM BibliometryMainBundle:Team t
                WHERE t.motherTeam = ' . $teamId)->getResult();
        $array_result = array();
        foreach($results as $result)
        {
            $array_result[] = $result["id"];
            $array_result = array_merge($array_result, $this->getDautherTeamsId($result["id"]));
        }
        return $array_result;
    }

    public function getDautherTeamsIdIncludingOurself($teamId)
    {
        $results = $this->getDautherTeamsId($teamId);
        array_unshift($results, $teamId);
        return $results;
    }
    
    public function getLeafTeamsId($teamId)
    {
        $results = $this->getEntityManager()->createQuery('
                SELECT t.id FROM BibliometryMainBundle:Team t
                WHERE t.motherTeam = ' . $teamId)->getResult();
        if(count($results) == 0)
            return array($teamId);
        $array_result = array();
        foreach($results as $result)
            $array_result = array_merge($array_result, $this->getLeafTeamsId($result["id"]));
        return $array_result;
    }
}
