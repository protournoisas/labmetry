<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PubliConference
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\PubliConferenceRepository")
 */
class PubliConference
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=255, nullable=true)
     */
    private $country;
    
    /**
     * @var string
     *
     * @ORM\Column(name="titleOfConferenceInHAL", type="string", length=255)
     */
    private $titleOfConferenceInHAL;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Publication", inversedBy="publiConference")
    */
    private $publication;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Conference", cascade={"persist"}, inversedBy="publiConferences")
    */
    private $conference;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\ConferenceRanking", cascade = {"persist"})
    */
    private $conferenceRanking;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set publication
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publication
     * @return PubliConference
     */
    public function setPublication(\Bibliometry\MainBundle\Entity\Publication $publication = null)
    {
        $this->publication = $publication;

        return $this;
    }

    /**
     * Get publication
     *
     * @return \Bibliometry\MainBundle\Entity\Publication 
     */
    public function getPublication()
    {
        return $this->publication;
    }

    /**
     * Set conference
     *
     * @param \Bibliometry\MainBundle\Entity\Conference $conference
     * @return PubliConference
     */
    public function setConference(\Bibliometry\MainBundle\Entity\Conference $conference = null)
    {
        $this->conference = $conference;
        if($conference && $this->publication)
        { // cache publication grade
            $conference->addPubliConference($this);
            $ranking = $conference->getConferenceRankingYear($this->publication->getYear());
            $this->setConferenceRanking($ranking);
        }
        else
        {
            $this->setConferenceRanking(null);
        }
        return $this;
    }

    /**
     * Get conference
     *
     * @return \Bibliometry\MainBundle\Entity\Conference 
     */
    public function getConference()
    {
        return $this->conference;
    }

    /**
     * Set conferenceRanking
     *
     * @param \Bibliometry\MainBundle\Entity\ConferenceRanking $conferenceRanking
     * @return PubliConference
     */
    public function setConferenceRanking(\Bibliometry\MainBundle\Entity\ConferenceRanking $conferenceRanking = null)
    {
        $this->conferenceRanking = $conferenceRanking;
        return $this;
    }

    /**
     * Get conferenceRanking
     *
     * @return \Bibliometry\MainBundle\Entity\ConferenceRanking 
     */
    public function getConferenceRanking()
    {
        return $this->conferenceRanking;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return PubliConference
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return PubliConference
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set titleOfConferenceInHAL
     *
     * @param string $titleOfConferenceInHAL
     *
     * @return PubliConference
     */
    public function setTitleOfConferenceInHAL($titleOfConferenceInHAL)
    {
        $this->titleOfConferenceInHAL = $titleOfConferenceInHAL;

        return $this;
    }

    /**
     * Get titleOfConferenceInHAL
     *
     * @return string
     */
    public function getTitleOfConferenceInHAL()
    {
        return $this->titleOfConferenceInHAL;
    }
    
    public function getYear()
    {
        return $this->getPublication()->getYear();
    }
}
