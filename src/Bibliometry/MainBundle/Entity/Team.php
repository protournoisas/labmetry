<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Team
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\TeamRepository")
 */
class Team
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Researcher", cascade={"persist"}, inversedBy="leadTeams")
    */
    private $leader;
    
    /**
     * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\HierarchicalLevel", cascade={"persist"}, inversedBy="teams")
     */
    private $hierarchicalLevel;
    
    /**
    * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Researcher", cascade={"persist"}, inversedBy="teams")
    */
    private $researchers;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\Team", inversedBy="daughterTeams")
    */
    private $motherTeam;
    
    /**
     * @ORM\OneToMany(targetEntity="Bibliometry\MainBundle\Entity\Team", cascade={"persist", "remove"}, mappedBy="motherTeam")
     */
    private $daughterTeams;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Laboratory", inversedBy="laboratoryTeam")
    */
    private $laboratory;
    
    /**
    * @ORM\ManyToOne(targetEntity="Bibliometry\MainBundle\Entity\ResearchField", cascade={"persist"}, inversedBy="teams")
    */
    private $researchField;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Team
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->researchers = new \Doctrine\Common\Collections\ArrayCollection();
        $this->daughterTeams = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add researchers
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $researchers
     * @return Team
     */
    public function addResearcher(\Bibliometry\MainBundle\Entity\Researcher $researchers)
    {
        $this->researchers[] = $researchers;
        $researchers->addTeam($this);
        return $this;
    }

    /**
     * Remove researchers
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $researchers
     */
    public function removeResearcher(\Bibliometry\MainBundle\Entity\Researcher $researchers)
    {
        $this->researchers->removeElement($researchers);
        $researchers->removeTeam($this);
    }

    /**
     * Get researchers
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getResearchersCollection()
    {
        return $this->researchers;
    }
     
    public function getResearchers()
    {
        $toReturn = $this->researchers->toArray();
        foreach ($this->getAllDaughterTeamsArray() as $team)
        {
            foreach ($team->getResearchers() as $researcher)
            {
                if (! in_array($researcher, $toReturn))
                {
                    $toReturn[] = $researcher;
                }
            }
        }
        return $toReturn;
    }
    
    public function getResearchersNotNested()
    {
        return $this->researchers;
    }

    /**
     * Set leader
     *
     * @param \Bibliometry\MainBundle\Entity\Researcher $leader
     * @return Team
     */
    public function setLeader(\Bibliometry\MainBundle\Entity\Researcher $leader = null)
    {
        $this->leader = $leader;
        $leader->addLeadTeam($this);
        return $this;
    }

    /**
     * Get leader
     *
     * @return \Bibliometry\MainBundle\Entity\Researcher 
     */
    public function getLeader()
    {
        return $this->leader;
    }
    
    public function isManaged($user)
    {
        // Test if admin
        if (in_array("ROLE_ADMIN", $user->getRoles()))
        {
            return true;
        }
        
        // Test if leader of the team
        if ($user->getResearcher() != null && $user->getResearcher() == $this->getLeader())
        {
            return true;
        }
        
        // Else
        return false;
    }

    /**
     * Set hierarchicalLevel
     *
     * @param \Bibliometry\MainBundle\Entity\HierarchicalLevel $hierarchicalLevel
     * @return Team
     */
    public function setHierarchicalLevel(\Bibliometry\MainBundle\Entity\HierarchicalLevel $hierarchicalLevel = null)
    {
        $this->hierarchicalLevel = $hierarchicalLevel;
        $hierarchicalLevel->addTeam($this);
        return $this;
    }

    /**
     * Get hierarchicalLevel
     *
     * @return \Bibliometry\MainBundle\Entity\HierarchicalLevel 
     */
    public function getHierarchicalLevel()
    {
        return $this->hierarchicalLevel;
    }

    

    /**
     * Set motherTeam
     *
     * @param \Bibliometry\MainBundle\Entity\Team $motherTeam
     * @return Team
     */
    public function setMotherTeam(\Bibliometry\MainBundle\Entity\Team $motherTeam = null)
    {
        $this->motherTeam = $motherTeam;

        return $this;
    }

    /**
     * Get motherTeam
     *
     * @return \Bibliometry\MainBundle\Entity\Team 
     */
    public function getMotherTeam()
    {
        return $this->motherTeam;
    }

    /**
     * Add daughterTeams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $daughterTeams
     * @return Team
     */
    public function addDaughterTeam(\Bibliometry\MainBundle\Entity\Team $daughterTeams)
    {
        $this->daughterTeams[] = $daughterTeams;
        $daughterTeams->setMotherTeam($this);
        return $this;
    }

    /**
     * Remove daughterTeams
     *
     * @param \Bibliometry\MainBundle\Entity\Team $daughterTeams
     */
    public function removeDaughterTeam(\Bibliometry\MainBundle\Entity\Team $daughterTeams)
    {
        $this->daughterTeams->removeElement($daughterTeams);
    }

    /**
     * Get daughterTeams
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getDaughterTeams()
    {
        $toReturn = $this->daughterTeams->toArray();
        
        // Sort this array by number of members
        uasort($toReturn, function ($t1, $t2)
        {
            if (count($t1->getResearchers()) > count($t2->getResearchers()))
                return -1;
            elseif (count($t1->getResearchers()) < count($t2->getResearchers()))
                return 1;
            else
                return 0;
        });
        
        return $toReturn;
    }

    

    /**
     * Set laboratory
     *
     * @param \Bibliometry\MainBundle\Entity\Laboratory $laboratory
     * @return Team
     */
    public function setLaboratory(\Bibliometry\MainBundle\Entity\Laboratory $laboratory = null)
    {
        $this->laboratory = $laboratory;

        return $this;
    }

    /**
     * Get laboratory
     *
     * @return \Bibliometry\MainBundle\Entity\Laboratory 
     */
    public function getLaboratory()
    {
        return $this->laboratory;
    }

    /**
     * Set researchField
     *
     * @param \Bibliometry\MainBundle\Entity\ResearchField $researchField
     * @return Team
     */
    public function setResearchField(\Bibliometry\MainBundle\Entity\ResearchField $researchField = null)
    {
        $this->researchField = $researchField;
        $researchField->addTeam($this);
        return $this;
    }

    /**
     * Get researchField
     *
     * @return \Bibliometry\MainBundle\Entity\ResearchField 
     */
    public function getResearchField()
    {
        return $this->researchField;
    }
    
    /**
     * Get root team
     *
     * @return Team
     */
    public function getRootTeam()
    {
        if ($this->getMotherTeam() == NULL)
        {
            return $this;
        }
        else
        {
            return $this->getMotherTeam()->getRootTeam();
        }
    }
    
    /**
     * Get all daughters
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAllDaughterTeams()
    {
        $daughterTeams = $this->getDaughterTeams();

        $toReturn = array();
        foreach ($daughterTeams as $daughter)
        {
            $daughterArray = $daughter->getAllDaughterTeams();
            $toReturn[] = $daughterArray;
        }
        return array("root" => $this, "children" => $toReturn);
    }
    
    /**
     * Get all daughters in an array
     *
     * @return array
     */
    public function getAllDaughterTeamsArray()
    {
        $daughterTeams = $this->getDaughterTeams();

        $toReturn = array();
        foreach ($daughterTeams as $daughter)
        {
            $daughterArray = $daughter->getAllDaughterTeamsArray();
            $toReturn = array_merge($toReturn, [$daughter], $daughterArray);
        }
        return $toReturn;
    }
    
    /**
     * Get all ancestors of a team
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getAllMotherTeams()
    {   
        if ($this->getMotherTeam() == NULL)
        {
            return array($this);
        }   
        return array_merge([$this], $this->getMotherTeam()->getAllMotherTeams());
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Team
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    public function getPublications($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {        
        $toReturn = new \Doctrine\Common\Collections\ArrayCollection();
        
        foreach ($this->getResearchers() as $researcher)
        {
            $researcherPublis = $researcher->getPublications($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
            foreach ($researcherPublis as $publi)
                if (! $toReturn->contains($publi))
                    $toReturn->add($publi);
        }
        
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($toReturn);
    }
    
    public function getPublicationsIndexedPerYear($publications, $beginYear, $endYear, $constraint = "all")
    {
        $toReturn = array();
        for ($year = $beginYear ; $year <= $endYear ; $year++)
            $toReturn[$year] = array();
        foreach ($publications as $publication)
            if ($publication->satisfyConstraint($constraint))
                $toReturn[$publication->getYear()][] = $publication; 
        return $toReturn;
    }
    
    public function getPublicationsConferences($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->getResearchers() as $researcher)
        {
            $researcherPublisConferences = $researcher->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
            foreach ($researcherPublisConferences as $publiConference)
                if (! $toReturn->contains($publiConference))
                    $toReturn->add($publiConference);
        }
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($toReturn);
    }
    
    public function getPublicationsJournals($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->getResearchers() as $researcher)
        {
            $researcherPublisJournals = $researcher->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
            foreach ($researcherPublisJournals as $publiJournal)
                if (! $toReturn->contains($publiJournal))
                    $toReturn->add($publiJournal);
        }
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($toReturn);
    }
    
    public function getPublicationsOthers($beginYear = NULL, $endYear = NULL, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = new \Doctrine\Common\Collections\ArrayCollection();
        foreach ($this->getResearchers() as $researcher)
        {
            $researcherPublisOthers = $researcher->getPublicationsOthers($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
            foreach ($researcherPublisOthers as $publiOther)
                if (! $toReturn->contains($publiOther))
                    $toReturn->add($publiOther);
        }
        return \Bibliometry\MainBundle\Entity\Publication::sortPublicationsByTime($toReturn);
    }
    
    public function getQuartileRepartition($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        $toReturn['NC'] = 0;
        $toReturn['Q1'] = 0;
        $toReturn['Q2'] = 0;
        $toReturn['Q3'] = 0;
        $toReturn['Q4'] = 0;
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the journal of the year in which the paper has been published
            $IF = $publication->getPubliJournal()->getImpactFactor();
            if ($IF == NULL)
            {
                $toReturn['NC']++;
            }
            else
            {
                $toReturn['Q'.$IF->getQuartile()]++;
            }
        }
        return $toReturn;
    }
    
    public function getJournalRepartitionQuartile($beginYear, $endYear, $quartile, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the journal of the year in which the paper has been published
            $publiJournal = $publication->getPubliJournal();
            $journal = $publiJournal->getJournal();
            $IF = $publiJournal->getImpactFactor();
            if ( ($IF == NULL && $quartile == "NC") || ($IF != NULL && $IF->getQuartile() == $quartile) )
            {
                if (array_key_exists($journal->getId(), $toReturn))
                {
                    $toReturn[$journal->getId()][1]++;
                }
                else
                {
                    $toReturn[$journal->getId()] = [$journal, 1];
                }
            }
        }
        
        // Sort this array by frequency (to have a nice sorted pie chart)
        uasort($toReturn, function ($j1, $j2)
        {
            if ($j1[1] > $j2[1])
                return -1;
            elseif ($j1[1] < $j2[1])
                return 1;
            else
                return 0;
        });
        
        return $toReturn;
    }
    
    public function getQuartileEvolution($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        for ($year = $beginYear ; $year <= $endYear ; $year++)
        {
            $repartitionYear = array();
            $repartitionYear['NC'] = 0;
            $repartitionYear['Q1'] = 0;
            $repartitionYear['Q2'] = 0;
            $repartitionYear['Q3'] = 0;
            $repartitionYear['Q4'] = 0;
        
            foreach ($publications as $publication)
            {
                if ($publication->getYear() == $year)
                {
                    // Get the ranking of the journal of the year in which the paper has been published
                    $IF = $publication->getPubliJournal()->getImpactFactor();
                    if ($IF == NULL)
                    {
                        $repartitionYear['NC']++;
                    }
                    else
                    {
                        $repartitionYear['Q'.$IF->getQuartile()]++;
                    }
                }
            }
            $toReturn[$year] = $repartitionYear;
        }
        return $toReturn;
    }
    
    public function getAverageIFEvolution($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $evolutionYear = array();
        
        $publications = $this->getPublicationsJournals($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        for ($year = $beginYear ; $year <= $endYear ; $year++)
        {
            $evolutionYear[$year] = 0.;
			$numberOfNotNullIF = 0;

            foreach ($publications as $publication)
            {
                if ($publication->getYear() == $year)
                {
                    // Get the ranking of the journal of the year in which the paper has been published
                    $IF = $publication->getPubliJournal()->getImpactFactor();
                    if ($IF != NULL)
                    {
                        $evolutionYear[$year] += $IF->getValue();
                        $numberOfNotNullIF++;
                    }
                }
            }
            if ($numberOfNotNullIF != 0)
            {
                $evolutionYear[$year] /= $numberOfNotNullIF;
            }
        }
        
        return $evolutionYear;
    }
    
    public function getConferenceRankingRepartition($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        $toReturn['NC'] = 0;
        $toReturn['FR'] = 0;
        $toReturn['C'] = 0;
        $toReturn['B'] = 0;
        $toReturn['A'] = 0;
        $toReturn['A*'] = 0;
        
        $publications = $this->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the conference of the year in which the paper has been published
            $ranking = $publication->getPubliConference()->getConferenceRanking();
            if ($ranking == NULL)
            {
                if (strtolower($publication->getLanguage()) == "fr")
                {
                    $toReturn['FR']++;
                }
                else
                {
                    $toReturn['NC']++;
                }
            }
            else
            {
                $toReturn[$ranking->getValue()]++;
            }
        }
        
        return $toReturn;
    }
    
    public function getConferenceRepartitionRanking($beginYear, $endYear, $ranking, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        foreach ($publications as $publication)
        {
            // Get the ranking of the conference of the year in which the paper has been published
            $conference = $publication->getPubliConference()->getConference();
            $conferenceRanking = $publication->getPubliConference()->getConferenceRanking();
            if ($conferenceRanking != NULL && $conferenceRanking->getValue() == $ranking)
            {
                if (array_key_exists($conference->getId(), $toReturn))
                {
                    $toReturn[$conference->getId()][1]++;
                }
                else
                {
                    $toReturn[$conference->getId()] = [$conference, 1];
                }
            }
        }
        
        // Sort this array by frequency (to have a nice sorted pie chart)
        uasort($toReturn, function ($c1, $c2)
        {
            if ($c1[1] > $c2[1])
                return -1;
            elseif ($c1[1] < $c2[1])
                return 1;
            else
                return 0;
        });
        
        return $toReturn;
    }
    
    public function getConferenceRankingEvolution($beginYear, $endYear, $beginMonth = NULL, $endMonth = NULL, $constraint = "all")
    {
        $toReturn = array();
        
        $publications = $this->getPublicationsConferences($beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        for ($year = $beginYear ; $year <= $endYear ; $year++)
        {
            $repartitionYear = array();
            $repartitionYear['A*'] = 0;
            $repartitionYear['A'] = 0;
            $repartitionYear['B'] = 0;
            $repartitionYear['C'] = 0;
            $repartitionYear['FR'] = 0;
            $repartitionYear['NC'] = 0;
        
            foreach ($publications as $publication)
            {
                if ($publication->getYear() == $year)
                {
                    // Get the ranking of the journal of the year in which the paper has been published
                    $ranking = $publication->getPubliConference()->getConferenceRanking();
                    if ($ranking == NULL)
                    {
                        if ($publication->getLanguage() == "fr")
                        {
                            $repartitionYear['FR']++;
                        }
                        else
                        {
                            $repartitionYear['NC']++;
                        }
                    }
                    else
                    {
                        $repartitionYear[$ranking->getValue()]++;
                    }
                }
            }
            $toReturn[$year] = $repartitionYear;
        }
        return $toReturn;
    }
}
