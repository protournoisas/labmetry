<?php

namespace Bibliometry\MainBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Laboratory
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="Bibliometry\MainBundle\Entity\LaboratoryRepository")
 */
class Laboratory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;
    
    /**
     * @Gedmo\Slug(fields={"name"})
     * @ORM\Column(length=128, unique=true)
     */
    private $slug;

    /**
     * @var string
     *
     * @ORM\Column(name="HALID", type="string", length=255)
     */
    private $HALID;
    
    /**
    * @ORM\ManyToMany(targetEntity="Bibliometry\MainBundle\Entity\Publication", mappedBy="laboratories")
    */
    private $publications;
    
    /**
    * @ORM\OneToOne(targetEntity="Bibliometry\MainBundle\Entity\Team", cascade={"persist"}, mappedBy="laboratory")
    */
    private $laboratoryTeam;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Laboratory
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set HALID
     *
     * @param string $HALID
     * @return Laboratory
     */
    public function setHALID($HALID)
    {
        $this->HALID = $HALID;

        return $this;
    }

    /**
     * Get HALID
     *
     * @return string 
     */
    public function getHALID()
    {
        return $this->HALID;
    }
    
    /**
     * Constructor
     */
    public function __construct($name)
    {
        $this->publications = new \Doctrine\Common\Collections\ArrayCollection();
        $this->name = $name;
        $this->setLaboratoryTeam(new \Bibliometry\MainBundle\Entity\Team());
        $this->getLaboratoryTeam()->setName($name);
    }

    /**
     * Add publications
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publications
     * @return Laboratory
     */
    public function addPublication(\Bibliometry\MainBundle\Entity\Publication $publications)
    {
        $this->publications[] = $publications;

        return $this;
    }

    /**
     * Remove publications
     *
     * @param \Bibliometry\MainBundle\Entity\Publication $publications
     */
    public function removePublication(\Bibliometry\MainBundle\Entity\Publication $publications)
    {
        $this->publications->removeElement($publications);
    }

    /**
     * Get publications
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getPublications()
    {
        return $this->publications;
    }


    /**
     * Set laboratoryTeam
     *
     * @param \Bibliometry\MainBundle\Entity\Team $laboratoryTeam
     * @return Laboratory
     */
    public function setLaboratoryTeam(\Bibliometry\MainBundle\Entity\Team $laboratoryTeam = null)
    {
        $this->laboratoryTeam = $laboratoryTeam;
        $laboratoryTeam->setLaboratory($this);
        return $this;
    }

    /**
     * Get laboratoryTeam
     *
     * @return \Bibliometry\MainBundle\Entity\Team 
     */
    public function getLaboratoryTeam()
    {
        return $this->laboratoryTeam;
    }

    /**
     * Set slug
     *
     * @param string $slug
     * @return Laboratory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Get slug
     *
     * @return string 
     */
    public function getSlug()
    {
        return $this->slug;
    }
}
