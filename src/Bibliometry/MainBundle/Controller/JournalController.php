<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Doctrine\Common\Collections\ArrayCollection;
use Bibliometry\MainBundle\Form\JournalType;
use Symfony\Component\HttpFoundation\Request;

class JournalController extends Controller
{

    /**
     * @Route("/journal", name="all_journals_route")
     * @Template()
     */
    public function showJournalsAction()
    {
        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
        $journals = $journalsRepository->findByMergedTo(null);
        
        return array(
                'journals' => $journals
        );
    }

    /**
     * @Route("/journal/{slug_journal}", name="journal_route")
     * @Template()
     */
    public function showJournalAction($slug_journal)
    {
        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
        $journal = $journalsRepository->findOneBySlug($slug_journal);
        
        // If not found, 404
        if($journal == NULL)
        {
            throw $this->createNotFoundException('This journal was not found on the website.');
        }
        
        // If this journal has been merged (duplicate), redirect to the right one
        if($journal->getMergedTo() != NULL)
        {
            return $this->redirect($this->generateUrl('journal_route', array(
                    'slug_journal' => $journal->getMergedTo()->getSlug()
            )));
        }
        
        return array(
                'journal' => $journal,
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
        );
    }

    /**
     * @Route("/journal/{slug_journal}/export-statistics/{graph}.{_format}",
     * name = "export_stats_journal_route",
     * requirements={"_format" = "csv", "graph" = "if_evolution|quartile_evolution"})
     */
    public function exportStatsJournalAction($slug_journal, $graph, $_format)
    {
        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
        $journal = $journalsRepository->findOneBySlug($slug_journal);
        
        // If not found, 404
        if($journal == NULL)
        {
            throw $this->createNotFoundException('This journal was not found on the website.');
        }
        
        // If this journal has been merged (duplicate), redirect to the right one
        if($journal->getMergedTo() != NULL)
        {
            return $this->redirect($this->generateUrl('export_stats_journal_route', array(
                    'slug_journal' => $journal->getMergedTo()->getSlug(),
                    'graph' => $graph,
                    '_format' => $format
            )));
        }
        
        $exported_data = $this->renderView('BibliometryMainBundle:Journal:' . $graph . '.' . $_format . '.twig', array(
                "journal" => $journal
        ));
        
        return new Response($exported_data);
    }

    /**
     * @Route("/edit-journal/{slug_journal}", name="edit_journal_route")
     * @Template()
     */
    public function editJournalAction(Request $request, $slug_journal)
    {
        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
        $journal = $journalsRepository->findOneBySlug($slug_journal);
        
        // If not found, 404
        if($journal == NULL)
        {
            throw $this->createNotFoundException('This journal was not found on the website.');
        }
        
        // If this journal has been merged (duplicate), redirect to the right one
        if($journal->getMergedTo() != NULL)
        {
            return $this->redirect($this->generateUrl('journal_route', array(
                    'slug_journal' => $journal->getMergedTo()->getSlug()
            )));
        }
        
        if(false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            throw new AccessDeniedException();
        }
        
        // Save original IFs to compare and delete
        $originalIFs = new ArrayCollection();
        foreach($journal->getIFs() as $IF)
        {
            $originalIFs->add($IF);
        }
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\JournalType', $journal);
        
        $form->handleRequest($request);
        
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            
            // Delete IFs
            foreach($originalIFs as $IF)
            {
                if(in_array($IF, $journal->getIFs()) === false)
                {
                    $journal->removeIF($IF);
                    foreach($journal->getPubliJournals() as $publiJournal)
                    { // remove impact factor of publiJournals before deleting impact factor due to foreign key constraint
                        $publiJournal->setJournal($journal);
                    }
                    $em->remove($IF);
                }
            }

            foreach($journal->getPubliJournals() as $publiJournal)
            { // recompute impact factors of the publications and cache it
                $publiJournal->setJournal($journal);
            }
            $em->persist($journal);
            $em->flush();
            
            $session = $request->getSession();
            $session->getFlashBag()->add('success', 'bibliometry.journal.edit_success');
            
            return $this->redirect($this->generateUrl('journal_route', array(
                    'slug_journal' => $journal->getSlug()
            )));
        }
        
        return array(
                'journal' => $journal,
                'form' => $form->createView()
        );
    }
    
    /**
     * @Route("/searchJournalWok/{request}", name="search_journal_wok_route", requirements={"request" = ".+"})
     * @Template()
     */
    public function searchJournalWokAction($request)
    {
        $WOKService = $this->get('bibliometry_main.WOK');
        $results = $WOKService->searchJournal($request);
        return array(
            'request' => $request,
            'results' => $results
        );
    }

    /**
     * @Route("/importAndRedirectToJournalPage/{journalTitle}/{issn}/{abbrJournal}/{edition}", name="import_journal_wok_route", requirements={"journalTitle" = ".+", "issn" = ".+", "abbrJournal" = ".+", "edition" = ".+"})
     * @Template()
     */
    public function importAndRedirectToJournalPageAction($journalTitle, $issn, $abbrJournal, $edition)
    {
        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
        $existingJournals = $journalsRepository->findAll();
        $journal = $this->get('bibliometry_main.HALMatchDB')->findOrCreateJournal($existingJournals, $journalTitle, $issn);
        $WOKService = $this->get('bibliometry_main.WOK');
        $WOKService->importOneJournal($journal, $abbrJournal, $edition);
        return $this->redirect($this->generateUrl('journal_route', array('slug_journal' => $journal->getSlug())));
    }
}
