<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Bibliometry\MainBundle\Entity\Page;
use Bibliometry\MainBundle\Form\PageType;
use Bibliometry\MainBundle\Entity\Category;
use Bibliometry\MainBundle\Form\CategoryType;

class HelpController extends Controller
{
    /**
     * @Route("/help/page/{slug_page}", name="help_show_page")
     * @Template()
     */
    public function showPageAction($slug_page)
    {
        $pagesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Page');
        $page = $pagesRepository->findOneBySlug($slug_page);
        
        // If not page, 404
        if ($page == NULL)
        {
            throw $this->createNotFoundException('This help page was not found on the website.');
        }
        
        return array("page" => $page);
    }
    
    /**
     * @Route("/admin/help/add-page", name="help_add_page")
     * @Template()
     */
    public function addPageAction(Request $request)
    {
        $page = new Page();
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\PageType', $page);
        
        // Handle the form
        $form->handleRequest($request);
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            
            $this->container->get("session")->getFlashBag()->add("success", "bibliometry.help.add_page.success");

            return $this->redirect($this->generateUrl('help_show_page', array('slug_page' => $page->getSlug())));   
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/admin/help/edit-page/{slug_page}", name="help_edit_page")
     * @Template()
     */
    public function editPageAction(Request $request, $slug_page)
    {
        $pagesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Page');
        $page = $pagesRepository->findOneBySlug($slug_page);
        
        // If not page, 404
        if ($page == NULL)
        {
            throw $this->createNotFoundException('This help page was not found on the website.');
        }
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\PageType', $page);
        
        // Handle the form
        $form->handleRequest($request);
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($page);
            $em->flush();
            
            $this->container->get("session")->getFlashBag()->add("success", "bibliometry.help.edit_page.success");

            return $this->redirect($this->generateUrl('help_show_page', array('slug_page' => $page->getSlug())));   
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/help/category/{slug_category}", name="help_show_category")
     * @Template()
     */
    public function showCategoryAction($slug_category)
    {
        $categoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Category');
        $category = $categoriesRepository->findOneBySlug($slug_category);
        
        // If not page, 404
        if ($category == NULL)
        {
            throw $this->createNotFoundException('This help category was not found on the website.');
        }
        
        return array("category" => $category);
    }
    
    /**
     * @Route("/admin/help/add-category", name="help_add_category")
     * @Template()
     */
    public function addCategoryAction(Request $request)
    {
        $category = new Category();
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\CategoryType', $category);
        
        // Handle the form
        $form->handleRequest($request);
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($category);
            $em->flush();
            
            $this->container->get("session")->getFlashBag()->add("success", "bibliometry.help.add_category.success");

            return $this->redirect($this->generateUrl('help_show_category', array('slug_category' => $category->getSlug())));   
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("/help", name="help_show_help")
     * @Template()
     */
    public function showHelpAction()
    {
        $categoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Category');
        $categories = $categoriesRepository->findAll();

        return array("categories" => $categories);
    }

}
