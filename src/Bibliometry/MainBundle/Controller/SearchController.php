<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class SearchController extends Controller
{

    /**
     * @Route("/search/{request}", name="search_route", requirements={"request" = ".+"})
     * @Template()
     */
    public function searchAction($request)
    {
        // Retrieve the laboratory configured for the application
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $laboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        $keywords = explode(" ", $request);
        
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $matchingResearchers = $researchersRepository->searchResearchers($keywords);
        
        $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
        $matchingTeams = $teamsRepository->searchTeams($keywords);
        
        $publicationsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $matchingPublications = $publicationsRepository->searchPublications($keywords);
        
        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
        $matchingJournals = $journalsRepository->searchJournals($keywords);
        
        $conferencesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Conference');
        $matchingConferences = $conferencesRepository->searchConferences($keywords);
        
        return array(
                "request" => $request,
                "matchingResearchers" => $matchingResearchers,
                "matchingTeams" => $matchingTeams,
                "matchingPublications" => $matchingPublications,
                "matchingJournals" => $matchingJournals,
                "matchingConferences" => $matchingConferences,
                "laboratory" => $laboratory,
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
        );
    }
}
