<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Process\Process;

class AdminController extends Controller
{
    private $HALProgressPath = "/../../../../web/imports/HAL_progress.txt";
    private $CoreProgressPath = "/../../../../web/imports/Core_progress.txt";
    private $WOKProgressPath = "/../../../../web/imports/WOK_progress.txt";
    private $ScholarProgressPath = "/../../../../web/imports/Scholar_progress.txt";
    private $DblpProgressPath = "/../../../../web/imports/Dblp_progress.txt";
    private $cacheGradeProgressPath = "/../../../../web/imports/cacheGrade_progress.txt";
    
    /**
     * @Route("admin", name="admin_route")
     * @Template()
     */
    public function showAdminAction()
    {
        $usersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:User');
        $administrators = $usersRepository->getAdministrators();
        $users = $usersRepository->getNonAdministrators();
        
        $process = new Process('git log -1 --format=%cd --date=local');
        $process->run();
        $currentVersion =  $process->getOutput(); // date of the last commit on the website        
        return array("administrators" => $administrators, "users" => $users, "currentVersion" => $currentVersion);
    }
    
    /**
     * @Route("admin/revoke/{username}", name="admin_revoke_route")
     * @Template()
     */
    public function revokeAdminAction($username)
    {
        $process = new Process($this->container->getParameter('php_path').'php ../app/console fos:user:demote '.$username.' ROLE_ADMIN');
        $process->run();
        if (!$process->isSuccessful())
        {
            $this->container->get("session")->getFlashBag()->add("error", "bibliometry.admin.revoke_error");
        }
        else
        {
            $this->container->get("session")->getFlashBag()->add("success", "bibliometry.admin.revoke_success");
        }
        
        return $this->redirect($this->generateUrl('admin_route'));   
    }
    
    /**
     * @Route("admin/promote/{username}", name="admin_promote_route")
     * @Template()
     */
    public function promoteAdminAction($username)
    {
        $process = new Process($this->container->getParameter('php_path').'php ../app/console fos:user:promote '.$username.' ROLE_ADMIN');
        $process->run();
        if (!$process->isSuccessful())
        {
            $this->container->get("session")->getFlashBag()->add("error", "bibliometry.admin.promote_error");
        }
        else
        {
            $this->container->get("session")->getFlashBag()->add("success", "bibliometry.admin.promote_success");
        }
        
        return $this->redirect($this->generateUrl('admin_route'));   
    }

    /**
     * @Route("/importHAL", name="admin_import_hal")
     */
    public function importHALAction()
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $beginYear = $request->get("beginYear");
            $endYear = $request->get("endYear");
            $process = new Process($this->container->getParameter('php_path').'php ../app/console bibliometry:hal-import --useWOK '.$beginYear.' '.$endYear.' > '.__DIR__.$this->HALProgressPath);
            $process->start();
            
            return new Response('Pending');
        }
        
        return new Response('Fail');
    }
    
    /**
     * @Route("/checkImportHAL", name="admin_check_import_hal")
     */
    public function checkImportHALAction()
    {
        if (file_exists(__DIR__.$this->HALProgressPath))
        {
            $content = file_get_contents(__DIR__.$this->HALProgressPath);
            $lines = explode("\n", $content);
            $progress = $lines[count($lines) - 2];
            if ($progress == 100 || strpos($progress, "ERROR : ") !== false)
            {
                unlink(__DIR__.$this->HALProgressPath);
            }
            return new Response($progress);
        } // else
        return new Response('Not launched');
    }
    
    /**
     * @Route("/importCore", name="admin_import_core")
     */
    public function importCoreAction()
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $process = new Process($this->container->getParameter('php_path').'php ../app/console bibliometry:core-import > '.__DIR__.$this->CoreProgressPath);
            $process->start();
            
            return new Response('Pending');
        }
        
        return new Response('Fail');
    }
    
    /**
     * @Route("/checkImportCore", name="admin_check_import_core")
     */
    public function checkImportCoreAction()
    {
        if (file_exists(__DIR__.$this->CoreProgressPath))
        {
            $content = file_get_contents(__DIR__.$this->CoreProgressPath);
            $lines = explode("\n", $content);
            $progress = $lines[count($lines) - 2];
            if ($progress == 100 || strpos($progress, "ERROR : ") !== false)
            {
                unlink(__DIR__.$this->CoreProgressPath);
            }
            return new Response($progress);
        } // else
        return new Response('Not launched');
    }
    
    /**
     * @Route("/seeAvailableYearsWok", name="admin_see_available_years_wok")
     */
    public function seeAvailableYearsWOKAction()
    {
        $WOKService = $this->get('bibliometry_main.WOK');
        $yearsAvailable = $WOKService->getAvailableYears();
        
        return new Response(implode(",", $yearsAvailable));
    }
    
    /**
     * @Route("/importWOK", name="admin_import_wok")
     */
    public function importWOKAction()
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
            $beginYear = $request->get("beginYear");
            $endYear = $request->get("endYear");

            $process = new Process($this->container->getParameter('php_path').'php ../app/console bibliometry:wok-import '.$beginYear.' '.$endYear.' > '.__DIR__.$this->WOKProgressPath);
            $process->start();
            
            return new Response('Pending');
        }
        
        return new Response('Fail');
    }
    
    /**
     * @Route("/checkImportWOK", name="admin_check_import_wok")
     */
    public function checkImportWOKAction()
    {
        if (file_exists(__DIR__.$this->WOKProgressPath))
        {
            $content = file_get_contents(__DIR__.$this->WOKProgressPath);
            $lines = explode("\n", $content);
            $progress = $lines[count($lines) - 2];
            if ($progress == 100 || strpos($progress, "ERROR : ") !== false)
            {
                unlink(__DIR__.$this->WOKProgressPath);
            }
            return new Response($progress);
        } // else
        return new Response('Not launched');
    }
    
    /**
     * @Route("/importScholar", name="admin_import_scholar")
     */
    public function importScholarAction()
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {

            $process = new Process($this->container->getParameter('php_path').'php ../app/console bibliometry:scholar-import > '.__DIR__.$this->ScholarProgressPath);
            $process->start();
            
            return new Response('Pending');
        }
        
        return new Response('Fail');
    }
    
    /**
     * @Route("/checkImportScholar", name="admin_check_import_scholar")
     */
    public function checkImportScholarAction()
    {
        if (file_exists(__DIR__.$this->ScholarProgressPath))
        {
            $content = file_get_contents(__DIR__.$this->ScholarProgressPath);
            $lines = explode("\n", $content);
            $progress = $lines[count($lines) - 2];
            if ($progress == 100 || strpos($progress, "ERROR : ") !== false)
            {
                unlink(__DIR__.$this->ScholarProgressPath);
            }
            return new Response($progress);
        } // else
        return new Response('Not launched');
    }

    /**
     * @Route("/importDblp", name="admin_import_dblp")
     */
    public function importDblpAction()
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
    
            $process = new Process($this->container->getParameter('php_path').'php ../app/console bibliometry:dblp-import > '.__DIR__.$this->DblpProgressPath);
            $process->start();
    
            return new Response('Pending');
        }
    
        return new Response('Fail');
    }
    
    /**
     * @Route("/checkImportDblp", name="admin_check_import_dblp")
     */
    public function checkImportDblpAction()
    {
        if (file_exists(__DIR__.$this->DblpProgressPath))
        {
            $content = file_get_contents(__DIR__.$this->DblpProgressPath);
            $lines = explode("\n", $content);
            $progress = $lines[count($lines) - 2];
            if ($progress == 100 || strpos($progress, "ERROR : ") !== false)
            {
                unlink(__DIR__.$this->DblpProgressPath);
            }
            return new Response($progress);
        } // else
        return new Response('Not launched');
    }

    /**
     * @Route("/cacheGrade", name="admin_cache_grade")
     */
    public function cacheGradeAction()
    {
        $request = $this->get('request');
        if ($request->getMethod() == 'POST') {
    
            $process = new Process($this->container->getParameter('php_path').'php ../app/console bibliometry:cache-publications-grade > '.__DIR__.$this->cacheGradeProgressPath);
            $process->start();
    
            return new Response('Pending');
        }
    
        return new Response('Fail');
    }
    
    /**
     * @Route("/checkCacheGrade", name="admin_check_cache_grade")
     */
    public function checkCacheGradeAction()
    {
        if (file_exists(__DIR__.$this->cacheGradeProgressPath))
        {
            $content = file_get_contents(__DIR__.$this->cacheGradeProgressPath);
            $lines = explode("\n", $content);
            $progress = $lines[count($lines) - 2];
            if ($progress == 100 || strpos($progress, "ERROR : ") !== false)
            {
                unlink(__DIR__.$this->cacheGradeProgressPath);
            }
            return new Response($progress);
        } // else
        return new Response('Not launched');
    }
}
