<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Bibliometry\MainBundle\Form\PublicationType;
use Symfony\Component\HttpFoundation\Request;

class PublicationController extends Controller
{

    /**
     * @Route("/publication/{slug_publication}.{_format}",
     * defaults = { "_format" = "html" },
     * requirements = { "_format" = "html|bib|txt" },
     * name="publication_route")
     * @Template()
     */
    public function showPublicationAction($slug_publication)
    {
        $publicationsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $publication = $publicationsRepository->findOneBySlug($slug_publication);
        
        // If not found, 404
        if($publication == NULL)
        {
            throw $this->createNotFoundException('This publication was not found on the website.');
        }
        
        // If this publication has been merged (duplicate), redirect to the right one
        if($publication->getMergedTo() != NULL)
        {
            return $this->redirect($this->generateUrl('publication_route', array(
                    'slug_publication' => $publication->getMergedTo()->getSlug()
            )));
        }
        
        return array(
                'publication' => $publication,
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
        );
    }

    /**
     * @Route("/edit-publication/{slug_publication}",
     * name="edit_publication_route")
     * @Template()
     */
    public function editPublicationAction(Request $request, $slug_publication)
    {
        $publicationsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $publication = $publicationsRepository->findOneBySlug($slug_publication);
        
        // If not found, 404
        if($publication == NULL)
        {
            throw $this->createNotFoundException('This publication was not found on the website.');
        }
        
        // If this publication has been merged (duplicate), redirect to the right one
        if($publication->getMergedTo() != NULL)
        {
            return $this->redirect($this->generateUrl('publication_route', array(
                    'slug_publication' => $publication->getMergedTo()->getSlug()
            )));
        }
        
        if(false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            throw new AccessDeniedException();
        }
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\PublicationType', $publication);
        
        $form->handleRequest($request);
        
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($publication);
            $em->flush();
            
            $session = $request->getSession();
            $session->getFlashBag()->add('success', 'bibliometry.publication.edit_success');
            
            return $this->redirect($this->generateUrl('publication_route', array(
                    'slug_publication' => $publication->getSlug()
            )));
        }
        
        return array(
                'publication' => $publication,
                'form' => $form->createView()
        );
    }
}
