<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;

use Bibliometry\MainBundle\Form\ErrorReportHandlingType;
use Bibliometry\MainBundle\Entity\Notification;

class HandleErrorsController extends Controller
{
    /**
     * @Route("/reported-errors", name = "show_errors_route")
     * @Template()
     */
    public function showErrorsAction()
    {
        $error_reports = $this->get('bibliometry_main.concerned_error_reports')->getConcernedErrorReports($this->getUser());
            
        return array('error_reports' => $error_reports);
    }
    
    /**
     * @Route("/error", name = "show_error_popup_route")
     * @Template()
     */
    public function showErrorPopupAction(Request $request)
    {
        $errorReportsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:ErrorReport');
        
        if ($request->getMethod() == "POST")
        {
            $errorId = $request->request->get("error_id");
            $errorReport = $errorReportsRepository->findOneById($errorId);
            
            $form = $this->createForm('Bibliometry\MainBundle\Form\ErrorReportHandlingType', $errorReport);
            
            return array("error" => $errorReport, "form" => $form->createView());
        }
    }

    /**
     * @Route("/handle-error", name = "handle_error_route")
     * @Template()
     */
    public function handleErrorAction(Request $request)
    {
        $errorReportsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:ErrorReport');
        
        if ($request->getMethod() == "POST")
        {
            // Retrieve the error to handle
            $errorId = $request->request->get("error_id");
            $errorReport = $errorReportsRepository->findOneById($errorId);
            
            // Handle the final comments field
            $form = $this->createForm('Bibliometry\MainBundle\Form\ErrorReportHandlingType', $errorReport);
            $form->handleRequest($request);
            if ($form->isValid())
            {
                $em = $this->getDoctrine()->getManager();
                // Handle the correction
                $rightSelection = $request->request->get("right");
                
                // Do nothing choice
                if ($rightSelection == "NULL")
                {
                    $errorReport->setValidated(false);
                }
                // Do the correction
                else
                {
                    // Duplicate publications
                    if ($errorReport->getErrorType() == "duplicatePublications")
                    {
                        $publicationsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
                        $selectedPublication = $publicationsRepository->findOneById($rightSelection);
                        
                        $wrongPublication = $errorReport->getDuplicatePublications()->getSelectedWrongPublication($selectedPublication);
                        
                        $em->remove($wrongPublication->getLinkedPubli());
                        $wrongPublication->mergeWith($selectedPublication);
                        $em->persist($wrongPublication);
                        
                        if ($rightSelection == $errorReport->getDuplicatePublications()->getRightPublication()->getId())
                        {
                            $sameDecision = true;
                        }
                        else
                        {
                            $sameDecision = false;
                        }
                    }
                    // Duplicate journals
                    else if ($errorReport->getErrorType() == "duplicateJournals")
                    {
                        $journalsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Journal');
                        $selectedJournal = $journalsRepository->findOneById($rightSelection);
                        
                        $wrongJournal = $errorReport->getDuplicateJournals()->getSelectedWrongJournal($selectedJournal);
                        
                        foreach ($wrongJournal->getIFs() as $IF)
                        {
                            $em->remove($IF);
                        }
                        $wrongJournal->mergeWith($selectedJournal);
                        $em->persist($wrongJournal);
                        
                        if ($rightSelection == $errorReport->getDuplicateJournals()->getRightJournal()->getId())
                        {
                            $sameDecision = true;
                        }
                        else
                        {
                            $sameDecision = false;
                        }
                    }
                    // Duplicate researchers
                    else if ($errorReport->getErrorType() == "duplicateResearchers")
                    {
                        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
                        $selectedResearcher = $researchersRepository->findOneById($rightSelection);
                        
                        $duplicateResearchers = $errorReport->getDuplicateResearchers()->getSelectedDuplicateResearchers($selectedResearcher);
                        
                        foreach ($duplicateResearchers as $researcher)
                        {   
                            $selectedResearcher->mergeWith($researcher);
                            $em->remove($researcher);
                        }
                        $em->persist($selectedResearcher);
                        
                        if ($rightSelection == $errorReport->getDuplicateResearchers()->getRightResearcher()->getId())
                        {
                            $sameDecision = true;
                        }
                        else
                        {
                            $sameDecision = false;
                        }
                    }
                    // Wrong publication match
                    else if ($errorReport->getErrorType() == "wrongPublicationMatch")
                    {
                        $publication = $errorReport->getWrongPublicationMatch()->getPublication();
                        if ($errorReport->getWrongPublicationMatch()->getRightConference())
                        {
                            if ($publication->getType() == "journal")
                            {
                                $em->remove($publication->getPubliJournal());
                            }
                            $publication->changeJournalOrConference($errorReport->getWrongPublicationMatch()->getRightConference(), "conference");
                        }
                        else if ($errorReport->getWrongPublicationMatch()->getRightJournal())
                        {
                            if ($publication->getType() == "conference")
                            {
                                $em->remove($publication->getPubliConference());
                            }
                            $publication->changeJournalOrConference($errorReport->getWrongPublicationMatch()->getRightJournal(), "journal");
                        }
                        else if($errorReport->getWrongPublicationMatch()->getConferenceNotReferenced())
                        {
                            $publication->changeJournalOrConference(NULL, "conference");
                        }
                        
                        $em->persist($publication);
                        
                        $sameDecision = true;
                    }
                    // Split researchers
                    else if ($errorReport->getErrorType() == "splitResearchers")
                    {
                        $multipleResearcher = $errorReport->getSplitResearchers()->getMultipleResearcher();
                        $splitResearchers = $errorReport->getSplitResearchers()->getSplitResearchers();
                        
                        $multipleResearcher->splitWith($splitResearchers);
                        
                        foreach ($splitResearchers as $splitResearcher)
                        {
                            $em->persist($splitResearcher);
                        }
                        $em->remove($multipleResearcher);
                        
                        $sameDecision = true;
                    }
                    // Not from lab publications
                    else if ($errorReport->getErrorType() == "notLabPublication")
                    {
                        $publication = $errorReport->getNotLabPublication()->getPublication();
                        
                        // We have to explictly remove these entities and unlink with the researcher ; otherwise, still present.
                        foreach ($publication->getPublicationAuthors() as $publicationAuthor)
                        {
                            $publicationAuthor->setResearcher(null);
                            $em->remove($publicationAuthor);
                        }
                        $publication->removeFromLab();
                        
                        $em->persist($publication);
                        
                        $sameDecision = true;
                    }
                    
                    $errorReport->setValidated(true);
                }
                
                $errorReport->setHandled(true);
                
                // Create a notification for the reporter
                $notification = new Notification();
                $notification->setType("errorReport");
                $notification->setUser($errorReport->getReporter());
                
                $shortMessage = $this->get('translator')->trans("bibliometry.notifications_messages.errorReport_short");
                $notification->setShortMessage($shortMessage);
                
                $fullMessage = $this->get('translator')->trans("bibliometry.notifications_messages.errorReport_full", array("%subject%" => $errorReport->__toString(), "%name%" => $this->getUser()->getName(), "%surname%" => $this->getUser()->getSurname()));
                if ($errorReport->getValidated() == true)
                {
                    if ($sameDecision == true)
                    {
                        $fullMessage .= $this->get('translator')->trans("bibliometry.notifications_messages.errorReport_accepted");
                    }
                    else
                    {
                        $fullMessage .= $this->get('translator')->trans("bibliometry.notifications_messages.errorReport_other_decision");
                    }
                }
                else
                {
                    $fullMessage .= $this->get('translator')->trans("bibliometry.notifications_messages.errorReport_refused");
                }
                if ($errorReport->getFinalComments())
                {
                    $fullMessage .= $this->get('translator')->trans("bibliometry.notifications_messages.errorReport_comment", array("%comment%" => $errorReport->getFinalComments()));
                }
                $notification->setFullMessage($fullMessage);
                
                $em->persist($notification);
                $em->remove($errorReport);
                
                $em->flush();
                
                // Send a mail if the reporter asked for it
                if ($errorReport->getSendMail())
                {
                    $message = \Swift_Message::newInstance()
                    ->setSubject($shortMessage)
                    ->setFrom($this->container->getParameter('mailer_sender'))
                    ->setTo($errorReport->getReporter()->getEmail())
                    ->setBody($this->renderView('BibliometryMainBundle:HandleErrors:errorHandledMail.html.twig', array('shortMessage' => $shortMessage, 'fullMessage' => $fullMessage)), 'text/html')
                    ;
                    $this->get('mailer')->send($message);
                }
                
                $this->container->get("session")->getFlashBag()->add("success", "bibliometry.errors.handle_errors.handle_success");
                $url = $request->headers->get("referer");
                return new RedirectResponse($url);
            }
        }
    }
}