<?php


namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\SecurityContextInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Bibliometry\MainBundle\Form\EditUserType;
use Bibliometry\MainBundle\Entity\UserInterface;

class UserController extends Controller
{
    /**
     * @Route("/register", name="fos_user_registration_register")
     * @Template()
     */
    public function registerAction(Request $request)
    {
        $userManager = $this->get('bibliometry_main.entity.user_manager');
        $user = $userManager->createUser();
        $user->setEnabled(true);
        $form = $this->createForm('Bibliometry\MainBundle\Form\RegistrationFormType', $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('bibliometry.registration.user_created'));
            $url = $this->generateUrl('final_registration_route');
            $response = new RedirectResponse($url);
            $this->get('bibliometry_main.security.login_manager')->logInUser("main", $user, $response);
            $user->setLastLogin(new \DateTime());
            $userManager->updateUser($user);
            return $response;
        }
        return $this->render('BibliometryMainBundle:User:register.html.twig', array(
            'form' => $form->createView(),
        ));
    }

    /**
     * @Route("/login", name="fos_user_security_login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $session = $request->getSession();
        $authErrorKey = Security::AUTHENTICATION_ERROR;
        $lastUsernameKey = Security::LAST_USERNAME;
        // get the error if any (works with forward and redirect -- see below)
        if ($request->attributes->has($authErrorKey))
            $error = $request->attributes->get($authErrorKey);
        elseif (null !== $session && $session->has($authErrorKey)) {
            $error = $session->get($authErrorKey);
            $session->remove($authErrorKey);
        }
        else
            $error = null;
        if (!$error instanceof AuthenticationException)
            $error = null; // The value does not come from the security component.
        // last username entered by the user
        $lastUsername = (null === $session) ? '' : $session->get($lastUsernameKey);
        $csrfToken = $this->get('security.csrf.token_manager')->getToken('authenticate')->getValue();
        return $this->render('BibliometryMainBundle:User:login.html.twig', array(
            'last_username' => $lastUsername,
            'error' => $error,
            'csrf_token' => $csrfToken,
        ));
    }

    /**
     * @Route("/login_check", name="fos_user_security_check")
     * @Template()
     */
    public function checkAction()
    {
        throw new \RuntimeException('You must configure the check path to be handled by the firewall using form_login in your security firewall configuration.');
    }

    /**
     * @Route("/logout", name="fos_user_security_logout")
     * @Template()
     */
    public function logoutAction()
    {
        throw new \RuntimeException('You must activate the logout in your security firewall configuration.');
    }

    /**
     * Edit the user
     * @Route("/profil/", name="fos_user_profile_show")
     * @Route("/profil/edit", name="fos_user_profile_edit")
     * @Template()
     */
    public function editAction(Request $request)
    {
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        if (!is_object($user) || !$user instanceof UserInterface)
            throw new AccessDeniedException('This user does not have access to this section.');
        // EDIT FORM
        $form = $this->createForm('Bibliometry\MainBundle\Form\EditUserType', $user);
        // CHANGE PASSWORD FORM
        $formPassword = $this->createForm('Bibliometry\MainBundle\Form\ChangePasswordFormType', $user);
        if ('POST' === $request->getMethod()) {
            // EDIT FORM POSTED
            if ($request->get('user_profile'))
            {
                $form->bind($request);
                if ($form->isValid()) {
                    $userManager = $this->container->get('bibliometry_main.entity.user_manager');
                    $userManager->updateUser($user);
                    $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('bibliometry.edit_profile.updated'));
                    $url = $this->container->get('router')->generate('fos_user_profile_edit');
                    $response = new RedirectResponse($url);
                    return $response;
                }
            }
            // CHANGE PASSWORD FORM POSTED
            else if ($request->get('user_change_password'))
            {
                $formPassword->bind($request);
                if ($formPassword->isValid()) {
                    $userManager = $this->container->get('bibliometry_main.entity.user_manager');
                    $userManager->updateUser($user);
                    $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('bibliometry.edit_profile.password_updated'));
                    $url = $this->container->get('router')->generate('fos_user_profile_edit');
                    $response = new RedirectResponse($url);
                    return $response;
                }
            }
        }
        return $this->container->get('templating')->renderResponse(
            'BibliometryMainBundle:User:edit.html.twig',
            array('form' => $form->createView(), 'formPassword' => $formPassword->createView())
            );
    }

    /**
     * Request reset user password: show form
     * @Route("/resetting/request", name="fos_user_resetting_request")
     * @Template()
     */
    public function requestAction()
    {
        return $this->render('BibliometryMainBundle:User:request.html.twig');
    }

    protected function generateToken()
    {
        return rtrim(strtr(base64_encode(hash('sha256', uniqid(mt_rand(), true), true)), '+/', '-_'), '=');
    }

    /**
     * Request reset user password: submit form and send email
     * @Route("/resetting/send-email", name="fos_user_resetting_send_email")
     * @Template()
     */
    public function sendEmailAction(Request $request)
    {
        $username = $request->request->get('username');
        $user = $this->get('bibliometry_main.entity.user_manager')->findUserByUsernameOrEmail($username);
        if (null === $user)
            return $this->render('BibliometryMainBundle:User:request.html.twig', array('invalid_username' => $username));
        if ($user->isPasswordRequestNonExpired())
            return $this->render('BibliometryMainBundle:User:passwordAlreadyRequested.html.twig');
        if (null === $user->getConfirmationToken())
            $user->setConfirmationToken($this->generateToken());
        $url = $this->container->get('router')->generate('fos_user_resetting_reset', array('token' => $user->getConfirmationToken()), true);
        $message = \Swift_Message::newInstance()
        ->setSubject($this->get('translator')->trans('bibliometry.resetting.email.subject'))
        ->setFrom($this->container->getParameter('mailer_sender'))
        ->setTo($user->getEmail())
        ->setBody($this->renderView('BibliometryMainBundle:User:resetPasswordMail.html.twig', array('user' => $user, 'confirmationUrl'=> $url)), 'text/html');
        $this->get('mailer')->send($message);
        $user->setPasswordRequestedAt(new \DateTime());
        $this->get('bibliometry_main.entity.user_manager')->updateUser($user);
        return new RedirectResponse($this->generateUrl('fos_user_resetting_check_email', array('email' => $this->getObfuscatedEmail($user))));
    }

    /**
     * Tell the user to check his email provider
     * @Route("/resetting/check-email", name="fos_user_resetting_check_email")
     * @Template()
     */
    public function checkEmailAction(Request $request)
    {
        $email = $request->query->get('email');
        if (empty($email)) // the user does not come from the sendEmail action
            return new RedirectResponse($this->generateUrl('fos_user_resetting_request'));
        return $this->render('BibliometryMainBundle:User:checkEmail.html.twig', array(
            'email' => $email,
        ));
    }

    /**
     * Reset user password
     * @Route("/resetting/reset/{token}", name="fos_user_resetting_reset")
     * @Template()
     */
    public function resetAction(Request $request, $token)
    {
        $userManager = $this->get('bibliometry_main.entity.user_manager');
        $user = $userManager->findUserByConfirmationToken($token);
        if (null === $user)
            throw new NotFoundHttpException(sprintf('The user with "confirmation token" does not exist for value "%s"', $token));
        $form = $this->createForm('Bibliometry\MainBundle\Form\ResettingFormType', $user);
        $form->handleRequest($request);
        if ($form->isValid()) {
            $request->getSession()->getFlashBag()->add('success', $this->get('translator')->trans('bibliometry.resetting.success'));
            $url = $this->container->get('router')->generate('fos_user_profile_edit');
            $response = new RedirectResponse($url);
            $this->get('bibliometry_main.security.login_manager')->logInUser("main", $user, $response);
            $user->setLastLogin(new \DateTime());
            $userManager->updateUser($user);
            return $response;
        }
        return $this->render('BibliometryMainBundle:User:reset.html.twig', array(
            'token' => $token,
            'form' => $form->createView(),
        ));
    }

    /**
     * Get the truncated email displayed when requesting the resetting.
     * The default implementation only keeps the part following @ in the address.
     * @param Bibliometry\MainBundle\Entity\UserInterface $user
     * @return string
     */
    protected function getObfuscatedEmail(UserInterface $user)
    {
        $email = $user->getEmail();
        if (false !== $pos = strpos($email, '@'))
            $email = '...' . substr($email, $pos);
        return $email;
    }
}
