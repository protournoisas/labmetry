<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Bibliometry\MainBundle\Entity\Publication;
use Bibliometry\MainBundle\Entity\Team;
use Bibliometry\MainBundle\Form\TeamType;
use Bibliometry\MainBundle\Form\AddResearcherTeamType;

class TeamController extends Controller
{

    /**
     * @Route("/team", name = "all_teams_route")
     * @Template()
     */
    public function showTeamsAction(Request $request)
    {
        // Retrieve the laboratory configured for the application
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $laboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        // If User is admin or leader of the lab, show him the administration of the teams
        $user = $this->getUser();
        if($laboratory->getLaboratoryTeam()->isManaged($user))
        {
            $newTeam = new Team();
            $formTeam = $this->createForm('Bibliometry\MainBundle\Form\TeamType', $newTeam,
            							  array('HALID' => $this->container->getParameter('HALID_lab')));
            $admin = true;
            
            $formTeam->handleRequest($request);
            
            if($formTeam->isValid())
            {
                $newTeam->setMotherTeam($laboratory->getLaboratoryTeam());
                $newTeam->addResearcher($newTeam->getLeader());
                
                $em = $this->getDoctrine()->getManager();
                $em->persist($newTeam);
                $em->flush();
                
                $session = $request->getSession();
                $session->getFlashBag()->add('success', 'bibliometry.team.team_added');
            }
            
            return array(
                    'laboratory' => $laboratory,
                    'admin' => $admin,
                    'formTeam' => $formTeam->createView(),
                    'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
            );
        }
        else
        {
            $admin = false;
            return array(
                    'laboratory' => $laboratory,
                    'admin' => $admin,
                    'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
            );
        }
    }

    /**
     * @Route("/update-teams", name = "update_teams_route")
     * @Template()
     */
    public function updateTeamsAction(Request $request)
    {
        // Retrieve the laboratory configured for the application
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $laboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        $user = $this->getUser();
        if($laboratory->getLaboratoryTeam()->isManaged($user))
        {
            // We get the hierarchy in POST from AJAX
            if($request->getMethod() == 'POST')
            {
                // POST data
                $teamsHierarchyPosted = $request->get('teamsHierarchy');
                // Create a new array indexed by Team id
                $teamsHierarchy = array();
                foreach(array_slice($teamsHierarchyPosted, 1) as $team)
                {
                    $teamsHierarchy[$team["item_id"]] = $team["parent_id"];
                }
                
                // Retrieve the laboratory configured for the application
                $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
                $laboratory = $laboratoriesRepository->findOneBy(array(
                        "HALID" => $this->container->getParameter('HALID_lab')
                ));
                
                $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
                
                $em = $this->getDoctrine()->getManager();
                
                // Compare the given data with the DB data
                foreach($laboratory->getLaboratoryTeam()->getAllDaughterTeamsArray() as $teamDB)
                {
                    // Deleted team
                    if(!array_key_exists($teamDB->getId(), $teamsHierarchy))
                    {
                        // Delete all the notifications concerning the team before
                        $notificationsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Notification');
                        $notificationsTeam = $notificationsRepository->findByTeam($teamDB);
                        
                        foreach($notificationsTeam as $notification)
                        {
                            $em->remove($notification);
                        }
                        
                        $em->remove($teamDB);
                    }
                    // Update mother team
                    else
                    {
                        // Get the new mother team
                        $motherTeam = $teamsRepository->findOneById($teamsHierarchy[$teamDB->getId()]);
                        
                        // If mother has changed
                        if($motherTeam != $teamDB->getMotherTeam())
                        {
                            // Remove from the old one
                            $teamDB->getMotherTeam()->removeDaughterTeam($teamDB);
                            
                            // Add to the new one
                            $motherTeam->addDaughterTeam($teamDB);
                            
                            $em->persist($teamDB);
                        }
                    }
                }
                
                $em->flush();
                return new Response("Update success");
            }
            
            return new Response("Error : no data");
        }
        else
        {
            throw new AccessDeniedException();
        }
    }

    /**
     * @Route("/team/{slug_team}/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}",
     * name = "show_team_route",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+"},
     * defaults={"beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all"})
     * @Template()
     */
    public function showTeamAction($slug_team, $beginYear, $endYear, $beginMonth, $endMonth, $constraint)
    {
        if($slug_team == "NotFromLeafTeams")
        {
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            $team = $laboratory->getLaboratoryTeam();
            $team->setSlug("NotFromLeafTeams");
            $team->setName("Not From Leaf Teams");
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publicationsType = $publicationRepository->getPublicationsInSeveralFormat("NotFromLeafTeams", $team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        }
        else
        {
            // Retrieve the team corresponding to slug_team
            $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
            $team = $teamsRepository->findOneBySlug($slug_team);
            
            // Retrieve the laboratory configured for the application
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            
            // If not found, 404
            if($team == NULL)
            {
                throw $this->createNotFoundException('This team was not found on the website.');
            }
            
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publicationsType = $publicationRepository->getPublicationsInSeveralFormat("team", $team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        }
        return array(
                "beginYear" => $beginYear,
                "endYear" => $endYear,
                "beginMonth" => $beginMonth,
                "endMonth" => $endMonth,
                "constraint" => $constraint,
                "team" => $team,
                "laboratory" => $laboratory,
                "publicationsJournalsIndexedPerYear" => $publicationsType[5],
                "publicationsConferencesIndexedPerYear" => $publicationsType[4],
                "publicationsOthers" => $publicationsType[3],
                "publicationsJournals" => $publicationsType[2],
                "publicationsConferences" => $publicationsType[1],
                "publications" => $publicationsType[0],
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
        );
    }

    /**
     * @Route("/team/{slug_team}/statistics/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}",
     * name = "show_team_stats_route",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+"},
     * defaults={"beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all"})
     * @Template()
     */
    public function showStatsTeamAction($slug_team, $beginYear, $endYear, $beginMonth, $endMonth, $constraint)
    {
        if($slug_team == "NotFromLeafTeams")
        {
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            $team = $laboratory->getLaboratoryTeam();
            $team->setSlug("NotFromLeafTeams");
            $team->setName("Not From Leaf Teams");
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publicationsType = $publicationRepository->getPublicationsStatisticsInSeveralFormat("NotFromLeafTeams", $team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        }
        else
        {
            // Retrieve the team corresponding to slug_team
            $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
            $team = $teamsRepository->findOneBySlug($slug_team);
            
            // Retrieve the laboratory configured for the application
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            
            // If not found, 404
            if($team == NULL)
            {
                throw $this->createNotFoundException('This team was not found on the website.');
            }
            
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publicationsType = $publicationRepository->getPublicationsStatisticsInSeveralFormat("team", $team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        }
        return array(
                "team" => $team,
                "laboratory" => $laboratory,
                "beginYear" => $beginYear,
                "endYear" => $endYear,
                "beginMonth" => $beginMonth,
                "endMonth" => $endMonth,
                "constraint" => $constraint,
                'quartileRepartition' => $publicationsType[0],
                'journalRepartitionQuartile' => $publicationsType[1],
                'quartileEvolution' => $publicationsType[2],
                'averageIFEvolution' => $publicationsType[3],
                'conferenceRankingRepartition' => $publicationsType[4],
                'conferenceRepartitionRanking' => $publicationsType[5],
                'conferenceRankingEvolution' => $publicationsType[6]
        );
    }

    /**
     * @Route("/team/{slug_team}/manage",
     * name = "manage_team_route")
     * @Template()
     */
    public function manageTeamAction(Request $request, $slug_team)
    {
        // Retrieve the laboratory configured for the application
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $laboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        // Retrieve the team corresponding to slug_team
        $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
        $team = $teamsRepository->findOneBySlug($slug_team);
        
        // If not found, 404
        if($team == NULL)
        {
            throw $this->createNotFoundException('This team was not found on the website.');
        }
        
        // If not admin or not leader of the team, exception
        $user = $this->getUser();
        if(!$team->isManaged($user))
        {
            throw new AccessDeniedException();
        }
        
        // Initialize form to add researcher
        $addResearcherForm = $this->createForm('Bibliometry\MainBundle\Form\AddResearcherTeamType', $team,
        									   array('HALID' => $this->container->getParameter('HALID_lab')));
        
        // Handle data posted
        $addResearcherForm->handleRequest($request);
        
        if($addResearcherForm->isValid())
        {
            
            // Add the leader to the team, if not already the case
            if(!$team->getResearchersNotNested()->contains($team->getLeader()))
            {
                $team->addResearcher($team->getLeader());
            }
            
            $em = $this->getDoctrine()->getManager();
            $em->persist($team);
            $em->flush();
            
            $session = $request->getSession();
            $session->getFlashBag()->add('success', 'bibliometry.team.manage.success');
            
            return $this->redirect($this->generateUrl('manage_team_route', array(
                    'slug_team' => $team->getSlug()
            )));
        }
        
        return array(
                "team" => $team,
                'addResearcherForm' => $addResearcherForm->createView()
        );
    }

    /**
     * @Route("/api/export_publications_team/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}/{letterIndex}/{slug_team}",
     * defaults = { "beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all", "letterIndex" = "A" },
     * requirements = { "beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+" },
     * name = "export_publications_team_route")
     * @Template()
     */
    public function exportPublicationsAction(Request $request, $beginYear, $endYear, $beginMonth, $endMonth, $constraint, $letterIndex, $slug_team)
    {
        $giveJournals = $request->get('giveJournals');
        $giveConferences = $request->get('giveConferences');
        $giveOthers = $request->get('giveOthers');
        
        if($slug_team == "NotFromLeafTeams")
        {
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            $team = $laboratory->getLaboratoryTeam();
            $team->setSlug("NotFromLeafTeams");
            $team->setName("Not From Leaf Teams");
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publications = $publicationRepository->getPublicationsNotFromLeafTeams($team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $giveJournals, $giveConferences, $giveOthers, $constraint);
        }
        else
        {
            // Retrieve the team corresponding to slug_team
            $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
            $team = $teamsRepository->findOneBySlug($slug_team);
            
            // If not found, 404
            if($team == NULL)
            {
                throw $this->createNotFoundException('This team was not found on the website.');
            }
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publications = $publicationRepository->getPublicationsOfTeam($team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $giveJournals, $giveConferences, $giveOthers, $constraint);
        
        }
        
        $sort = $request->get('sort');
        $_format = $request->get('format');
        
        if($_format == "html")
            $contentType = "text/html";
        else if($_format == "txt" || $_format == "bib")
            $contentType = "text/plain";
        else if($_format == "json")
            $contentType = "application/json";
        else if ($_format == "doc")
        {
            $_format = "html";
            $contentType = "doc";
        }
        else if($_format != "pdf")
            return new Response("Unknown format");
        
        if($sort === "ranked")
            $publications = \Bibliometry\MainBundle\Entity\Publication::sortHydratedPublicationsByRanking($publications);
        else if($sort === "chronological")
            $publications = \Bibliometry\MainBundle\Entity\Publication::sortHydratedPublicationsByTime($publications);
        else if($sort === "alphabetical")
            $publications = \Bibliometry\MainBundle\Entity\Publication::sortHydratedPublicationsByAlphabetical($publications);
        $referencesPublications = \Bibliometry\MainBundle\Entity\Publication::getReferencesHydratedPublications($publications);
        
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        $researchersFromLabIds = [];
        foreach($homeLaboratory->getLaboratoryTeam()->getResearchers() as $researcherLabFromLab)
        {
            $researchersFromLabIds[] = $researcherLabFromLab->getId();
        }
        
        // Special case where we need to generate Latex and compile
        if($_format == "pdf")
        {
            putenv("HOME=" . $this->container->getParameter('apache_home'));
            // Generate the Latex using Twig
            $fileName = $team->getSlug() . time();
            $latex = $this->renderView('BibliometryMainBundle:Team:exportPublications.tex.twig', array(
                    "team" => $team,
                    'beginYear' => $beginYear,
                    'endYear' => $endYear,
                    "beginMonth" => $beginMonth,
                    "endMonth" => $endMonth,
                    "constraint" => $constraint,
                    'fileName' => $fileName,
                    'publications' => $publications,
                    'referencesPublications' => $referencesPublications,
                    'sort' => $sort,
                    'researchersFromLabIds' => $researchersFromLabIds
            ));
            // Put it in a file
            $path = __DIR__ . "/../../../../web/latex/";
            $latex = html_entity_decode($latex, ENT_QUOTES, "UTF-8"); // replace HTML characters by their signification: for example &039; by '
            file_put_contents($path . $fileName . ".tex", $latex);
            // Compile latex
            shell_exec("cd " . $path . ";" . "latex -interaction=nonstopmode " . $fileName . ";" . "bibtex " . $fileName . ";" . "pdflatex -interaction=nonstopmode " . $fileName);
            
            // Open PDF
            $pdf = file_get_contents($path . $fileName . ".pdf");
            $response = new Response();
            $response->headers->set('Content-Type', 'application/pdf');
            $response->setContent($pdf);
            
            // Remove temp files
            // shell_exec("cd " . $path . ";" . "rm " . $fileName . "*");
            
            return $response;
        }
        
        $viewName = 'BibliometryMainBundle:Team:exportPublications.' . $_format . '.twig';
        $page = $this->renderView($viewName, array(
                "team" => $team,
                'beginYear' => $beginYear,
                'endYear' => $endYear,
                "beginMonth" => $beginMonth,
                "endMonth" => $endMonth,
                "constraint" => $constraint,
                'letterIndex' => $letterIndex,
                'publications' => $publications,
                'referencesPublications' => $referencesPublications,
                'sort' => $sort,
                'researchersFromLabIds' => $researchersFromLabIds
        ));

        $response = new Response();
        if($contentType === "doc")
        {
            $filename = $team->getSlug() . ".doc";
            $contentType = "text/html";
            $response->headers->set('Content-Type', "application/msword; charset=utf8");
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
            $page = '<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head><body>' .$page . '</body></html>';
        }
        else
            $response->headers->set('Content-Type', $contentType . "; charset=utf8");
        $response->setContent($page);
        return $response;
    }

    /**
     * @Route("/team/{slug_team}/export-statistics/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}/{graph}.{_format}",
     * name = "export_team_stats_route",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+", "_format" = "csv", "graph" = "barchart_quartile_repartition|barchart_conference_ranking_repartition|linechart_quartile_evolution|linechart_average_if_evolution|linechart_conference_ranking_evolution|piechart_conference_repartition|piechart_journal_repartition|barchart_publications_per_year"},
     * defaults={"beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all"})
     */
    public function exportStatsTeamAction($slug_team, $beginYear, $beginMonth, $endMonth, $endYear, $constraint, $graph, $_format)
    {
        // Retrieve the team corresponding to slug_team
        $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
        $team = $teamsRepository->findOneBySlug($slug_team);
        
        // If not found, 404
        if($team == NULL)
        {
            throw $this->createNotFoundException('This team was not found on the website.');
        }
        
        $exported_data = $this->renderView('BibliometryMainBundle:Graphs:' . $graph . '.' . $_format . '.twig', array(
                "subject" => $team,
                "beginYear" => $beginYear,
                "endYear" => $endYear,
                "beginMonth" => $beginMonth,
                "endMonth" => $endMonth,
                "constraint" => $constraint
        ));
        
        return new Response($exported_data);
    }
    
    /**
     * @Route("/team/{slug_team}/{data_to_download}/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}",
     * name = "team_data_to_download_route",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+", "data_to_download" = "histogram_conference_countries|histogram_affiliations_nationalities"})
     * @Template()
     */
    public function exportDataTeam($data_to_download, $slug_team, $beginYear, $endYear, $beginMonth, $endMonth, $constraint)
    {
        if($slug_team == "NotFromLeafTeams")
        {
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            $team = $laboratory->getLaboratoryTeam();
            $team->setSlug("NotFromLeafTeams");
            $team->setName("Not From Leaf Teams");
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publicationsType = $publicationRepository->getPublicationsStatisticsInSeveralFormat("NotFromLeafTeams", $team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        }
        else
        {
            // Retrieve the team corresponding to slug_team
            $teamsRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Team');
            $team = $teamsRepository->findOneBySlug($slug_team);
            
            // Retrieve the laboratory configured for the application
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array(
                    "HALID" => $this->container->getParameter('HALID_lab')
            ));
            
            // If not found, 404
            if($team == NULL)
            {
                throw $this->createNotFoundException('This team was not found on the website.');
            }
            
            $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
            $publicationsType = $publicationRepository->getPublicationsStatisticsInSeveralFormat("team", $team->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        }
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv' . "; charset=utf8");
        if($data_to_download == "histogram_conference_countries")
        {
            $toDownload = "";
            $filename = $team->getSlug()."_".$beginYear."_".$endYear."_".$beginMonth."_".$endMonth."_HistogramConferenceCountries.csv";
            foreach($publicationsType[7] as $country => $val)
            {
                $toDownload .= "\n" . $country . ";" . strval($val);
            }
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
            $response->setContent($toDownload);
        }
        else if($data_to_download == "histogram_affiliations_nationalities")
        {
            $toDownload = "";
            $filename = $team->getSlug()."_".$beginYear."_".$endYear."_".$beginMonth."_".$endMonth."_HistogramAffiliationsNationalities.csv";
            foreach($publicationsType[8] as $country => $val)
            {
                $toDownload .= "\n" . $country . ";" . strval($val);
            }
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
            $response->setContent($toDownload);
        }
        return $response;
    }
}
