<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class NotificationController extends Controller
{
    /**
     * @Route("/notifications", name="notifications_route")
     * @Template()
     */
    public function showNotificationsAction()
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        foreach ($user->getUnreadNotifications() as $notification)
        {
            $notification->setIsRead(true);
            $em->persist($notification);
        }
        $em->flush();
        
        return array();
    }

}
