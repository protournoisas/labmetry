<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Bibliometry\MainBundle\Form\ConferenceType;
use Symfony\Component\HttpFoundation\Request;

class ConferenceController extends Controller
{

    /**
     * @Route("/conference", name="all_conferences_route")
     * @Template()
     */
    public function showConferencesAction()
    {
        $conferencesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Conference');
        $conferences = $conferencesRepository->getNotEmptyConferences();
        
        return array(
                'conferences' => $conferences
        );
    }

    /**
     * @Route("/conference/{slug_conference}", name="conference_route")
     * @Template()
     */
    public function showConferenceAction($slug_conference)
    {
        $conferencesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Conference');
        $conference = $conferencesRepository->findOneBySlug($slug_conference);
        
        // If not found, 404
        if($conference == NULL)
        {
            throw $this->createNotFoundException('This conference was not found on the website.');
        }
        
        return array(
                'conference' => $conference,
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
        );
    }

    /**
     * @Route("/edit-conference/{slug_conference}", name="edit_conference_route")
     * @Template()
     */
    public function editConferenceAction(Request $request, $slug_conference)
    {
        $conferencesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Conference');
        $conference = $conferencesRepository->findOneBySlug($slug_conference);
        
        // If not found, 404
        if($conference == NULL)
        {
            throw $this->createNotFoundException('This conference was not found on the website.');
        }
        
        if(false === $this->get('security.authorization_checker')->isGranted('ROLE_ADMIN'))
        {
            throw new AccessDeniedException();
        }
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\ConferenceType', $conference);
        
        $form->handleRequest($request);
        
        if($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($conference);
            $em->flush();
            
            $session = $request->getSession();
            $session->getFlashBag()->add('success', 'bibliometry.conference.edit_success');
            
            return $this->redirect($this->generateUrl('conference_route', array(
                    'slug_conference' => $conference->getSlug()
            )));
        }
        
        return array(
                'conference' => $conference,
                'form' => $form->createView()
        );
    }
}
