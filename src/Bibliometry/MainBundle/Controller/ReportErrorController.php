<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

use Bibliometry\MainBundle\Entity\ErrorReport;
use Bibliometry\MainBundle\Form\ErrorReportType;

class ReportErrorController extends Controller
{
    /**
     * @Route("/reportPopup", name="report_popup_route")
     * @Template()
     */
    public function reportPopupAction(Request $request)
    {
        // Create a new error report
        $user = $this->getUser();
        $errorReport = new ErrorReport($user);
        
        // Create the form of the error report
        $form = $this->createForm('Bibliometry\MainBundle\Form\ErrorReportType', $errorReport);
        
        // Handle the form
        $form->handleRequest($request);
        if ($form->isValid())
        {
            $em = $this->getDoctrine()->getManager();
            $em->persist($errorReport);
            $em->flush();
            
            $this->container->get("session")->getFlashBag()->add("success", "bibliometry.errors.error_report.report_sucess");
            $url = $request->headers->get("referer");
            $response = new Response();
            $response->setContent(json_encode(array(
                'url' => $url,
            )));
            $response->headers->set('Content-Type', 'application/json');
            
            return $response;
        }

        return array('form' => $form->createView());
    }

}
