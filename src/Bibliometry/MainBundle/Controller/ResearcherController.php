<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Bibliometry\MainBundle\Entity\Publication;

class ResearcherController extends Controller
{

    /**
     * @Route("/researcher", name = "researchers_researcher_researchers")
     * @Template()
     */
    public function showResearchersAction()
    {
        // Retrieve the laboratory configured for the application
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $laboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        return array(
                'laboratory' => $laboratory,
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN')
        );
    }

    /**
     * @Route("/researcher/{slug_researcher}/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}",
     * name = "researchers_researcher_researcher",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+"},
     * defaults={"beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all"})
     * @Template()
     */
    public function showResearcherAction($slug_researcher, $beginYear, $endYear, $beginMonth, $endMonth, $constraint)
    {
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        // Retrieve the researcher corresponding to slug_researcher
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $researcher = $researchersRepository->findOneBySlug($slug_researcher);
        
        // If not found, 404
        if($researcher == NULL)
        {
            throw $this->createNotFoundException('This researcher was not found on the website.');
        }
        
        if(!$this->getUser()->hasRole('ROLE_ADMIN') && $researcher != $this->getUser()->getResearcher())
        {
            throw new AccessDeniedException('bibliometry.researchers.researcher.access_denied');
        }
        $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $publicationsType = $publicationRepository->getPublicationsInSeveralFormat("researcher", $researcher->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        
        // Retrieve the researcher's teams
        $teams = $researcher->getTeams();
        if(count($teams) == 0)
        {
            $all_tree = array();
            $belonging_tree = array();
        }
        else
        {
            $laboratory = $teams[0]->getRootTeam();
            $all_tree = $laboratory->getAllDaughterTeams();
            $belonging_tree = $researcher->getAllTeamsAndAncestors();
        }
        
        // Get interests for word cloud
        $wordsInterestService = $this->container->get("bibliometry_main.WordsInterest");
        $wordsInterest = $wordsInterestService->getWordsInterest($publicationsType[2]);
        
        return array(
                "researcher" => $researcher,
                "beginYear" => $beginYear,
                "endYear" => $endYear,
                "beginMonth" => $beginMonth,
                "endMonth" => $endMonth,
                "constraint" => $constraint,
                "wordsInterest" => $wordsInterest,
                "all_tree" => $all_tree,
                "belonging_tree" => $belonging_tree,
                "homeLaboratory" => $homeLaboratory,
                "publicationsJournalsIndexedPerYear" => $publicationsType[5],
                "publicationsConferencesIndexedPerYear" => $publicationsType[4],
                "publicationsOthers" => $publicationsType[3],
                "publicationsJournals" => $publicationsType[2],
                "publicationsConferences" => $publicationsType[1],
                "publications" => $publicationsType[0]
        );
    }

    /**
     * @Route("/researcher/{slug_researcher}/statistics/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}/{nbOfCollaborators}/{showPublications}",
     * name = "researchers_researcher_stats",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+", "nbOfCollaborators" = "\d+", "showPublications" = "true|false"},
     * defaults={"beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all", "nbOfCollaborators" = "20", "showPublications" = "false"})
     * @Template()
     */
    public function showStatsResearcherAction($slug_researcher, $beginYear, $endYear, $beginMonth, $endMonth, $constraint, $nbOfCollaborators, $showPublications)
    {
        // Retrieve the researcher corresponding to slug_researcher
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $researcher = $researchersRepository->findOneBySlug($slug_researcher);
        
        // If not found, 404
        if($researcher == NULL)
        {
            throw $this->createNotFoundException('This researcher was not found on the website.');
        }
        
        if(!$this->getUser()->hasRole('ROLE_ADMIN') && $researcher != $this->getUser()->getResearcher())
        {
            throw new AccessDeniedException('bibliometry.researchers.researcher.access_denied');
        }
        
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        
        $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $publicationsType = $publicationRepository->getPublicationsStatisticsInSeveralFormat("researcher", $researcher->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        
        return array(
                "researcher" => $researcher,
                "beginYear" => $beginYear,
                "endYear" => $endYear,
                'beginMonth' => $beginMonth,
                'endMonth' => $endMonth,
                "constraint" => $constraint,
                "nbOfCollaborators" => $nbOfCollaborators,
                "showPublications" => $showPublications,
                "laboratory" => $homeLaboratory,
                'hasRightToSee' => $this->getUser()->hasRole('ROLE_ADMIN'),
                'quartileRepartition' => $publicationsType[0],
                'journalRepartitionQuartile' => $publicationsType[1],
                'quartileEvolution' => $publicationsType[2],
                'averageIFEvolution' => $publicationsType[3],
                'conferenceRankingRepartition' => $publicationsType[4],
                'conferenceRepartitionRanking' => $publicationsType[5],
                'conferenceRankingEvolution' => $publicationsType[6]
        );
    }

    /**
     * @Route("/api/export_publications/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}/{letterIndex}/{slug_researcher}",
     * defaults = { "beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all", "letterIndex" = "A" },
     * requirements = { "beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+" },
     * name = "export_publications_researcher_route")
     * @Template()
     */
    public function exportPublicationsAction(Request $request, $beginYear, $endYear, $beginMonth, $endMonth, $constraint, $letterIndex, $slug_researcher)
    {
        // Retrieve the researcher corresponding to slug_researcher
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $researcher = $researchersRepository->findOneBySlug($slug_researcher);
        
        // If not found, 404
        if($researcher == NULL)
        {
            throw $this->createNotFoundException('This researcher was not found on the website.');
        }
        if(!$this->getUser()->hasRole('ROLE_ADMIN') && $researcher != $this->getUser()->getResearcher())
        {
            throw new AccessDeniedException('bibliometry.researchers.researcher.access_denied');
        }
        
        $sort = $request->get('sort');
        $_format = $request->get('format');
        
        if($_format == "html")
            $contentType = "text/html";
        else if($_format == "txt" || $_format == "bib")
            $contentType = "text/plain";
        else if($_format == "json")
            $contentType = "application/json";
        else if ($_format == "doc")
        {
            $_format = "html";
            $contentType = "doc";
        } 
        else if($_format != "pdf")
            return new Response("Unknown format");
        
        $giveJournals = $request->get('giveJournals');
        $giveConferences = $request->get('giveConferences');
        $giveOthers = $request->get('giveOthers');
        
        $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $publications = $publicationRepository->getPublicationsOfResearcher($researcher->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $giveJournals, $giveConferences, $giveOthers, $constraint);
        
        if($sort === "ranked")
            $publications = \Bibliometry\MainBundle\Entity\Publication::sortHydratedPublicationsByRanking($publications);
        else if($sort === "chronological")
            $publications = \Bibliometry\MainBundle\Entity\Publication::sortHydratedPublicationsByTime($publications);
        else if($sort === "alphabetical")
            $publications = \Bibliometry\MainBundle\Entity\Publication::sortHydratedPublicationsByAlphabetical($publications);
        $referencesPublications = \Bibliometry\MainBundle\Entity\Publication::getReferencesHydratedPublications($publications);
        
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        $researchersFromLabIds = [];
        foreach($homeLaboratory->getLaboratoryTeam()->getResearchers() as $researcherLabFromLab)
        {
            $researchersFromLabIds[] = $researcherLabFromLab->getId();
        }
        // Special case where we need to generate Latex and compile
        if($_format == "pdf")
        {
            putenv("HOME=" . $this->container->getParameter('apache_home'));
            // Generate the Latex using Twig
            $fileName = $researcher->getSlug() . time();
            $latex = $this->renderView('BibliometryMainBundle:Researcher:exportPublications.tex.twig', array(
                    'researcher' => $researcher,
                    'beginYear' => $beginYear,
                    'endYear' => $endYear,
                    'beginMonth' => $beginMonth,
                    'endMonth' => $endMonth,
                    "constraint" => $constraint,
                    'fileName' => $fileName,
                    'referencesPublications' => $referencesPublications,
                    'publications' => $publications,
                    'sort' => $sort,
                    'researchersFromLabIds' => $researchersFromLabIds
            ));
            // Put it in a file
            $path = __DIR__ . "/../../../../web/latex/";
            $latex = html_entity_decode($latex, ENT_QUOTES, "UTF-8"); // replace HTML characters by their signification: for example &039; by '
            file_put_contents($path . $fileName . ".tex", $latex);
            // Compile latex
            shell_exec("cd " . $path . ";" . "latex -interaction=nonstopmode " . $fileName . ";" . "bibtex " . $fileName . ";" . "pdflatex -interaction=nonstopmode " . $fileName);
            
            // Open PDF
            $pdf = file_get_contents($path . $fileName . ".pdf");
            $response = new Response();
            $response->headers->set('Content-Type', 'application/pdf');
            $response->setContent($pdf);
            
            // Remove temp files
            shell_exec("cd " . $path . ";" . "rm " . $fileName . "*");
            
            return $response;
        }
        
        $viewName = 'BibliometryMainBundle:Researcher:exportPublications.' . $_format . '.twig';
        $page = $this->renderView($viewName, array(
                'researcher' => $researcher,
                'beginYear' => $beginYear,
                'endYear' => $endYear,
                'beginMonth' => $beginMonth,
                'endMonth' => $endMonth,
                "constraint" => $constraint,
                'letterIndex' => $letterIndex,
                'referencesPublications' => $referencesPublications,
                'publications' => $publications,
                'sort' => $sort,
                'researchersFromLabIds' => $researchersFromLabIds
        ));

        $response = new Response();
        if($contentType === "doc")
       	{
       	    $filename = $researcher->getSlug() . ".doc";
       	    $contentType = "text/html";
            $response->headers->set('Content-Type', "application/msword; charset=utf8");
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
            $page = '<html><head><meta http-equiv="content-type" content="text/html; charset=UTF-8"></head><body>' .$page . '</body></html>';
        }
        else
        	$response->headers->set('Content-Type', $contentType . "; charset=utf8");
        $response->setContent($page);
        return $response;
    }

    /**
     * @Route("/researcher/{slug_researcher}/export-statistics/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}/{nbOfCollaborators}/{showPublications}/{graph}.{_format}",
     * name = "researchers_researcher_export_stats",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+", "nbOfCollaborators" = "\d+", "showPublications" = "true|false", "_format" = "csv", "graph" = "barchart_quartile_repartition|barchart_conference_ranking_repartition|linechart_quartile_evolution|linechart_average_if_evolution|linechart_conference_ranking_evolution|piechart_conference_repartition|piechart_journal_repartition|barchart_publications_per_year"},
     * defaults={"beginYear" = "2005", "endYear" = "%current_year%", "beginMonth" = "1", "endMonth" = "12", "constraint" = "all", "nbOfCollaborators" = "20", "showPublications" = "false"})
     */
    public function exportStatsResearcherAction($slug_researcher, $beginYear, $endYear, $beginMonth, $endMonth, $constraint, $nbOfCollaborators, $showPublications, $graph, $_format)
    {
        // Retrieve the researcher corresponding to slug_researcher
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $researcher = $researchersRepository->findOneBySlug($slug_researcher);
        
        // If not found, 404
        if($researcher == NULL)
        {
            throw $this->createNotFoundException('This researcher was not found on the website.');
        }
        
        if(!$this->getUser()->hasRole('ROLE_ADMIN') && $researcher != $this->getUser()->getResearcher())
        {
            throw new AccessDeniedException('bibliometry.researchers.researcher.access_denied');
        }
        
        $exported_data = $this->renderView('BibliometryMainBundle:Graphs:' . $graph . '.' . $_format . '.twig', array(
                "subject" => $researcher,
                "beginYear" => $beginYear,
                "endYear" => $endYear,
                "beginMonth" => $beginMonth,
                "endMonth" => $endMonth,
                "constraint" => $constraint,
                "nbOfCollaborators" => $nbOfCollaborators,
                "showPublications" => $showPublications
        ));
        
        return new Response($exported_data);
    }
    
    /**
     * @Route("/researcher/{slug_researcher}/{data_to_download}/{beginYear}/{endYear}/{beginMonth}/{endMonth}/{constraint}",
     * name = "researcher_data_to_download_route",
     * requirements={"beginYear" = "\d{4}", "endYear" = "\d{4}", "beginMonth" = "\d+", "endMonth" = "\d+", "data_to_download" = "histogram_conference_countries|histogram_affiliations_nationalities"})
     * @Template()
     */
    public function exportDataResearcher($data_to_download, $slug_researcher, $beginYear, $endYear, $beginMonth, $endMonth, $constraint)
    {
        // Retrieve the researcher corresponding to slug_researcher
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $researcher = $researchersRepository->findOneBySlug($slug_researcher);
        // If not found, 404
        if($researcher == NULL)
        {
            throw $this->createNotFoundException('This researcher was not found on the website.');
        }
        if(!$this->getUser()->hasRole('ROLE_ADMIN') && $researcher != $this->getUser()->getResearcher())
        {
            throw new AccessDeniedException('bibliometry.researchers.researcher.access_denied');
        }
        $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->container->getParameter('HALID_lab')
        ));
        $publicationRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Publication');
        $publicationsType = $publicationRepository->getPublicationsStatisticsInSeveralFormat("researcher", $researcher->getId(), $beginYear, $endYear, $beginMonth, $endMonth, $constraint);
        $response = new Response();
        $response->headers->set('Content-Type', 'text/csv' . "; charset=utf8");
        if($data_to_download == "histogram_conference_countries")
        {
            $toDownload = "";
            $filename = $researcher->getSlug()."_".$beginYear."_".$endYear."_".$beginMonth."_".$endMonth."_HistogramConferenceCountries.csv";
            foreach($publicationsType[7] as $country => $val)
            {
                $toDownload .= "\n" . $country . ";" . strval($val);
            }
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
            $response->setContent($toDownload);
        }
        else if($data_to_download == "histogram_affiliations_nationalities")
        {
            $toDownload = "";
            $filename = $researcher->getSlug()."_".$beginYear."_".$endYear."_".$beginMonth."_".$endMonth."_HistogramAffiliationsNationalities.csv";
            foreach($publicationsType[8] as $country => $val)
            {
                $toDownload .= "\n" . $country . ";" . strval($val);
            }
            $response->headers->set('Content-Disposition', 'attachment; filename="'.$filename.'"');
            $response->setContent($toDownload);
        }
        return $response;
    }
}
