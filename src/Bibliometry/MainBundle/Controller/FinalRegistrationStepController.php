<?php

namespace Bibliometry\MainBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;

use Bibliometry\MainBundle\Form\UserType;
use Bibliometry\MainBundle\Entity\Researcher;

class FinalRegistrationStepController extends Controller
{
    /**
     * @Route("/end-registration", name="final_registration_route")
     * @Template()
     */
    public function finalRegistrationAction(Request $request)
    {   
        $user = $this->getUser();
        
        if ($user->getRegistrationCompleted() == true)
        {
            return $this->redirect($this->generateUrl('welcome_route'));
        }
        
        $form = $this->createForm('Bibliometry\MainBundle\Form\UserType', $user);
        
        $form->handleRequest($request);
        if ($form->isValid()) {
        
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();
            
            if ($request->request->get('user_role') == "researcher")
            {
                return $this->redirect($this->generateUrl('select_matching_researcher_route'));
            }
            
            $user->setRegistrationCompleted(true);
            $em->persist($user);
            $em->flush();
            
            $this->sendWelcomeMail($user);
            
            return $this->redirect($this->generateUrl('welcome_route'));
        }
        
        return array('form' => $form->createView());
    }

    /**
     * @Route("/select-matching-researcher", name="select_matching_researcher_route")
     * @Template()
     */
    public function selectMatchingResearcherAction(Request $request)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        
        if ($user->getRegistrationCompleted() == true)
        {
            return $this->redirect($this->generateUrl('welcome_route'));
        }
        
        if ($request->getMethod() == "POST")
        {
            $selectedResearchers = $request->request->get('matched_researchers');
            // If user didn't select any option
            if ($selectedResearchers == NULL)
            {
                // If there is not matching researcher, create one
                if ($request->request->get('no_researcher_match'))
                {
                    $selectedResearcher = new Researcher();
                    $selectedResearcher->setName($user->getName());
                    $selectedResearcher->setSurname($user->getSurname());
                    $em->persist($selectedResearcher);
                }
                else
                {
                    $session = $request->getSession();
                    $session->getFlashBag()->add('error', 'bibliometry.researchers.select_matching_researcher.error_no_one_selected');
                    
                    return $this->redirect($this->generateUrl('select_matching_researcher_route'));
                }
            }
            else {
                // Several account selected
                if (count($selectedResearchers) > 1)
                {
                    $session = $request->getSession();
                    $session->set('selectedResearchers', $selectedResearchers);
                    return $this->redirect($this->generateUrl('select_main_researcher_route'));
                }
                // Only one account
                $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
                $selectedResearcher = $researchersRepository->findOneById($selectedResearchers[0]);
            }
            
            $user->setResearcher($selectedResearcher);
            $user->setRegistrationCompleted(true);
            $em->persist($user);
            $em->flush();
        
            $this->sendWelcomeMail($user);
            // Welcome redirection
            return $this->redirect($this->generateUrl('welcome_route'));
        }

        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        $matchingResearchers = $researchersRepository->getMatchingResearchers($user->getSurname());
        
        return array('matchingResearchers' => $matchingResearchers);
    }
    
    /**
     * @Route("/select-main-researcher", name="select_main_researcher_route")
     * @Template()
     */
    public function selectMainResearcherAction(Request $request)
    {
        $user = $this->getUser();
        
        if ($user->getRegistrationCompleted() == true)
        {
            return $this->redirect($this->generateUrl('welcome_route'));
        }
        
        $session = $request->getSession();
        $selectedResearchersId = $session->get('selectedResearchers');
        
        $selectedResearchers = array();
        $researchersRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Researcher');
        foreach ($selectedResearchersId as $id)
        {
            $selectedResearchers[] = $researchersRepository->findOneById($id);
        }
        
        if ($request->getMethod() == "POST")
        {
            $mainResearcherId = $request->request->get('main_researcher');
            
            // If user didn't select any option
            if ($mainResearcherId == NULL)
            {
                $session->getFlashBag()->add('error', 'bibliometry.researchers.select_main_researcher.error_no_one_selected');
            }
            // Merge all the accounts with the main one
            else {
                $mainResearcher = $researchersRepository->findOneById($mainResearcherId);
            
                $em = $this->getDoctrine()->getManager();
                foreach ($selectedResearchers as $researcher)
                {   
                    if ($researcher != $mainResearcher)
                    {
                        $mainResearcher->mergeWith($researcher);
                        $em->remove($researcher);
                    }
                }
                $user->setResearcher($mainResearcher);
                $user->setRegistrationCompleted(true);
                $em->persist($user);
            
                $em->flush();
            
                $this->sendWelcomeMail($user);
                // Welcome redirection
                return $this->redirect($this->generateUrl('welcome_route'));
            }
        }
        
        return array('selectedResearchers' => $selectedResearchers);
    }
    
    /**
     * @Route("/", name="welcome_route")
     * @Template()
     */
    public function welcomeAction()
    {
        $user = $this->getUser();
        
        // Researcher : redirect to personnal page
        if ($user->getResearcher())
        {
            return $this->redirect($this->generateUrl('researchers_researcher_researcher', array('slug_researcher' => $user->getResearcher()->getSlug())));
        }
        // Admin : redirect to lab page
        else
        {
            $laboratoriesRepository = $this->getDoctrine()->getRepository('BibliometryMainBundle:Laboratory');
            $laboratory = $laboratoriesRepository->findOneBy(array("HALID" => $this->container->getParameter('HALID_lab')));
            
            return $this->redirect($this->generateUrl('show_team_route', array('slug_team' => $laboratory->getSlug())));
        }
        
        return array();
    }
    
    private function sendWelcomeMail($user)
    {
        $message = \Swift_Message::newInstance()
        ->setSubject($this->get('translator')->trans('bibliometry.researchers.final_registration_step.welcome_mail_title', array('%name%' => $user->getName())))
        ->setFrom($this->container->getParameter('mailer_sender'))
        ->setTo($user->getEmail())
        ->setBody($this->renderView('BibliometryMainBundle:FinalRegistrationStep:welcomeMail.html.twig', array('user' => $user)), 'text/html')
        ;
        $this->get('mailer')->send($message);
    }
}
