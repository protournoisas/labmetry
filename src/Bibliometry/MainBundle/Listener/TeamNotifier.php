<?php

namespace Bibliometry\MainBundle\Listener;

use Bibliometry\MainBundle\Entity\Team;
use Bibliometry\MainBundle\Entity\Notification;

class TeamNotifier
{
    private $translator;
    private $container;
    private $mailer_sender;
    private $mailer;
    
    public function __construct($translator, $container, $mailer_sender, $mailer)
    {
        $this->translator = $translator;
        $this->container = $container;
        $this->mailer_sender = $mailer_sender;
        $this->mailer = $mailer;
    }
    
    public function onFlush($eventArgs)
    {
        $entityManager = $eventArgs->getEntityManager();
        $uow = $entityManager->getUnitOfWork();
        
        foreach ($uow->getScheduledEntityUpdates() as $entity) {
            if ($entity instanceof Team) {
                // Notifications for new members
                $newMembers = $entity->getResearchersCollection()->getInsertDiff();
                foreach ($newMembers as $member)
                {
                    if ($member->getUser())
                    {
                        $notification = new Notification();
                        $notification->setType("teamMember");
                        $notification->setTeam($entity);
                        $notification->setUser($member->getUser());
                        $notification->setShortMessage($this->translator->trans("bibliometry.notifications_messages.teamMember_short"));
        
                        $fullMessage = $this->translator->trans("bibliometry.notifications_messages.teamMember_full", array("%team_name%" => $entity->getName()));
                        $notification->setFullMessage($fullMessage);
            
                        $entityManager->persist($notification);
                        $md = $entityManager->getClassMetadata('Bibliometry\MainBundle\Entity\Notification');
                        $uow->computeChangeSet($md, $notification);
                    }
                }
                // Notification for new team leader
                $changes = $uow->getEntityChangeSet($entity);
                if (array_key_exists("leader", $changes))
                {
                    if ($entity->getLeader() && $entity->getLeader()->getUser())
                    {
                        $notification = new Notification();
                        $notification->setType("teamLeader");
                        $notification->setTeam($entity);
                        $notification->setUser($entity->getLeader()->getUser());
                        $notification->setShortMessage($this->translator->trans("bibliometry.notifications_messages.teamLeader_short"));
    
                        $fullMessage = $this->translator->trans("bibliometry.notifications_messages.teamLeader_full", array("%team_name%" => $entity->getName()));
                        $notification->setFullMessage($fullMessage);
        
                        $entityManager->persist($notification);
                        $md = $entityManager->getClassMetadata('Bibliometry\MainBundle\Entity\Notification');
                        $uow->computeChangeSet($md, $notification);
                        
                        $this->sendLeaderMail($entity->getLeader()->getUser(), $entity);
                    }
                }
            }
        }
    }
    
    private function sendLeaderMail($user, $team)
    {
        $message = \Swift_Message::newInstance()
        ->setSubject($this->translator->trans('bibliometry.mail.mail_leader_team_title', array('%team%' => $team->getName())))
        ->setFrom($this->mailer_sender)
        ->setTo($user->getEmail())
        ->setBody($this->container->get('templating')->render('BibliometryMainBundle:Notification:teamLeaderMail.html.twig', array('team' => $team)), 'text/html')
        ;
        $this->mailer->send($message);
    }
}