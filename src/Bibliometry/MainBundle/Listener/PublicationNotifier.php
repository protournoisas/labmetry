<?php

namespace Bibliometry\MainBundle\Listener;

use Bibliometry\MainBundle\Entity\Publication;
use Bibliometry\MainBundle\Entity\PublicationAuthor;
use Bibliometry\MainBundle\Entity\Notification;

class PublicationNotifier
{
    private $translator;
    
    public function __construct($translator)
    {
        $this->translator = $translator;
    }
    
    public function onFlush($eventArgs)
    {
        $entityManager = $eventArgs->getEntityManager();
        $uow = $entityManager->getUnitOfWork();
        
        foreach ($uow->getScheduledEntityInsertions() as $publicationAuthor) {
            if ($publicationAuthor instanceof PublicationAuthor)
            {
                $author = $publicationAuthor->getResearcher();
                $publication = $publicationAuthor->getPublication();
                if ($author->getUser())
                {
                    $notification = new Notification();
                    $notification->setType("publication");
                    $notification->setPublication($publication);
                    $notification->setUser($author->getUser());
                    $notification->setShortMessage($this->translator->trans("bibliometry.notifications_messages.publication_short"));
    
                    $fullMessage = $this->translator->trans("bibliometry.notifications_messages.publication_full", array("%publication_title%" => $publication->getTitle()));
                    $notification->setFullMessage($fullMessage);
        
                    $entityManager->persist($notification);
                    $md = $entityManager->getClassMetadata('Bibliometry\MainBundle\Entity\Notification');
                    $uow->computeChangeSet($md, $notification);
                }
            }
        }
    }
}