<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JournalType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('iSSN')
            ->add('IFs', 'Symfony\Component\Form\Extension\Core\Type\CollectionType', array(
                'entry_type'   => 'Bibliometry\MainBundle\Form\ImpactFactorType',
                'allow_add'  => true,
                'allow_delete' => true
            ))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\Journal'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_journal';
    }
}
