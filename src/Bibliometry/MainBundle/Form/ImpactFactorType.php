<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ImpactFactorType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('year')
            ->add('value')
            ->add('quartile')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\ImpactFactor'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_impactfactor';
    }
}
