<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\EmailType;

class EditUserType extends AbstractType
{
    private $authentication_type;

    public function __construct($authentication_type)
    {
        $this->authentication_type = $authentication_type;
    }
    
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        if ($this->authentication_type != "LDAP")  // If LDAP, no need of username/mail/password
        {
            $builder
            ->add('username')
            ->add('email', 'Symfony\Component\Form\Extension\Core\Type\EmailType');
        }
        $builder->add('name', null, array('required' => true));
        $builder->add('surname', null, array('required' => true));
        $builder->add('photo', 'Bibliometry\MainBundle\Form\PhotoType', array('required' => false));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\User',
            'csrf_token_id'  => 'profile',
            'validation_groups' => ['Profile']
        ));
    }

    public function getBlockPrefix()
    {
        return 'user_profile';
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
}
