<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Security\Core\Validator\Constraints\UserPassword;
use Symfony\Component\Validator\Constraints\NotBlank;

class ChangePasswordFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('current_password', 'Symfony\Component\Form\Extension\Core\Type\PasswordType', array(
            'label' => 'bibliometry.reset_password.current_password',
            'mapped' => false,
            'constraints' => [new NotBlank(), new UserPassword()],
        ));
        $builder->add('plainPassword', 'Symfony\Component\Form\Extension\Core\Type\RepeatedType', array(
            'type' => 'Symfony\Component\Form\Extension\Core\Type\PasswordType',
            'first_options' => array('label' => 'bibliometry.reset_password.first_password'),
            'second_options' => array('label' => 'bibliometry.reset_password.second_password'),
            'invalid_message' => 'bibliometry.errors.fos_user.password.mismatch',
        ));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\User',
            'csrf_token_id' => 'change_password',
        ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }

    public function getBlockPrefix()
    {
        return 'user_change_password';
    }
}
