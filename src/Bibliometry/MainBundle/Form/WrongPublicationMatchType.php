<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

class WrongPublicationMatchType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('publication', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Publication', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('p')->orderBy('p.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_publication'))
            ->add('rightConference', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Conference', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('c')->orderBy('c.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_conference', 'required' => false))
            ->add('conferenceNotReferenced', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array('required'  => false))
            ->add('rightJournal', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Journal', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('j')->orderBy('j.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_journal', 'required' => false))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\WrongPublicationMatch'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_wrongpublicationmatch';
    }
}
