<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegistrationFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', 'Symfony\Component\Form\Extension\Core\Type\EmailType')
            ->add('username')
            ->add('plainPassword', 'Symfony\Component\Form\Extension\Core\Type\RepeatedType', array(
                'type' => 'Symfony\Component\Form\Extension\Core\Type\PasswordType',
                'first_options' => array('label' => 'bibliometry.registration.password1'),
                'second_options' => array('label' => 'bibliometry.registration.password2'),
                'invalid_message' => 'bibliometry.errors.fos_user.password.mismatch',
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\User',
            'csrf_token_id' => 'registration'
        ));
    }

    public function getName()
    {
        return $this->getBlockPrefix();
    }
    
    public function getBlockPrefix()
    {
        return 'user_registration';
    }
}
