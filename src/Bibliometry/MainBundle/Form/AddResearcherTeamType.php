<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

class AddResearcherTeamType extends AbstractType
{
    private $HALID;
    public function __construct()
    {
    }
    
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
    	$this->HALID = $options['HALID'];
        $builder
             ->add('name')
             ->add('leader', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'BibliometryMainBundle:Researcher',
                'placeholder' => 'bibliometry.team.new_team_leader',
                'label' => 'bibliometry.team.manage.leader',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                    ->innerJoin('r.teams', 't')
                    ->innerJoin('t.laboratory', 'l')
                    ->where('l.HALID LIKE :halid')
                    ->setParameter('halid', '%'.$this->HALID.'%')
                    ->orderBy('r.surname', 'ASC');
            }))
            ->add('researchers', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array(
                'class' => 'BibliometryMainBundle:Researcher',
                'multiple' => true,
                'label' => 'bibliometry.team.manage.members',
                'query_builder' => function(EntityRepository $er) {
                    return $er->createQueryBuilder('r')
                    ->orderBy('r.surname', 'ASC');
            }))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\Team',
        	'HALID' => ''
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_team';
    }
}
