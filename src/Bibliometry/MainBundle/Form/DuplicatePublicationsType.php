<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

class DuplicatePublicationsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rightPublication', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Publication', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('p')->where('p.mergedTo IS NULL')->orderBy('p.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_publication'))
            ->add('wrongPublication', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Publication', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('p')->where('p.mergedTo IS NULL')->orderBy('p.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_publication'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\DuplicatePublications'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_duplicatepublications';
    }
}
