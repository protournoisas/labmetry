<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;

use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class PublicationType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('year')
            ->add('month')
            ->add('pages')
        ;
        
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            $publication = $event->getData();
            $form = $event->getForm();

            if ($publication)
            {
                if ($publication->getType() == "conference")
                {
                    $form->add('publiConference', 'Bibliometry\MainBundle\Form\PubliConferenceType');
                }
                else if ($publication->getType() == "journal")
                {
                    $form->add('publiJournal', 'Bibliometry\MainBundle\Form\PubliJournalType');
                }
                else
                {
                    $form->add('publiOther', 'Bibliometry\MainBundle\Form\PubliOtherType');
                }
            }
        });
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\Publication'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_publication';
    }
}
