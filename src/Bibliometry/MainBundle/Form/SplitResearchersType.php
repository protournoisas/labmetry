<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

class SplitResearchersType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('multipleResearcher', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Researcher', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('r')->addOrderBy('r.surname', 'ASC')->addOrderBy('r.name', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_researcher'))
            ->add('splitResearchers')
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\SplitResearchers'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_splitresearchers';
    }
}
