<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

class DuplicateJournalsType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rightJournal', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Journal', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('j')->where('j.mergedTo IS NULL')->orderBy('j.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_journal'))
            ->add('wrongJournal', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Journal', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('j')->where('j.mergedTo IS NULL')->orderBy('j.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_journal'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\DuplicateJournals'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_duplicatejournals';
    }
}
