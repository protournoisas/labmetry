<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Doctrine\ORM\EntityRepository;

class NotLabPublicationType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('publication', 'Symfony\Bridge\Doctrine\Form\Type\EntityType', array('class' => 'BibliometryMainBundle:Publication', 'query_builder' => function(EntityRepository $er) { return $er->createQueryBuilder('p')->orderBy('p.title', 'ASC');}, 'placeholder' => 'bibliometry.errors.error_report.select_publication'))
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\NotLabPublication'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_notlabpublication';
    }
}
