<?php

namespace Bibliometry\MainBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class ErrorReportType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('errorType', 'Symfony\Component\Form\Extension\Core\Type\ChoiceType', array(
                'placeholder' => 'bibliometry.errors.error_report.select_error_type',
                'choices_as_values' => true,
                'choices'   => array('bibliometry.errors.error_report.duplicatePublications' => 'duplicatePublications',
                                     'bibliometry.errors.error_report.duplicateResearchers' => 'duplicateResearchers',
                                     'bibliometry.errors.error_report.duplicateJournals' => 'duplicateJournals',
                                     'bibliometry.errors.error_report.wrongPublicationMatch' => 'wrongPublicationMatch',
                                     'bibliometry.errors.error_report.splitResearchers' => 'splitResearchers',
                                     'bibliometry.errors.error_report.notLabPublication' => 'notLabPublication'
                                     ),
                'required'  => true,
            ))
        ;
        
        $formModifier = function (FormInterface $form, $errorType = null) {
            if ($errorType != null)
            {
                switch ($errorType)
                {
                    case 'duplicatePublications':
                        $form->add('duplicatePublications', 'Bibliometry\MainBundle\Form\DuplicatePublicationsType');
                        break;
                    case 'duplicateResearchers':
                        $form->add('duplicateResearchers', 'Bibliometry\MainBundle\Form\DuplicateResearchersType');
                        break;
                    case 'duplicateJournals':
                        $form->add('duplicateJournals', 'Bibliometry\MainBundle\Form\DuplicateJournalsType');
                        break;
                    case 'wrongPublicationMatch':
                        $form->add('wrongPublicationMatch', 'Bibliometry\MainBundle\Form\WrongPublicationMatchType');
                        break;
                    case 'splitResearchers':
                        $form->add('splitResearchers', 'Bibliometry\MainBundle\Form\SplitResearchersType');
                        break;
                    case 'notLabPublication':
                        $form->add('notLabPublication', 'Bibliometry\MainBundle\Form\NotLabPublicationType');
                        break;
                }
                $form->add('comments', 'Symfony\Component\Form\Extension\Core\Type\TextareaType');
                $form->add('sendMail', 'Symfony\Component\Form\Extension\Core\Type\CheckboxType', array('required' => false));
            }
        };

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) use ($formModifier) {
                $data = $event->getData();

                $formModifier($event->getForm(), $data->getErrorType());
            }
        );

        $builder->get('errorType')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) use ($formModifier) {
                // It's important here to fetch $event->getForm()->getData(), as
                // $event->getData() will get you the client data (that is, the ID)
                $errorType = $event->getForm()->getData();

                // since we've added the listener to the child, we'll have to pass on
                // the parent to the callback functions!
                $formModifier($event->getForm()->getParent(), $errorType);
            }
        );
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Bibliometry\MainBundle\Entity\ErrorReport'
        ));
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'bibliometry_mainbundle_errorreport';
    }
}
