<?php

namespace Bibliometry\MainBundle\Twig;

use Gedmo\Sluggable\Util\Urlizer;

class StringToLatex extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('toLaTeX', array($this, 'toLaTeX')),
        );
    }

    public function toLaTeX($string)
    {
        // Process ligature chars
        $string = \Normalizer::normalize($string, \Normalizer::FORM_KC);
        // Replace all UTF8 chars
        $string = Urlizer::utf8ToAscii($string);
        // Process special LateX chars
        $string = str_replace("\\", "\\textbackslash", $string);
        $string = str_replace("^", "\\textasciicircum", $string);
        $string = str_replace("~", "\textasciitilde", $string);
        foreach (["#", "$", "%", "&", "_", "{", "}"] as $character)
        {
            $string = str_replace($character, "\\".$character, $string);
        }
        return $string;
    }
    
   
    
    public function getName()
    {
        return 'string_to_latex_extension';
    }
}