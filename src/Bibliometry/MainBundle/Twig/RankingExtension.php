<?php

namespace Bibliometry\MainBundle\Twig;

class RankingExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('if_color', array($this, 'ifColorFilter')),
            new \Twig_SimpleFilter('conf_color', array($this, 'confRankColorFilter')),
            new \Twig_SimpleFilter('conf_background_color', array($this, 'confRankBoxBackgroundColor')),
            new \Twig_SimpleFilter('journal_background_color', array($this, 'journalRankBoxBackgroundColor')),
        );
    }

    public function ifColorFilter($impactFactor, $quartile)
    {
        if ($quartile == 1 || $quartile == 2)
        {
            $color = "green";
        }
        else if ($quartile == 3)
        {
            $color = "yellow";
        }
        else if ($quartile == 4)
        {
            $color = "red";
        }
        return "<small class='badge bg-".$color."'>".$impactFactor." (Q".$quartile.")</small>";
    }
    
    public function journalRankBoxBackgroundColor($quartile)
    {
        if ($quartile == 1 || $quartile == 2)
        {
            $color = "green";
        }
        else if ($quartile == 3)
        {
            $color = "yellow";
        }
        else if ($quartile == 4)
        {
            $color = "red";
        }
        else
        {
             $color = "darkgrey";
        }
        
        return $color;
    }
    
    public function confRankColorFilter($rank)
    {
        if ($rank == "A*" || $rank == "A")
        {
            $color = "green";
        }
        else if ($rank == "B")
        {
            $color = "yellow";
        }
        else
        {
            $color = "red";
        }
        return "<small class='badge bg-".$color."'>".$rank."</small>";
    }
    
    public function confRankBoxBackgroundColor($rank)
    {
        if ($rank == "A*" || $rank == "A")
        {
            $color = "green";
        }
        else if ($rank == "B")
        {
            $color = "yellow";
        }
        else if ($rank == "C")
        {
            $color = "red";
        }
        else if ($rank == "FR")
        {
            $color = "blue";
        }
        else
        {
            $color = "darkgrey";
        }
        
        return $color;
    }
    
    public function getName()
    {
        return 'ranking_extension';
    }
}