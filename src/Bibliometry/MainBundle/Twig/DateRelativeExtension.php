<?php

namespace Bibliometry\MainBundle\Twig;

class DateRelativeExtension extends \Twig_Extension
{
    protected $translator;
    
    public function __construct($translator) {
        $this->translator = $translator;
    }

    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('dateRelative', array($this, 'dateRelative')),
        );
    }

    public function dateRelative(\DateTime $date)
    {
        $auj = new \DateTime;
        $intervalle = date_diff($date, $auj);
        
        $toReturn = "";
        if ($intervalle->y >= 1) {
            $value = $intervalle->y;
            $toReturn = $intervalle->y." ".$this->translator->trans("bibliometry.errors.date_relative.year");
        }
        else if ($intervalle->m >= 1) {
            $value = $intervalle->m;
            $toReturn = $intervalle->m." ".$this->translator->trans("bibliometry.errors.date_relative.month");
        }
        else if ($intervalle->d >= 1) {
            $value = $intervalle->d;
            $toReturn = $intervalle->d." ".$this->translator->trans("bibliometry.errors.date_relative.day");
        }
        else if ($intervalle->h >= 1) {
            $value = $intervalle->h;
            $toReturn = $intervalle->h." ".$this->translator->trans("bibliometry.errors.date_relative.hour");
        }
        else if ($intervalle->i >= 1) {
            $value = $intervalle->i;
            $toReturn = $intervalle->i." ".$this->translator->trans("bibliometry.errors.date_relative.minute");
        }
        else {
            $value = 0;
            $toReturn = $this->translator->trans("bibliometry.errors.date_relative.seconds");
        }
        if ($value > 1) {
            $toReturn = $toReturn."s";
        }
        return $toReturn;
    }

    public function getName()
    {
        return 'taikai_messagerie.twig.date_relative';
    }
}