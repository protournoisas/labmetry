<?php

namespace Bibliometry\MainBundle\Twig;

class HighlightSearch extends \Twig_Extension
{

    public function getFilters()
    {
        return array(
                new \Twig_SimpleFilter('highlightSearch', array(
                        $this,
                        'highlightSearch'
                ))
        );
    }

    public function highlightSearch($result, $query)
    {
        $keywords = explode(" ", $query);
        $keywords2 = explode(" ", $result);
        $toReturn = array();
        
        foreach($keywords2 as $keywordToHiglight)
        {
            $queryKeywordWithLargerMatch = "";
            $sizeLargerMatch = 0;
            foreach($keywords as $keywordQuery)
            {
                if(preg_match("/(" . preg_quote($keywordQuery, "/") . ")/i", $keywordToHiglight, $matched))
                {
                    $sizeMatch = strlen($matched[0]);
                    if($sizeMatch > $sizeLargerMatch)
                    {
                        $sizeLargerMatch = $sizeMatch;
                        $queryKeywordWithLargerMatch = $keywordQuery;
                    }
                }
            }
            if($sizeLargerMatch > 0) // highlighted: add it with yellow color for the part that matched
                array_push($toReturn, preg_replace("/(" . preg_quote($queryKeywordWithLargerMatch, "/") . ")/i", "<span style='background-color:yellow'>\\1</span>", $keywordToHiglight));
            else // not hilighted: add it normal
                array_push($toReturn, $keywordToHiglight);
        }
        return implode(" ", $toReturn);
    }

    public function getName()
    {
        return 'highlight_search_extension';
    }
}
