<?php

namespace Bibliometry\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class CoreCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('bibliometry:core-import')
            ->setDescription('Import CORE conference rankings')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $CoreService = $this->getApplication()->getKernel()->getContainer()->get('bibliometry_main.Core');
        $CoreService->importConferences($output);
    }
}