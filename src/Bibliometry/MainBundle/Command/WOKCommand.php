<?php

namespace Bibliometry\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class WOKCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('bibliometry:wok-import')
            ->setDescription('Import WOK impact factors and quartiles')
            ->addArgument(
                'beginYear',
                InputArgument::OPTIONAL,
                'IF/Quartile from beginYear'
            )
            ->addArgument(
                'endYear',
                InputArgument::OPTIONAL,
                'IF/Quartile to endYear'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository("BibliometryMainBundle:Journal");
        $journals = $repository->findAll();
        $WOKService = $this->getApplication()->getKernel()->getContainer()->get('bibliometry_main.WOK');
	$WOKService->loginToWOK();
        $WOKService->importWOK($journals, $output);
    }
}
