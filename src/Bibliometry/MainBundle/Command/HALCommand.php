<?php

namespace Bibliometry\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class HALCommand extends Command
{
    protected function configure()
    {
        $this
            ->setName('bibliometry:hal-import')
            ->setDescription('Import HAL publications')
            ->addArgument(
                'beginYear',
                InputArgument::OPTIONAL,
                'Publication date superior or equal to beginYear'
            )
            ->addArgument(
                'endYear',
                InputArgument::OPTIONAL,
                'Publication date less or equal to endYear'
            )
            ->addOption(
                'useWOK',
                null,
                InputOption::VALUE_NONE,
                'Import journals Impact Factors along with the publications import (takes time)'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $HALV3Service = $this->getApplication()->getKernel()->getContainer()->get('bibliometry_main.HALV3');
        
        $beginYear = $input->getArgument('beginYear') ? $input->getArgument('beginYear') : date('Y') - 5;
        $endYear = $input->getArgument('endYear') ? $input->getArgument('endYear') : date('Y');
        
        $HALV3Service->importPublications($beginYear, $endYear, $input->getOption('useWOK') ? true : false, $output);
    }
}
