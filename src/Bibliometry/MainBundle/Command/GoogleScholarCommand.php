<?php

namespace Bibliometry\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GoogleScholarCommand extends Command
{

    protected function configure()
    {
        $this->setName('bibliometry:scholar-import')->setDescription('Import H-indexes from Google Scholar');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $laboratoriesRepository = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->getApplication()->getKernel()->getContainer()->getParameter('HALID_lab')
        ));
        $researchers = $homeLaboratory->getLaboratoryTeam()->getResearchers();
        $ScholarService = $this->getApplication()->getKernel()->getContainer()->get("bibliometry_main.GoogleScholar");
        $ScholarService->updateHIndex($researchers, $output);
    }
}