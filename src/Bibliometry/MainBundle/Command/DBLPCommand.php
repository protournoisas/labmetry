<?php

namespace Bibliometry\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class DBLPCommand extends Command
{

    protected function configure()
    {
        $this->setName('bibliometry:dblp-import')->setDescription('Search and store the DBLP link of the researchers');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $laboratoriesRepository = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getRepository('BibliometryMainBundle:Laboratory');
        $homeLaboratory = $laboratoriesRepository->findOneBy(array(
                "HALID" => $this->getApplication()->getKernel()->getContainer()->getParameter('HALID_lab')
        ));
        $researchers = $homeLaboratory->getLaboratoryTeam()->getResearchers();
        $DBLPService = $this->getApplication()->getKernel()->getContainer()->get("bibliometry_main.DBLP");
        $DBLPService->importDblpProfile($researchers, $output);
    }
}