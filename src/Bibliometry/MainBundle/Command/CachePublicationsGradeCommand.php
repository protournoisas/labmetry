<?php

namespace Bibliometry\MainBundle\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class CachePublicationsGradeCommand extends Command
{

    protected function configure()
    {
        $this->setName('bibliometry:cache-publications-grade')->setDescription('For each publication stored, find its grade (conference:ranking, journal:IF) and store it in the publication if it exists (to avoid searching it when browsing the website).');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getApplication()->getKernel()->getContainer()->get('doctrine')->getManager();
        $repository = $em->getRepository("BibliometryMainBundle:Publication");
        $publications = $repository->findAll();
        $previousProgress = -1;
        $progress = 0;
        $nbOfPublications = count($publications);
        foreach($publications as $publication)
        {
            if ($previousProgress != round($progress)) {
                $previousProgress = round($progress);
                $output->writeln(round($progress));
            }
            $progress += 1. / $nbOfPublications * 100;
            $publiConference = $publication->getPubliConference();
            if($publiConference != NULL)
            { // conference publication
                $conference = $publiConference->getConference();
                if($conference != NULL)
                {
                    $ranking = $conference->getConferenceRankingYear($publication->getYear());
                    $publiConference->setConferenceRanking($ranking);
                    $em->persist($publiConference);
                }
                continue;
            }
            
            $publiJournal = $publication->getPubliJournal();
            if($publiJournal != NULL)
            { // journal publication
                $IF = $publication->getPubliJournal()->getJournal()->getIFYear($publication->getYear());
                $publiJournal->setImpactFactor($IF);
                $em->persist($publiJournal);
            }
        }
        $em->flush();
    }
}