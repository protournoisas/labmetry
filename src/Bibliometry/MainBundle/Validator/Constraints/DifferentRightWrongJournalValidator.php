<?php

namespace Bibliometry\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DifferentRightWrongJournalValidator extends ConstraintValidator
{
    public function validate($duplicateJournals, Constraint $constraint)
    {
        if ($duplicateJournals->getRightJournal() != NULL && $duplicateJournals->getWrongJournal() != NULL) {
            if ($duplicateJournals->getRightJournal()->getId() == $duplicateJournals->getWrongJournal()->getId()) {
                $this->context->addViolation(
                    $constraint->message
                );
            }
        }
    }
}