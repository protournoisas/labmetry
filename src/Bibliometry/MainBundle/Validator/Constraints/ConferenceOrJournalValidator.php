<?php

namespace Bibliometry\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConferenceOrJournalValidator extends ConstraintValidator
{

    public function validate($wrongPublicationMatch, Constraint $constraint)
    {
        if($wrongPublicationMatch->getRightConference() != NULL && $wrongPublicationMatch->getRightJournal() != NULL)
        { // the user cannot chose at the same time a right conference and a right journal
            $this->context->addViolation($constraint->message);
        }
        
        if($wrongPublicationMatch->getRightConference() == NULL && $wrongPublicationMatch->getConferenceNotReferenced() == false && $wrongPublicationMatch->getRightJournal() == NULL)
        { // the user must fill one of the three available things among right conference, ConferenceNotReferenced, right journal
            $this->context->addViolation($constraint->message);
        }
        
        if($wrongPublicationMatch->getConferenceNotReferenced() == true && ($wrongPublicationMatch->getRightConference() != NULL || $wrongPublicationMatch->getRightJournal() != NULL))
        { // if the user check the checkbox "ConferenceNotReferenced", their should be no right journal or right conference selected
            $this->context->addViolation($constraint->message);
        }
        
        if($wrongPublicationMatch->getConferenceNotReferenced() == true && $wrongPublicationMatch->getPublication()->getType() != "conference")
        { // if the user check the checkbox "ConferenceNotReferenced", the publication must be of type conference
            $this->context->addViolation($constraint->message);
        }
    }
}
