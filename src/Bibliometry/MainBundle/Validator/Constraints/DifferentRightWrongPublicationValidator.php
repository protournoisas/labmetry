<?php

namespace Bibliometry\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DifferentRightWrongPublicationValidator extends ConstraintValidator
{
    public function validate($duplicatePublications, Constraint $constraint)
    {
        if ($duplicatePublications->getRightPublication() != NULL && $duplicatePublications->getWrongPublication() != NULL) {
            if ($duplicatePublications->getRightPublication()->getId() == $duplicatePublications->getWrongPublication()->getId()) {
                $this->context->addViolation(
                    $constraint->message
                );
            }
        }
    }
}