<?php

namespace Bibliometry\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class DifferentRightWrongPublication extends Constraint
{
    public $message = 'bibliometry.errors.error_report.error_same_right_wrong_publication';
    
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}