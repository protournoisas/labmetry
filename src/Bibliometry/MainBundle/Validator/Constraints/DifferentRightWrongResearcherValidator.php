<?php

namespace Bibliometry\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class DifferentRightWrongResearcherValidator extends ConstraintValidator
{
    public function validate($duplicateResearchers, Constraint $constraint)
    {
        if ($duplicateResearchers->getRightResearcher() != NULL && $duplicateResearchers->getDuplicateResearchers()->count() > 0) {
            foreach ($duplicateResearchers->getDuplicateResearchers() as $duplicateResearcher)
            {           
                if ($duplicateResearchers->getRightResearcher()->getId() == $duplicateResearcher->getId()) {
                    $this->context->addViolation(
                        $constraint->message
                    );
                }
            }
        }
    }
}