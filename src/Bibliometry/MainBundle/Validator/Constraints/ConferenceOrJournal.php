<?php

namespace Bibliometry\MainBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 */
class ConferenceOrJournal extends Constraint
{
    public $message = 'bibliometry.errors.error_report.error_conference_or_journal';
    
    public function validatedBy()
    {
        return get_class($this).'Validator';
    }
    
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}